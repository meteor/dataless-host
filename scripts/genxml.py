#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import getopt
from dbirdparse.dbirdparserv2 import DbirdParserV2
from hermes.generator import XMLGenerator


def usage():
    """
    Basic usage method.
    """
    print("%s %s" % (sys.argv[0], "  by GeoAzur"))
    print("Generate or verify Station XML")
    print("")
    print("  -i, --input    Set dbird input file name")
    print("  -o, --output   Set station XML output file name")
    print("  --sensor       Set sensor_list file")
    print("  --unit         Set unit_list file")
    print("  --pz           Set PZ root directory")
    print("  --verify       Set the station XML file to verify (exclusive with -i)")
    print("  -h, --help     Display this message")
    print("  -v, --verbose  Enable verbose mode")


def main():
    inputs = None
    output = None
    sensor = None
    unit = None
    verify = None
    pz_dir = None
    verbose = False
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:v", ["help", "input=",
                                                            "output=", "verify=",
                                                            "sensor=", "unit=",
                                                            "pz=", "verbose"])
    except getopt.GetoptError as err:
        # will print something like "option -current_argument not recognized"
        print(str(err))
        usage()
        sys.exit(1)

    for current_option, current_argument in opts:
        if current_option in ("-h", "--help"):
            usage()
            sys.exit()
        elif current_option in ("-i", "--input"):
            inputs = current_argument
        elif current_option in ("--sensor"):
            sensor = current_argument
        elif current_option in ("--unit"):
            unit = current_argument
        elif current_option in ("--pz"):
            pz_dir = current_argument
        elif current_option in ("-o", "--output"):
            output = current_argument
        elif current_option in ("--verify"):
            verify = current_argument
        elif current_option in ("-v", "--verbose"):
            verbose = True
        else:
            assert False, "unhandled option"

    if inputs is None and verify is None:
        print("Missing mandatory parameters -i or --verify")
        sys.exit(1)

    if output is None and verify is None:
        print("Missing mandatory parameters -o")
        sys.exit(1)

    if inputs is not None and (sensor is None or unit is None):
        print("-i parameters imply --sensor and --unit parameters")
        sys.exit(1)

    try:
        if inputs is not None:
            input_file = open(inputs)
        else:
            input_file = open(verify)
    except IOError as exception:
        print("Open : %s" % exception)
        sys.exit(1)

    generator = XMLGenerator()

    if output is not None:
        # Open output file
        try:
            output_file = open(output, "w")
        except IOError as exception:
            print("Open : %s" % exception)
            sys.exit(1)

        # Get sensor_list file content
        try:
            sensor_list_file = open(sensor)
        except IOError as exception:
            print("Open : %s" % exception)
            sys.exit(1)

        sensor_content = sensor_list_file.readlines()

        # Get unit_list file content
        try:
            unit_list_file = open(unit)
        except IOError as exception:
            print("Open : %s" % exception)
            sys.exit(1)

        unit_content = unit_list_file.readlines()

        if verbose:
            print("Parsing %s" % inputs)
        file_content = input_file.read()
        dbirdparser = DbirdParserV2(sensor_content, unit_content, pz_dir, verbose)
        dbird = dbirdparser.parse_str(file_content)
        if dbird is None:
            print("Error parsing dbird")
            sys.exit(1)
        output_file.write(generator.generate(dbird).encode("utf-8"))
    else:
        (result, message) = generator.verify(input_file)
        if not result:
            print("The station XML file is not valid")
            print(message)
            sys.exit(1)

        print("The station XML file is valid")
        sys.exit(0)


if __name__ == "__main__":
    main()
