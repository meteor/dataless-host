#!/usr/bin/python27

import sys
import json
import getopt
from jinja2 import Environment, DictLoader
from oca.jinjautils.filters import empty, optional_value
VERSION = "0.1"


DBIRD_TMPL = """version 2
originating_organization "{{OriginatingOrganization}}" "noeudA_LB@geoazur.unice.fr" "{{WebSite}}" "{{ContactPhone}}"

begin_author
% for author in Authors
  {{author.Label}} ["{{author.Names|join(" ")}}"] [] [{{author.Emails|map('tojson')|join(",")}}] [{{author.Phones|join(",")}}]
% endfor
end_author

begin_network
  begin_comment
% for comment in Networks.NetworkComments
     {{ comment.Label }} "{{ comment.Description }}" {{ comment.StartTime|dbirddate }} {{ comment.EndTime|dbirddate }} [ {{ comment.Authors | join(",") }} ]
% endfor
  end_comment
% for network in Networks.Networks
  {{network.Label}} "{{network.FDSNCode}}" "{{network.Description}}" "{{network.ContactMail}}" {{network.StartTime|dbirddate}} {{network.EndTime|dbirddate}} "{{network.AlternateCode}}" [{{ network.Comments|join(',') }}]
% endfor
end_network

% for station in Stations
begin_station
  "{{station.IrisCode}}" {{station.StartTime|dbirddate }} {{station.EndTime|dbirddate }} ({{station.Latitude.Value}}, {{station.Latitude.MinusError | optional_value("none")}},{{station.Latitude.PlusError | optional_value("none")}}) ({{station.Longitude.Value}}, {{station.Longitude.MinusError | optional_value("none")}},{{station.Longitude.PlusError | optional_value("none")}}) ({{station.Elevation.Value}}, {{station.Elevation.MinusError | optional_value("none")}},{{station.Elevation.PlusError | optional_value("none")}}) "{{station.SiteName}}"
  Contact "{{station.Contact}}"
  Owner "{{station.Owner}}"

  begin_comment
   % for comment in station.Comments
     {{ comment.Label }} "{{ comment.Description }}" {{ comment.StartTime|dbirddate }} {{ comment.EndTime|dbirddate }} [ {{ comment.Authors | join(",") }} ] {{comment.Type}}
   % endfor
  end_comment
  begin_location
  % for location in station.Locations
    {{location.Label}} "{{location.Code}}" ({{location.Latitude.Value}}, {{location.Latitude.MinusError | optional_value("none")}},{{location.Latitude.PlusError | optional_value("none")}}) ({{location.Longitude.Value}}, {{location.Longitude.MinusError | optional_value("none")}},{{location.Longitude.PlusError | optional_value("none")}}) ({{location.Elevation.Value}}, {{location.Elevation.MinusError | optional_value("none")}},{{location.Elevation.PlusError | optional_value("none")}}) {{location.Depth}} "{{location.Vault}}" "{{location.Geology}}"
  % endfor
  end_location

  begin_sensor
  % for sensor in station.Sensors
    {{sensor.Label}} "{{sensor.Type}}" "{{sensor.SerialNumber}}" "{{sensor.PZFile}}" {{sensor.Azimuth}} {{sensor.Dip}}
  % endfor
  end_sensor

  % if station.AnalogFilters is not empty
  begin_ana_filter
  % for filter in station.AnalogFilters
    {{ filter.Label }} "{{filter.Type}}" "{{filter.SerialNumber}}" "{{ filter.PZFile }}"
  % endfor
  end_ana_filter
  % endif

  begin_digitizer
  % for digitizer in station.Digitizers
    {{ digitizer.Label }} "{{digitizer.Type}}" "{{digitizer.SerialNumber}}" "{{ digitizer.PZFile }}"
  % endfor
  end_digitizer

  % if station.DigitalFilters is not empty
  begin_decimation
  % for filter in station.DigitalFilters
    {{ filter.Label }} "{{filter.Type}}" "{{filter.SerialNumber}}" "{{ filter.PZFile }}"
  % endfor
  end_decimation
  % endif

  begin_channel
  % for current in station.Channels
    {{current.Location}} {{ current.IrisCode }} {{current.Sensor}} [{{current.AnalogFilter|join(',')}}] {{current.Digitizer}} [{{current.DigitalFilter|join(',')}}] {{current.Flags}} {{ current.Encoding }} {{current.StartTime|dbirddate }} {{current.EndTime|dbirddate }} {{current.Network}} [{{ current.Comments|join(',')}}]
  % endfor
  end_channel
end_station

% endfor
"""


def dbirddate(instant):
    """
    Transform string date in dbird string representation

    instant -- The string date
    """
    if instant is None:
        return "present"
    return instant


def usage():
    """
    Basic usage function
    """
    print(sys.argv[0], "  by Resif Team")
    print("Convert JDbird to Dbird")
    print("")
    print("-i, --input         Set input dbird filename")
    print("-o, --output        Set output jdbird filename")
    print("-h, --help          Display this message")
    print("-v, --verbose       Enable verbose mode.")
    print("-V, --version       Print version information.")


def main():

    input_filename = None
    output_filename = None
    verbose = False

    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:o:hvV",
                                   ["input=", "output=",
                                    "help", "verbose", "version"])
    except getopt.GetoptError as err:
        print(str(err))  # will print something like "option -current_argument not recognized"
        usage()
        sys.exit(1)

    for current_option, current_argument in opts:
        if current_option in ("-i", "--input"):
            input_filename = current_argument
        elif current_option in ("-o", "--output"):
            output_filename = current_argument
        elif current_option in ("-h", "--help"):
            usage()
            sys.exit()
        elif current_option in ("-v", "--verbose"):
            verbose = True
        elif current_option == "-V":
            print(sys.argv[0], " version ", VERSION)
        else:
            assert False, "unhandled option"

    if input_filename is None:
        print("You must specify input filename with -o")
        sys.exit(1)

    try:
        if verbose:
            print("Opening %s" % input_filename)
        input_file = open(input_filename)
        content = json.load(input_file)
    except IOError as exception:
        print("Error opening %s: %s" % (input_filename, exception))

    if output_filename is not None:
        try:
            if verbose:
                print("Opening %s" % output_filename)
            output_file = open(output_filename, "w")
        except IOError as exception:
            print("Error opening %s: %s" % (output_filename, exception))
    else:
        output_file = sys.stdout

    tmplMap = {
        'dbird.tmpl': DBIRD_TMPL
    }
    environement = Environment(loader=DictLoader(tmplMap),
                               line_statement_prefix="%")
    environement.filters['optional_value'] = optional_value
    environement.tests['empty'] = empty
    environement.filters['dbirddate'] = dbirddate
    template = environement.get_template('dbird.tmpl')
    output_file.write(template.render(content).encode('utf8'))


if __name__ == "__main__":
    main()
