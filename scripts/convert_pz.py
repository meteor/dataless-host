#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import shutil
import getopt
from convertpz.sensor import convert_sensor_dir
from convertpz.anafilter import convert_anafilter_dir
from convertpz.digitizer import convert_digitizer_dir
from convertpz.digfilter import convert_digfilter_dir
from convertpz.sensor import convert_sensor_include_dir
from convertpz.anafilter import convert_anafilter_include_dir
from convertpz.digfilter import convert_digfilter_include_dir
VERSION = 1.0


def usage():
    """
    Basic usage function
    """
    print(sys.argv[0], "  by Resif Team")
    print("Convert PZdatabank to JSON response databank")
    print("")
    print("-i, --input         Set input root response databank")
    print("-o, --output        Set output root response databank")
    print("-h, --help          Display this message")
    print("-v, --verbose       Enable verbose mode.")
    print("-V, --version       Print version information.")


def check_directory(directory_path, create_missing, verbose):
    if os.path.exists(directory_path):
        if not os.path.isdir(directory_path):
            sys.stderr.write("The specified directory %s is not a directory\n" %
                             directory_path)
            sys.exit(1)
    elif create_missing:
        if verbose:
            sys.stderr.write("The specified output directory not exists, try to create\n")
        try:
            os.mkdir(directory_path)
        except OSError as exception:
            sys.stderr.write("Can't create %s: %s\n" %
                             (directory_path, exception))
            sys.exit(1)
    else:
        sys.stderr.write("The specified directory %s does not exists\n" %
                         directory_path)
        sys.exit(1)


def main():

    verbose = False
    input_directory = None
    output_directory = None
    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:o:hvV",
                                   ["input=", "output=",
                                    "help", "verbose", "version"])
    except getopt.GetoptError as err:
        print(str(err))  # will print something like "option -current_argument not recognized"
        usage()
        sys.exit(1)

    for current_option, current_argument in opts:
        if current_option in ("-i", "--input"):
            input_directory = current_argument
        elif current_option in ("-o", "--output"):
            output_directory = current_argument
        elif current_option in ("-h", "--help"):
            usage()
            sys.exit()
        elif current_option in ("-v", "--verbose"):
            verbose = True
        elif current_option == "-V":
            print(sys.argv[0], " version ", VERSION)
        else:
            assert False, "unhandled option"

    if input_directory is None:
        sys.stderr.write("You must specify input response root directory with -i\n")
        sys.exit(1)

    if output_directory is None:
        sys.stderr.write("You must specify output response root directory with -o\n")
        sys.exit(1)

    # Verify input response databank structure
    check_directory(input_directory, False, verbose)
    check_directory(os.path.join(input_directory, "sensor"), False, verbose)
    check_directory(os.path.join(input_directory, "sensor", "include"), False, verbose)
    check_directory(os.path.join(input_directory, "ana_filter"), False, verbose)
    check_directory(os.path.join(input_directory, "ana_filter", "include"), False, verbose)
    check_directory(os.path.join(input_directory, "digitizer"), False, verbose)
    check_directory(os.path.join(input_directory, "dig_filter"), False, verbose)
    check_directory(os.path.join(input_directory, "dig_filter", "include"), False, verbose)
    check_directory(os.path.join(input_directory, "software_filter"), False, verbose)
    check_directory(os.path.join(input_directory, "software_filter", "include"), False, verbose)

    # Remove existing json PZ
    if os.path.exists(output_directory):
        shutil.rmtree(output_directory)

    # Verify/Create output response databank structure
    check_directory(output_directory, True, verbose)
    check_directory(os.path.join(output_directory, "sensor"), True, verbose)
    check_directory(os.path.join(output_directory, "sensor", "include"), True, verbose)
    check_directory(os.path.join(output_directory, "ana_filter"), True, verbose)
    check_directory(os.path.join(output_directory, "ana_filter", "include"), True, verbose)
    check_directory(os.path.join(output_directory, "digitizer"), True, verbose)
    check_directory(os.path.join(output_directory, "dig_filter"), True, verbose)
    check_directory(os.path.join(output_directory, "dig_filter", "include"), True, verbose)
    check_directory(os.path.join(output_directory, "software_filter"), True, verbose)
    check_directory(os.path.join(output_directory, "software_filter", "include"), True, verbose)

    convert_sensor_dir(os.path.join(input_directory, "sensor"),
                       os.path.join(output_directory, "sensor"),
                       verbose)
    convert_sensor_include_dir(os.path.join(input_directory, "sensor", "include"),
                               os.path.join(output_directory, "sensor", "include"),
                               verbose)
    convert_anafilter_dir(os.path.join(input_directory, "ana_filter"),
                          os.path.join(output_directory, "ana_filter"),
                          verbose)
    convert_anafilter_include_dir(os.path.join(input_directory, "ana_filter", "include"),
                                  os.path.join(output_directory, "ana_filter", "include"),
                                  verbose)
    convert_digitizer_dir(os.path.join(input_directory, "digitizer"),
                          os.path.join(output_directory, "digitizer"),
                          verbose)
    convert_digfilter_dir(os.path.join(input_directory, "dig_filter"),
                          os.path.join(output_directory, "dig_filter"),
                          verbose)
    convert_digfilter_include_dir(os.path.join(input_directory, "dig_filter", "include"),
                                  os.path.join(output_directory, "dig_filter", "include"),
                                  verbose)
    convert_digfilter_dir(os.path.join(input_directory, "software_filter"),
                          os.path.join(output_directory, "software_filter"),
                          verbose)
    convert_digfilter_include_dir(os.path.join(input_directory, "software_filter", "include"),
                                  os.path.join(output_directory, "software_filter", "include"),
                                  verbose)


if __name__ == "__main__":
    main()
