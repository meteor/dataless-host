#!/usr/bin/python

import sys
import getopt

VERSION = "0.1"


def usage():
    """
    Basic usage function
    """
    print(sys.argv[0], "  by Resif Team")
    print("Merge consecutive channel without change")
    print("")
    print("-i, --input         Set input dbird filename")
    print("-o, --output        Set output dbird filename")
    print("-h, --help          Display this message")
    print("-v, --verbose       Enable verbose mode.")
    print("-V, --version       Print version information.")


def merge_comment(l1, l2):
    """
    Merge comment list

    l1     -- The first comment list as string
    l2     -- The second comment list as string

    return -- The merged comment list as string
    """
    s1 = set(filter(lambda x: x != "", map(lambda x: x.strip(), l1[1:-1].split(","))))
    s2 = set(filter(lambda x: x != "", map(lambda x: x.strip(), l2[1:-1].split(","))))
    tmp_list = list(s1.union(s2))
    tmp_list.sort()
    return "[%s]" % ",".join(tmp_list)


def merge_channel(channels, sort_loc):
    """
    Merge consecutive channel without change

    channels -- The list of dbird channels as string
    sort_loc -- Say if channel must be sorted witrh location code
                (Don't sort on location code permit to prevent bug in SZBH)
    return   -- The list of merged channel as string

    !!! WARNING !!!
    You must note that this function merge only two consecutives channels.
    For a full merge this function must be call until
    result is equal to given channels list.
    """
    if len(channels) == 1:
        return channels
    have_merge_channel = False
    splitted_channel = list()
    for current in channels:
        splitted_channel.append(current.split())
    if sort_loc:
        splitted_channel.sort(key=lambda x: "%s_%s_%s" % (x[0], x[1], x[8]))
    else:
        splitted_channel.sort(key=lambda x: "%s_%s" % (x[1], x[8]))
    merged_channel = list()
    for index in range(1, len(splitted_channel)):
        if have_merge_channel:
            if index == len(splitted_channel) - 1:
                merged_channel.append(splitted_channel[index])
            have_merge_channel = False
            continue
        elif(splitted_channel[index - 1][:8] == splitted_channel[index][:8]
             and splitted_channel[index - 1][10] == splitted_channel[index][10]):
            # Merge channel
            merged_channel.append(splitted_channel[index - 1][:8] + [splitted_channel[index - 1][8]]
                                  + [splitted_channel[index][9]] + [splitted_channel[index - 1][10]]
                                  + [merge_comment(splitted_channel[index - 1][11],
                                                   splitted_channel[index][11])])
            have_merge_channel = True
        else:
            # keep channel
            merged_channel.append(splitted_channel[index - 1])
            # Keep last channel
            if index == len(splitted_channel) - 1:
                merged_channel.append(splitted_channel[index])

    merged_channel.sort(key=lambda x: "%s_%s_%s_%s" % (x[8], x[1], x[0], x[10]))
    result = list()
    for merged in merged_channel:
        result.append("    " + " ".join(merged))
    return result


def main():

    input_filename = None
    output_filename = None
    verbose = False

    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:o:hvV",
                                   ["input=", "output=",
                                    "help", "verbose", "version"])
    except getopt.GetoptError as err:
        print(str(err))  # will print something like "option -current_argument not recognized"
        usage()
        sys.exit(1)

    for current_option, current_argument in opts:
        if current_option in ("-i", "--input"):
            input_filename = current_argument
        elif current_option in ("-o", "--output"):
            output_filename = current_argument
        elif current_option in ("-h", "--help"):
            usage()
            sys.exit()
        elif current_option in ("-v", "--verbose"):
            verbose = True
        elif current_option == "-V":
            print(sys.argv[0], " version ", VERSION)
        else:
            assert False, "unhandled option"

    if input_filename is None:
        print("You must specify input filename with -i")
        sys.exit(1)

    try:
        if verbose:
            print("Opening %s" % input_filename)
        input_file = open(input_filename)
    except IOError as exception:
        print("Error opening %s: %s" % (input_filename, exception))

    if output_filename is not None:
        try:
            if verbose:
                print("Opening %s" % output_filename)
            output_file = open(output_filename, "w")
        except IOError as exception:
            print("Error opening %s: %s" % (output_filename, exception))
    else:
        output_file = sys.stdout

    input_nb_channel = 0
    output_nb_channel = 0
    in_channel = False
    start_station = False
    sort_location = True
    for current_line in input_file:
        if current_line.strip() == "begin_station":
            start_station = True
        elif current_line.strip() == "end_station":
            sort_location = True
        elif start_station:
            start_station = False
            if current_line.split()[0] == "\"SZBH\"":
                sort_location = False
        elif current_line.strip() == "begin_channel":
            # Begin of channel section
            in_channel = True
            channel_list = list()
            output_file.write(current_line)
            continue
        elif current_line.strip() == "end_channel":
            # End of channel section
            in_channel = False
            # Merge channel until no change
            previous_merge = None
            current_merge = merge_channel(channel_list, sort_location)
            while(previous_merge != current_merge):
                previous_merge = current_merge
                current_merge = merge_channel(previous_merge, sort_location)
            # Write merged channels
            for channel_line in current_merge:
                output_nb_channel += 1
                output_file.write("%s\n" % channel_line)
            output_file.write(current_line)
            continue
        elif in_channel:
            # Append channel to list
            channel_list.append(current_line)
            input_nb_channel += 1
            continue
        # Just copy line that are not channel
        output_file.write(current_line)

    if verbose:
        print("write %s channel on %s (%s%%)" % (output_nb_channel, input_nb_channel,
                                                 (float(output_nb_channel) / float(input_nb_channel))))


if __name__ == "__main__":
    main()
