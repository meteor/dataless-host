#!/usr/bin/python

import sys
import yaml
import getopt
from copy import copy
VERSION = "0.1"


def usage():
    """
    Basic usage function
    """
    print(sys.argv[0], "  by Resif Team")
    print("Merge consecutive channel without change")
    print("")
    print("-i, --input         Set input ydbird filename")
    print("-o, --output        Set output ydbird filename")
    print("-h, --help          Display this message")
    print("-v, --verbose       Enable verbose mode.")
    print("-V, --version       Print version information.")


def recursive_tuple(l):
    result = list()
    for current in l:
        if type(current) == list:
            current = recursive_tuple(current)
        result.append(current)
    return tuple(result)


def merge_comment(l1, l2):
    """
    Merge comment list

    l1     -- The first comment list as string
    l2     -- The second comment list as string

    return -- The merged comment list as string
    """
    s1 = set(recursive_tuple(l1))
    s2 = set(recursive_tuple(l2))
    tmp_list = list(s1.union(s2))
    tmp_list.sort()
    return tmp_list


def is_equivalent(channel1, channel2):
    """
    Say if two channel is equivalent.
    Two channels are equivalent if
      -- have same IRIS code, location code and network code
      -- have same response (same material)
      -- have same flags and same encoding

    channel1 -- The first channel
    channel2 -- The second channel

    return   -- True if the channels have are equivalent
                and false otherwise
    """
    if channel1[1] != channel2[1]:
        return False
    if channel1[0] != channel2[0]:
        return False
    if channel1[11] != channel2[11]:
        return False

    if channel1[2] != channel2[2]:
        return False
    if channel1[3] != channel2[3]:
        return False
    if channel1[4] != channel2[4]:
        return False
    if channel1[5] != channel2[5]:
        return False

    if channel1[6] != channel2[6]:
        return False
    if channel1[7] != channel2[7]:
        return False

    return True


def merge_channel(channels, sort_loc):
    """
    Merge consecutive channel without change

    channels -- The list of dbird channels
    sort_loc -- Say if channel must be sorted witrh location code
                (Don't sort on location code permit to prevent bug in SZBH)
    return   -- The list of merged channel

    !!! WARNING !!!
    You must note that this function merge only two consecutives channels.
    For a full merge this function must be call until
    result is equal to given channels list.
    """
    if len(channels) == 1:
        return channels
    if sort_loc:
        channels.sort(key=lambda x: "%s_%s_%s" % (x[0][0], x[1], x[8]))
    else:
        channels.sort(key=lambda x: "%s_%s" % (x[1], x[8]))
    have_merge_channel = False
    merged_channel = list()
    for index in range(1, len(channels)):
        if have_merge_channel:
            if index == len(channels) - 1:
                merged_channel.append(channels[index])
            have_merge_channel = False
            continue
        elif(is_equivalent(channels[index - 1], channels[index])):
            # Merge channel
            new_channel = copy(channels[index - 1])
            new_channel[9] = channels[index][9]
            new_channel[12] = merge_comment(new_channel[12],
                                            channels[index][12])

            merged_channel.append(new_channel)
            have_merge_channel = True
        else:
            # keep channel
            merged_channel.append(channels[index - 1])
            # Keep last channel
            if index == len(channels) - 1:
                merged_channel.append(channels[index])

    merged_channel.sort(key=lambda x: "%s_%s_%s_%s" %
                                      (x[8], x[1],
                                       x[0][0], x[11][0]))
    return merged_channel


def main():

    input_filename = None
    output_filename = None
    verbose = False

    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:o:hvV",
                                   ["input=", "output=",
                                    "help", "verbose", "version"])
    except getopt.GetoptError as err:
        print(str(err))  # will print something like "option -current_argument not recognized"
        usage()
        sys.exit(1)

    for current_option, current_argument in opts:
        if current_option in ("-i", "--input"):
            input_filename = current_argument
        elif current_option in ("-o", "--output"):
            output_filename = current_argument
        elif current_option in ("-h", "--help"):
            usage()
            sys.exit()
        elif current_option in ("-v", "--verbose"):
            verbose = True
        elif current_option == "-V":
            print(sys.argv[0], " version ", VERSION)
        else:
            assert False, "unhandled option"

    if input_filename is None:
        print("You must specify input filename with -i")
        sys.exit(1)

    try:
        if verbose:
            print("Opening %s" % input_filename)
        input_file = open(input_filename)
        content = yaml.load(input_file)
    except IOError as exception:
        print("Error opening %s: %s" % (input_filename, exception))

    if output_filename is not None:
        try:
            if verbose:
                print("Opening %s" % output_filename)
            output_file = open(output_filename, "w")
        except IOError as exception:
            print("Error opening %s: %s" % (output_filename, exception))
    else:
        output_file = sys.stdout

    input_nb_channel = 0
    output_nb_channel = 0
    for current_station in content["station"]:
        sort_location = True
        input_nb_channel += len(current_station["channel"])
        if current_station["definition"][0] == "SZBH":
            sort_location = False
        result = merge_channel(current_station["channel"], sort_location)
        while(len(result) != len(current_station["channel"])):
            current_station["channel"] = result
            result = merge_channel(current_station["channel"], sort_location)
        current_station["channel"] = result
        output_nb_channel += len(result)

    if verbose:
        print("write %s channel on %s (%s%%)" % (output_nb_channel, input_nb_channel,
                                                 (float(output_nb_channel) / float(input_nb_channel))))
    yaml.dump(content, output_file, indent=2, canonical=True, default_flow_style=True)
    output_file.close()


if __name__ == "__main__":
    main()
