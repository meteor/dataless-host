#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import json
import getopt
from oca.database.abstractdb import DatabaseObjectFactory

VERSION = 1.0


def usage():
    """
    Basic usage function
    """
    print(sys.argv[0], "  by Resif Team")
    print("Generate das_list from Phoenix")
    print("")
    print("-H, --host          Set database hostname")
    print("-d, --database      Set database name")
    print("-u, --user          Set user (web by default)")
    print("-p, --password      Set password (no password by default)")
    print("-o, --output        Set output filename")
    print("-h, --help          Display this message")
    print("-v, --verbose       Enable verbose mode.")
    print("-V, --version       Print version information.")


def main():

    verbose = False
    host = None
    database = None
    username = None
    password = None
    output = None
    try:
        opts, args = getopt.getopt(sys.argv[1:], "H:d:o:u:p:n:s:l:c:hvV",
                                   ["host=", "database=", "output=",
                                    "user=", "password=",
                                    "network=", "station=", "location=",
                                    "channel=",
                                    "help", "verbose", "version"])
    except getopt.GetoptError as err:
        print(str(err))  # will print something like "option -current_argument not recognized"
        usage()
        sys.exit(1)

    for current_option, current_argument in opts:
        if current_option in ("-H", "--host"):
            host = current_argument
        elif current_option in ("-d", "--database"):
            database = current_argument
        elif current_option in ("-u", "--user"):
            username = current_argument
        elif current_option in ("-p", "--password"):
            password = current_argument
        elif current_option in ("-o", "--output"):
            output = current_argument
        elif current_option in ("-h", "--help"):
            usage()
            sys.exit()
        elif current_option in ("-v", "--verbose"):
            verbose = True
        elif current_option == "-V":
            print(sys.argv[0], " version ", VERSION)
        else:
            assert False, "unhandled option"

    # Verify mandatory parameters
    if host is None:
        print("You must specify database hostname with -H")
        sys.exit(1)

    if database is None:
        print("You must specify database name with -d")
        sys.exit(1)

    if username is None:
        print("You must specify database login name with -u")
        sys.exit(1)

    if output is None:
        print("You must specify output filename with -o")
        sys.exit(1)

    try:
        output_file = open(output, "w")
    except IOError as exception:
        sys.stderr.write("Error opening %s: %s" %
                         (output, exception))
        sys.exit(1)

    # Get sensor model list
    database = DatabaseObjectFactory(host, username, database=database,
                                     password=password)
    das_model_list = database.get_table_objects("das_model")

    if verbose:
        print("Begin %s generation" % output)
    # Now generate file content
    content = list()
    for current_das_model in das_model_list:
        current_device_model = current_das_model.device_model()
        das_code = current_device_model.code()

        current_device_model.load("device_model_super_type")
        if len(current_device_model.device_model_super_type()) == 0:
            print("Can't find super type for das %s" % das_code)
            continue
        das_super_type = list(current_device_model.device_model_super_type())[0].super_type()
        description = "no description"

        if(das_super_type.description() not in (None, "")):
            description = das_super_type.description().encode('utf8')

        new_sensor = {
            "DbirdCode": das_code,
            "SeedCode": das_super_type.code(),
            "Description": description
        }
        content.append(new_sensor)
    json.dump(content, output_file, indent=2)
    output_file.close()


if __name__ == '__main__':
    main()
