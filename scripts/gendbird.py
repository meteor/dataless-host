#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
import os
import sys
import copy
import getopt
from fnmatch import fnmatch
from datetime import datetime
from pzbank.loader import PZAccess
from phoenix.abstract.dbird import DbirdContact
from phoenix.abstract.dbird import DbirdStation
from phoenix.abstract.dbird import DbirdChannel
from phoenix.abstract.dbird import set_pz_access
from gendbirdv2.dbird_generator import JSON_FORMAT
from gendbirdv2.dbird_generator import YAML_FORMAT
from gendbirdv2.dbird_generator import DBIRD_FORMAT
from phoenix.core.phoenix_query import PhoenixQuery
from phoenix.abstract.dbird import set_bugnodeB_date
from gendbirdv2.dbird_generator import DbirdGenerator
from phoenix.abstract.dbird import DbirdCommentAuthor
from phoenix.abstract.dbird import DbirdNetworkComment
from phoenix.abstract.dbird import DbirdStationComment
from phoenix.abstract.dbird import DbirdChannelComment
from oca.database.abstractdb import DatabaseObjectFactory

VERSION = "0.2"
TABLE_CONSTRUCTORS = {"station": DbirdStation,
                      "channel": DbirdChannel,
                      "network_comment": DbirdNetworkComment,
                      "station_comment": DbirdStationComment,
                      "channel_comment": DbirdChannelComment,
                      "comment_author": DbirdCommentAuthor,
                      "contact": DbirdContact}


def usage():
    """
    Basic usage function
    """
    print(sys.argv[0], "  by Resif Team")
    print("Generate DBird file from Phoenix database")
    print("")
    print("-H, --host          Set database hostname")
    print("-d, --database      Set database name")
    print("-u, --user          Set user (web by default)")
    print("-p, --password      Set password (no password by default)")
    print("--pzroot            Set PZ root directory")
    print("-o, --output        Set output filename")
    print("                    (exclusive with -O)")
    print("-O, --output_dir    Set output directory for per station dbird file")
    print("                    (exclusive with -o)")
    print("--json              Set JSON generation instead of legacy one")
    print("--yaml              Set YAML generation instead of legacy one")
    print("-n, --network       Network selection with joker (?, *, [])")
    print("-s, --station       Station selection with joker (?, *, [])")
    print("-l, --location      Location code selection with joker (?, *, [])")
    print("-c, --channel       Channel selection with joker (?, *, [])")
    print("-C, --collection    Collection selection")
    print("--observatory       Observatory selection with joker (?, *, [])")
    print("--server            Seedlink server with joker (?, *, [])")
    print("--after             Select only channel open after given date")
    print("--before            Select only channel open before given date")
    print("--bugnodeB          Set the oldest valid station installation date")
    print("--or                Begin a new selection set definition")
    print("-h, --help          Display this message")
    print("-v, --verbose       Enable verbose mode.")
    print("-V, --version       Print version information.")
    print("")
    print("")
    print("-n, -s -l, -c, -C, --observatory and --server can appear")
    print(" multiples times, a channel is selected if match one of criteria")
    print("")
    print("-n, -s, -l, -c, -C, --observatory and --server argument")
    print("can be prefixed by '-' (minus) to exclude instead of select")
    print("")
    print("Date format of --after, --before and --bugnodeB is YYYY.MM.DD[-HH:MM:SS]")


def update_selection(new_selector, selection_list, exclusion_list):
    """
    Update selection list and exclusion list according to given new selector.

    new_selector --   The new selector
    selection_list -- The selection list
    exclusion_list -- The exclusion list

    If new_selector if prefixed by '-' (minus) selector is added
    to exclusion_list,
    otherwise (prefixed with '+' or not prefixed) selector is added
    to selection_list.
    """
    if new_selector[0] == '-':
        exclusion_list.append(new_selector[1:])
    elif new_selector[0] == '+':
        selection_list.append(new_selector[1:])
    else:
        selection_list.append(new_selector)


def is_matching(code, pattern_list, initial_value):
    """
    Say if a given code match with one or more pattern.

    code          -- The tested code
    pattern_list  -- The pattern list
    initial_value -- Initial value

    return           -- True code match with one or more pattern
                        or initial_value if pattern_list is empty
    """
    return reduce(lambda accumulator, current: (fnmatch(code, current) or accumulator),
                  pattern_list, initial_value)


def is_selected(code, selection_list, exclusion_list):
    """
    Say if a code is selected according to given selection and exclusion list.

    code           -- The code to test
    selection_list -- The selection list
    exclusion_list -- The exclusion list

    return         -- True if current_code is selected
                      and False otherwise.
    """
    # If selection list is empty the code is selected
    select = is_matching(code, selection_list, not selection_list)
    exclude = is_matching(code, exclusion_list, False)
    return select and not exclude


def parse_date(string_date):
    if re.match("[0-9]{4}.[0-9]{2}.[0-9]{2}-[0-9]{2}:[0-9]{2}:[0-9]{2}",
                string_date):
        return datetime.strptime(string_date,
                                 "%Y.%m.%d-%H:%M:%S")
    else:
        return datetime.strptime(string_date,
                                 "%Y.%m.%d")


def update_author_station(station, author_set):
    """
    Update author_set with station and channel comment author.

    station -- The station object
    author_set -- The set of author to update
    """
    # Begin with station comment
    station.load("station_comment")
    for current_comment in station.station_comment():
        current_comment.comment().load("comment_author")
        for comment_author in current_comment.comment().comment_author():
            author_set.add(comment_author)
    # Now process channel comment
    for current_channel in station.selected_channel():
        current_channel.load("channel_comment")
        for current_comment in current_channel.channel_comment():
            current_comment.comment().load("comment_author")
            for comment_author in current_comment.comment().comment_author():
                author_set.add(comment_author)


def update_author_network(network, author_set):
    """
    Update author_set with network comment author.

    network    -- The network object
    author_set -- The set of author to update
    """
    for current_comment in network.comment():
        current_comment.comment().load("comment_author")
        for comment_author in current_comment.comment().comment_author():
            author_set.add(comment_author)

def generate_output_file(network_list, station_list, output_filepath, 
                         generation_format, verbose):
    """
    Generate stationXML intermediate file for given station list.

    network_list      -- The listof network
    station_list      -- The list of station
    output_filepath   -- The stationXML output filepath
    generation_format -- The intermediate file format
    verbose           -- verbose flag 
    """
    generator_dict = {
        "seed_generator": "RESIF Datacentre",
        "seed_generator_contact": "noeudA_LB@geoazur.unice.fr",
        "seed_generator_url": "https://geoazur.oca.eu/"
    }
    # Compute author set
    author_set = set()
    for current_net in network_list:
        update_author_network(current_net, author_set)
    for current_station in station_list:
        update_author_station(current_station, author_set)
        # Preload site for this station
        current_station.load("station_site")
    
    
    # Preload email and phone number
    for current in author_set:
        current.author().contact().load("contact_email")
        current.author().contact().load("contact_phone")
    
    generator_dict['stations'] = station_list
    generator_dict['networks'] = network_list
    generator_dict['authors'] = list(author_set)
    generator = DbirdGenerator(generation_format)
    
    if verbose:
        sys.stderr.write("Begin generation\n")
    result = generator.generate(generator_dict)
    try:
        dbird_file = open(output_filepath, "w")
        dbird_file.write(result.encode('utf8'))
    except IOError as exception:
        sys.stderr.write("Error: %s\n" % exception)
        sys.exit(1)


def main():

    verbose = False
    host = None
    database = None
    username = None
    password = None
    pzroot = None
    output = None
    output_dir = None
    bugnodeB_date = None
    generation_format = DBIRD_FORMAT
    empty_selection = {
        'collection_selection': [],
        'observatory_selection': [],
        'server_selection': [],
        'network_selection': [],
        'station_selection': [],
        'location_selection': [],
        'channel_selection': [],
        'collection_exclusion': [],
        'observatory_exclusion': [],
        'server_exclusion': [],
        'network_exclusion': [],
        'station_exclusion': [],
        'location_exclusion': [],
        'channel_exclusion': [],
        'after_date': None,
        'before_date': None
    }
    all_selection = []
    current_selection = copy.deepcopy(empty_selection)
    all_selection.append(current_selection)

    try:
        opts, args = getopt.getopt(sys.argv[1:], "H:d:o:O:u:p:n:s:l:c:C:hvV",
                                   ["host=", "database=", "output=", "output_dir=",
                                    "json", "yaml", "user=", "password=", "pzroot=",
                                    "observatory=", "server=",
                                    "network=", "station=", "location=",
                                    "channel=", "collection=", "after=", "before=",
                                    "or", "bugnodeB=", "help", "verbose", "version"])
    except getopt.GetoptError as err:
        sys.stderr.write("%s\n" % str(err))  # will print something like "option -current_argument not recognized"
        usage()
        sys.exit(1)

    for current_option, current_argument in opts:
        if current_option in ("-H", "--host"):
            host = current_argument
        elif current_option in ("-d", "--database"):
            database = current_argument
        elif current_option in ("-u", "--user"):
            username = current_argument
        elif current_option in ("-p", "--password"):
            password = current_argument
        elif current_option in ("--pzroot",):
            pzroot = current_argument
        elif current_option in ("-o", "--output"):
            if output_dir:
                sys.stderr.write("-o and -O are exclusive\n")
                sys.exit(1)
            output = current_argument
        elif current_option in ("-O", "--output_dir"):
            if output:
                sys.stderr.write("-o and -O are exclusive\n")
                sys.exit(1)
            output_dir = current_argument
        elif current_option in ("--json",):
            generation_format = JSON_FORMAT
        elif current_option in ("--yaml",):
            generation_format = YAML_FORMAT
        elif current_option in ("--server",):
            update_selection(current_argument,
                             current_selection['server_selection'],
                             current_selection['server_exclusion'])
        elif current_option in ("--observatory",):
            update_selection(current_argument,
                             current_selection['observatory_selection'],
                             current_selection['observatory_exclusion'])
        elif current_option in ("-C", "--collection"):
            update_selection(current_argument,
                             current_selection['collection_selection'],
                             current_selection['collection_exclusion'])
        elif current_option in ("-n", "--network"):
            update_selection(current_argument,
                             current_selection['network_selection'],
                             current_selection['network_exclusion'])
        elif current_option in ("-s", "--station"):
            update_selection(current_argument,
                             current_selection['station_selection'],
                             current_selection['station_exclusion'])
        elif current_option in ("-l", "--location"):
            update_selection(current_argument,
                             current_selection['location_selection'],
                             current_selection['location_exclusion'])
        elif current_option in ("-c", "--channel"):
            update_selection(current_argument,
                             current_selection['channel_selection'],
                             current_selection['channel_exclusion'])
        elif current_option in ("--after",):
            try:
                current_date = parse_date(current_argument)
            except ValueError as exception:
                sys.stderr.write("Error in --after: %s\n" % exception)
                sys.exit(1)
            current_selection['after_date'] = current_date
        elif current_option in ("--before",):
            try:
                current_date = parse_date(current_argument)
            except ValueError as exception:
                sys.stderr.write("Error in --after: %s\n" % exception)
                sys.exit(1)
            current_selection['before_date'] = current_date
        elif current_option in ("--or",):
            current_selection = copy.deepcopy(empty_selection)
            all_selection.append(current_selection)
        elif current_option in ("--bugnodeB",):
            try:
                current_date = parse_date(current_argument)
            except ValueError as exception:
                sys.stderr.write("Error in --after: %s\n" % exception)
                sys.exit(1)
            bugnodeB_date = current_date
        elif current_option in ("-h", "--help"):
            usage()
            sys.exit()
        elif current_option in ("-v", "--verbose"):
            verbose = True
        elif current_option == "-V":
            print(sys.argv[0], " version ", VERSION)
        else:
            assert False, "unhandled option %s" % current_option

    # Verify mandatory parameters
#    if host is None:
#        print("You must specify database hostname with -H")
#        sys.exit(1)

    if database is None:
        sys.stderr.write("You must specify database name with -d\n")
        sys.exit(1)

    if username is None:
        sys.stderr.write("You must specify database login name with -u\n")
        sys.exit(1)

    if pzroot is None:
        sys.stderr.write("You must specify PZ root directory with --pzroot\n")
        sys.exit(1)

    if output is None and output_dir is None:
        sys.stderr.write("You must specify output with -o or -O\n")
        sys.exit(1)

    if output_dir is not None:
        if os.path.exists(output_dir):
            if not os.path.isdir(output_dir):
                sys.stderr.write("The specified output directory %s is not a directory\n" %
                                 output_dir)
                sys.exit(1)
        else:
            if verbose:
                sys.stderr.write("The specified output directory not exists, try to create\n")
            try:
                os.mkdir(output_dir)
            except OSError as exception:
                sys.stderr.write("Can't create %s: %s\n" %
                                 (output_dir, exception))
                sys.exit(1)

    database = DatabaseObjectFactory(host, username, database=database,
                                     password=password,
                                     constructors=TABLE_CONSTRUCTORS)
    database.register_type("sensor_component_t")

    database_query = PhoenixQuery(database)

    # Initialize PZAccess object
    pz_access = PZAccess([pzroot])
    pz_access.load_database()
    set_pz_access(pz_access)

    # Set oldest valid station installation date
    set_bugnodeB_date(bugnodeB_date)

    networks = []
    station_list = []
    if verbose:
        sys.stderr.write("Begin to select stations and channels\n")
    for station_name in sorted(database_query.get_station_list()):
        for current_selection in all_selection:
            # Skip unselected station
            if not is_selected(station_name,
                               current_selection['station_selection'],
                               current_selection['station_exclusion']):
                if verbose:
                    sys.stderr.write("Skip %s (station iris code)\n" %
                                     station_name)
                continue

            current_station = database_query.load_station(station_name, True)

            # Test collection
            collection_list = database_query.get_collection(current_station.iris_code())
            # Default value is set to  True only if collection_list is empty
            # and collection_selection list is empty
            collection_select = not(collection_list) and not(current_selection['collection_selection'])
            for current_collection in collection_list:
                # If one collection is excluded station is skipped
                if is_matching(current_collection,
                               current_selection['collection_exclusion'],
                               False):
                    collection_select = False
                    break
                collection_select = (collection_select
                                     or is_selected(current_collection,
                                                    current_selection['collection_selection'],
                                                    current_selection['collection_exclusion']))
            if collection_select is False:
                sys.stderr.write("Skip %s (Collection)\n" % station_name)
                continue

            # Skip station belonging to no collection when positive selection is made on collection
            if not collection_list and current_selection['collection_selection']:
                sys.stderr.write("Skip %s (Collection)\n" % station_name)
                continue

            # Test observatory
            if not is_selected(current_station.observatory().code(),
                               current_selection['observatory_selection'],
                               current_selection['observatory_exclusion']):
                if verbose:
                    sys.stderr.write("Skip %s (observatory)\n" % station_name)
                continue

            for current_channel in current_station.channel():
                # Filter on network
                if not is_selected(current_channel.network().fdsn_code(),
                                   current_selection['network_selection'],
                                   current_selection['network_exclusion']):
                    if verbose:
                        sys.stderr.write("Skip %s.%s.%s.%s (network)\n" %
                                         (current_channel.network().fdsn_code(),
                                          station_name,
                                          current_channel.location_code(),
                                          current_channel.iris_code()))
                    continue

                # Filter on location code
                if not is_selected(current_channel.location_code(),
                                   current_selection['location_selection'],
                                   current_selection['location_exclusion']):
                    if verbose:
                        sys.stderr.write("Skip %s.%s.%s.%s (location code)\n" %
                                         (current_channel.network().fdsn_code(),
                                          station_name,
                                          current_channel.location_code(),
                                          current_channel.iris_code()))
                    continue

                # Filter on channel name
                if not is_selected(current_channel.iris_code(),
                                   current_selection['channel_selection'],
                                   current_selection['channel_exclusion']):
                    if verbose:
                        sys.stderr.write("Skip %s.%s.%s.%s (channel code)\n" %
                                         (current_channel.network().fdsn_code(),
                                          station_name,
                                          current_channel.location_code(),
                                          current_channel.iris_code()))
                    continue

                # Discard channel not open after given date
                if(current_selection['after_date']
                   and current_channel.close_date()
                   and current_channel.close_date() < current_selection['after_date']):
                    if verbose:
                        sys.stderr.write("Skip %s.%s.%s.%s (close date)\n" %
                                         (current_channel.network().fdsn_code(),
                                          station_name,
                                          current_channel.location_code(),
                                          current_channel.iris_code()))
                    continue

                # Discard channel not open before given date
                if(current_selection['before_date']
                   and current_channel.open_date() > current_selection['before_date']):
                    if verbose:
                        sys.stderr.write("Skip %s.%s.%s.%s (open date)\n" %
                                         (current_channel.network().fdsn_code(),
                                          station_name,
                                          current_channel.location_code(),
                                          current_channel.iris_code()))
                    continue

                # Filter on seedlink server
                if current_selection['server_selection'] or current_selection['server_exclusion']:
                    current_channel.load("seedlink_channel_config")
                    channel_selected = False
                    for current_config in current_channel.seedlink_channel_config():
                        channel_selected = (channel_selected
                                            or is_selected(current_config.server().code(),
                                                           current_selection['server_selection'],
                                                           current_selection['server_exclusion']))
                        if channel_selected:
                            break
                        if not channel_selected:
                            if verbose:
                                sys.stderr.write("Skip %s.%s.%s.%s (server)\n" %
                                                 (current_channel.network().fdsn_code(),
                                                  station_name,
                                                  current_channel.location_code(),
                                                  current_channel.iris_code()))
                            continue

                # Update network list
                if not current_channel.dbird_network() in networks:
                    networks.append(current_channel.dbird_network())

                current_channel.select()

            # Skip station without selected channel
            if not len(current_station.selected_channel()):
                sys.stderr.write("Skip %s because not selected channel\n" %
                                 station_name)
                continue
            if current_station not in station_list:
                station_list.append(current_station)
    if verbose:
        sys.stderr.write("Stations and channels selection is done\n")
        sys.stderr.write("%s\n" % map(lambda x: x.iris_code(), station_list))
 
    if output_dir:
        for current_station in station_list:
            filepath = os.path.join(output_dir, "%s.dbird" %
                                    current_station.iris_code())
            generate_output_file(networks, [current_station], filepath, 
                                 generation_format, verbose)
    else:
        generate_output_file(networks, station_list, output, 
                             generation_format, verbose)

    if verbose:
        sys.stderr.write("Generation is done\n")


if __name__ == '__main__':
    main()
