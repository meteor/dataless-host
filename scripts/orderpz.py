#!/usr/bin/python
import os
import sys
import json
import argparse

EPSILON = 10e-8


def find_conjugate(value, value_list):
    for current in value_list:
        if (abs(value[0] - current[0]) < EPSILON and abs(value[1] + current[1]) < EPSILON):
            return current
    raise ValueError("Can't find conjugate for %s" % (value,))


def sort_roots(roots_list):
    result = list()
    current_list = roots_list
    while len(current_list) != 0:
        current_root = current_list[0]
        current_list = current_list[1:]
        if current_root[1] < EPSILON:
            result.append(current_root)
            continue
        conjugate = find_conjugate(current_root, current_list)
        current_list.remove(conjugate)
        result.append(current_root)
        result.append(conjugate)
    return result


def main():
    parser = argparse.ArgumentParser(description='Reorder poles and zeros')

    parser.add_argument("-i", "--input", help="Set input filename", required=True)
    parser.add_argument("-o", "--output", help="Set output filename", default=None)
    parser.add_argument("-v", "--verbose", action="count",
                        help="Increase verbosity level", default=0)

    option = parser.parse_args(sys.argv[1:])
    if not os.path.exists(option.input):
        sys.stderr.write("The input file %s does not exists\n" %
                         option.input)
        sys.exit(1)

    # Loading input file
    if option.verbose >= 1:
        print("Loading %s" % option.input)
    try:
        f = open(option.input)
        content = json.load(f)
    except IOError as exception:
        sys.stderr.write("Can't open file %s: %s\n" %
                         (option.input, exception))
        sys.exit(1)
    except ValueError as exception:
        sys.stderr.write("Can't parse JSON in %s: %s\n" %
                         (option.input, exception))
        sys.exit(1)

    # Verify structure
    if "ZeroesDef" not in content:
        sys.stderr.write("Can't find zeros definition (ZeroesDef)\n")
        sys.exit(1)

    if "PolesDef" not in content:
        sys.stderr.write("Can't find poles definition (PolesDef)\n")
        sys.exit(1)

    # Make ordering of poles and zeros
    if option.verbose >= 2:
        print("Sorting zeros")
    try:
        content["ZeroesDef"] = sort_roots(content["ZeroesDef"])
    except Exception as exception:
        sys.stderr.write("Error in ordering zeros: %s\n" % exception)
        sys.exit(1)

    if option.verbose >= 2:
        print("Sorting poles")
    try:
        content["PolesDef"] = sort_roots(content["PolesDef"])
    except Exception as exception:
        sys.stderr.write("Error in ordering poles: %s\n" % exception)
        sys.exit(1)

    # Write ordered poles and zeros
    if option.verbose >= 1:
        print("Writing %s" % option.output)
    try:
        f = open(option.output, "w")
        content = json.dump(content, f, indent=2)
    except IOError as exception:
        sys.stderr.write("Can't open file %s: %s\n" %
                         (option.output, exception))
        sys.exit(1)
    except ValueError as exception:
        sys.stderr.write("Can't dump JSON in %s: %s\n" %
                         (option.output, exception))
        sys.exit(1)


if __name__ == "__main__":
    main()
