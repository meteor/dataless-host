#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import json
import getopt
from oca.database.abstractdb import DatabaseObjectFactory

VERSION = 1.0


def usage():
    """
    Basic usage function
    """
    print(sys.argv[0], "  by Resif Team")
    print("Generate wdataless sensor_list from Phoenix")
    print("")
    print("-H, --host          Set database hostname")
    print("-d, --database      Set database name")
    print("-u, --user          Set user (web by default)")
    print("-p, --password      Set password (no password by default)")
    print("--json              Set JSON output mode")
    print("-o, --output        Set output filename")
    print("-h, --help          Display this message")
    print("-v, --verbose       Enable verbose mode.")
    print("-V, --version       Print version information.")


def main():

    verbose = False
    host = None
    database = None
    username = None
    password = None
    output = None
    generate_json = False
    try:
        opts, args = getopt.getopt(sys.argv[1:], "H:d:o:u:p:n:s:l:c:hvV",
                                   ["host=", "database=", "output=",
                                    "user=", "password=", "json",
                                    "network=", "station=", "location=",
                                    "channel=",
                                    "help", "verbose", "version"])
    except getopt.GetoptError as err:
        print(str(err))  # will print something like "option -current_argument not recognized"
        usage()
        sys.exit(1)

    for current_option, current_argument in opts:
        if current_option in ("-H", "--host"):
            host = current_argument
        elif current_option in ("-d", "--database"):
            database = current_argument
        elif current_option in ("-u", "--user"):
            username = current_argument
        elif current_option in ("-p", "--password"):
            password = current_argument
        elif current_option in ("-o", "--output"):
            output = current_argument
        elif current_option in ("--json",):
            generate_json = True
        elif current_option in ("-h", "--help"):
            usage()
            sys.exit()
        elif current_option in ("-v", "--verbose"):
            verbose = True
        elif current_option == "-V":
            print(sys.argv[0], " version ", VERSION)
        else:
            assert False, "unhandled option"

    # Verify mandatory parameters
    if host is None:
        print("You must specify database hostname with -H")
        sys.exit(1)

    if database is None:
        print("You must specify database name with -d")
        sys.exit(1)

    if username is None:
        print("You must specify database login name with -u")
        sys.exit(1)

    if output is None:
        print("You must specify output filename with -o")
        sys.exit(1)

    try:
        output_file = open(output, "w")
    except IOError as exception:
        sys.stderr.write("Error opening %s: %s" %
                         (output, exception))
        sys.exit(1)

    # Get sensor model list
    database = DatabaseObjectFactory(host, username, database=database,
                                     password=password)
    database.register_type("seed_band_t")
    sensor_model_list = database.get_table_objects("sensor_model")

    if verbose:
        print("Begin %s generation" % output)
    # Now generate file content
    content = list()
    if not generate_json:
        content.append("# Auto-generated file from Phoenix")
        content.append("# Use gensensorlist.py for new generation")
    for current_sensor_model in sensor_model_list:
        current_device_model = current_sensor_model.device_model()
        sensor_code = current_device_model.code()
        family = current_sensor_model.sensor_family().code()
        band = current_sensor_model.seed_band().sensor_type

        current_device_model.load("device_model_super_type")
        if len(current_device_model.device_model_super_type()) == 0:
            print("Can't find super type for sensor %s" % sensor_code)
            continue
        sensor_super_type = list(current_device_model.device_model_super_type())[0].super_type()
        description = "no description"

        if(sensor_super_type.description() not in (None, "")):
            description = sensor_super_type.description().encode('utf8')

        if generate_json:
            new_sensor = {
                "DbirdCode": sensor_code,
                "SeedCode": sensor_super_type.code(),
                "Type": family,
                "Band": band,
                "Description": description
            }
            content.append(new_sensor)
        else:
            content.append("%s %s %s %s %s" %
                           (sensor_code, sensor_super_type.code(),
                            family, band, description))

    if generate_json:
        json.dump(content, output_file, indent=2)
    else:
        output_file.write("\n".join(content))
    output_file.close()


if __name__ == '__main__':
    main()
