#!/usr/bin/python

import sys
import getopt
from pzbank.loader import PZAccess
from oca.database.abstractdb import DatabaseObjectFactory
from phoenix.core.phoenix_query import PhoenixQuery

VERSION = 0.1


def usage():
    """
    Basic usage function
    """
    print(sys.argv[0], "  by Resif Team")
    print("Make some verification on PZ databank")
    print("")
    print("-H, --host          Set database hostname")
    print("-d, --database      Set database name")
    print("-u, --user          Set user (web by default)")
    print("-p, --password      Set password (no password by default)")
    print("--pzroot            Set PZ root directory")
    print("-h, --help          Display this message")
    print("-v, --verbose       Enable verbose mode.")
    print("-V, --version       Print version information.")


def verify_sensor_existence(phoenix_query, databank):
    database = phoenix_query.get_database()
    sensor_databank = databank[PZAccess.SENSOR_METATYPE]
    for current_manufacturer in sensor_databank:
        for current_model in sensor_databank[current_manufacturer]:
            for current_config in sensor_databank[current_manufacturer][current_model]:
                for current_serial in sensor_databank[current_manufacturer][current_model][current_config]:
                    if current_serial == PZAccess.THEORETICAL:
                        if phoenix_query.load_sensor_model(current_model.upper()) is None:
                            sys.stderr.write("Can't find model %s.%s in database\n" %
                                             (current_manufacturer,
                                              current_model.upper()))

                    else:
                        result = database.get_table_objects("device",
                                                            ("serial_number",
                                                             current_serial),
                                                            None)
                        if not result:
                            sys.stderr.write("Can't find instance %s.%s.%s in database\n" %
                                             (current_manufacturer,
                                              current_model,
                                              current_serial))
                        valid_result = None
                        for current_result in result:
                            if current_result.device_model().code() == current_model.upper():
                                valid_result = current_result
                                break

                        if valid_result is None:
                            sys.stderr.write("Can't find %s.%s.%s in database\n" %
                                             (current_manufacturer,
                                              current_model,
                                              current_serial))
                            continue

                        if valid_result.device_model().manufacturer().code() != current_manufacturer:
                            sys.stderr.write("For instance %s.%s.%s expecting manufacturer %s\n" %
                                             (current_manufacturer,
                                              current_model,
                                              current_serial,
                                              result[0].device_model().manufacturer().code()))


def verify_analog_filter_existence(phoenix_query, databank):
    database = phoenix_query.get_database()
    analog_filter_databank = databank[PZAccess.ANALOG_FILTER_METATYPE]
    for current_manufacturer in analog_filter_databank:
        for current_model in analog_filter_databank[current_manufacturer]:
            for current_filter in analog_filter_databank[current_manufacturer][current_model]:
                for current_serial in analog_filter_databank[current_manufacturer][current_model][current_filter]:
                    if current_serial == PZAccess.THEORETICAL:
                        if current_filter == "":
                            # analog filter
                            if phoenix_query.load_analog_filter_model(current_model.upper()) is None:
                                sys.stderr.write("Can't find analog filter %s.%s in database\n" %
                                                 (current_manufacturer,
                                                  current_model))
                        else:
                            # Device analog filter

                            # First try with sensor
                            sensor_model = phoenix_query.load_sensor_model(current_model.upper())
                            if sensor_model is None:
                                # Now try with das
                                das_model = phoenix_query.load_das_model(current_model.upper())
                                if das_model is None:
                                    sys.stderr.write("Can't find sensor or das with model=%s\n" %
                                                     (current_model))
                                    continue
                                # Das analog filter
                                find_filter = False
                                das_model.load("das_analog_filter")
                                for filter in das_model.das_analog_filter():
                                    if filter.code() == current_filter:
                                        find_filter = True
                                        break
                                if not find_filter:
                                    sys.stderr.write("Can't find filter %s for das model %s\n" %
                                                     (current_filter, current_model))
                            else:
                                # Sensor analog filter
                                find_filter = False
                                sensor_model.load("sensor_analog_filter")
                                for filter in sensor_model.sensor_analog_filter():
                                    if filter.code() == current_filter:
                                        find_filter = True
                                        break
                                if not find_filter:
                                    sys.stderr.write("Can't find filter %s for sensor model %s\n" %
                                                     (current_filter, current_model))
                    else:
                        result = database.get_table_objects("device",
                                                            ("serial_number",
                                                             current_serial),
                                                            None)
                        if not result:
                            sys.stderr.write("Can't find instance %s.%s.%s in database\n" %
                                             (current_manufacturer,
                                              current_model,
                                              current_serial))
                        valid_result = None
                        for current_result in result:
                            if current_result.device_model().code() == current_model.upper():
                                valid_result = current_result
                                break

                        if valid_result is None:
                            sys.stderr.write("Can't find %s.%s.%s in database\n" %
                                             (current_manufacturer,
                                              current_model,
                                              current_serial))
                            continue

                        if valid_result.device_model().manufacturer().code() != current_manufacturer:
                            sys.stderr.write("For instance %s.%s.%s expecting manufacturer %s\n" %
                                             (current_manufacturer,
                                              current_model,
                                              current_serial,
                                              result[0].device_model().manufacturer().code()))


def verify_das_existence(phoenix_query, databank):
    database = phoenix_query.get_database()
    das_databank = databank[PZAccess.DIGITIZER_METATYPE]
    for current_manufacturer in das_databank:
        for current_model in das_databank[current_manufacturer]:
            for current_config in das_databank[current_manufacturer][current_model]:
                for current_serial in das_databank[current_manufacturer][current_model][current_config]:
                    if current_serial == PZAccess.THEORETICAL:
                        if phoenix_query.load_das_model(current_model.upper()) is None:
                            sys.stderr.write("Can't find model %s.%s in database\n" %
                                             (current_manufacturer,
                                              current_model.upper()))

                    else:
                        result = database.get_table_objects("device",
                                                            ("serial_number",
                                                             current_serial),
                                                            None)
                        if not result:
                            sys.stderr.write("Can't find instance %s.%s.%s in database\n" %
                                             (current_manufacturer,
                                              current_model,
                                              current_serial))
                        valid_result = None
                        for current_result in result:
                            if current_result.device_model().code() == current_model.upper():
                                valid_result = current_result
                                break

                        if valid_result is None:
                            sys.stderr.write("Can't find %s.%s.%s in database\n" %
                                             (current_manufacturer,
                                              current_model,
                                              current_serial))
                            continue

                        if valid_result.device_model().manufacturer().code() != current_manufacturer:
                            sys.stderr.write("For instance %s.%s.%s expecting manufacturer %s\n" %
                                             (current_manufacturer,
                                              current_model,
                                              current_serial,
                                              result[0].device_model().manufacturer().code()))


def verify_digital_filter_existence(phoenix_query, databank):
    database = phoenix_query.get_database()
    digital_filter_databank = databank[PZAccess.DIGITAL_FILTER_METATYPE]
    for current_manufacturer in digital_filter_databank:
        manufacturer = database.get_table_objects("manufacturer",
                                                  ("code",
                                                   current_manufacturer),
                                                  None)
        if not manufacturer:
            # If manufacturer is unknown assume is a software digital filter
            soft_filter = database.get_table_objects("software_digital_filter",
                                                     ("code",
                                                      current_manufacturer),
                                                     None)
            if not soft_filter:
                sys.stderr.write("%s is not a manufacturer or a software digital filter\n" %
                                 current_manufacturer)
            continue
        # Find manufacturer assume is a device digital filter
        for current_model in digital_filter_databank[current_manufacturer]:
            for current_filter in digital_filter_databank[current_manufacturer][current_model]:
                dig_filter = phoenix_query.load_digital_filter_model(current_model.upper())
                if dig_filter is None:
                    # Digitizer digital filter
                    das_model = phoenix_query.load_das_model(current_model.upper())
                    if das_model is None:
                        sys.stderr.write("Can't find das or digital filter with model=%s\n" %
                                         (current_model))
                        continue
                    # Das digital filter
                    find_filter = False
                    das_model.load("das_digital_filter")
                    for filter in das_model.das_digital_filter():
                        if filter.code() == current_filter:
                            find_filter = True
                            break
                    if not find_filter:
                        sys.stderr.write("Can't find filter %s for das model %s\n" %
                                         (current_filter, current_model))
                else:
                    # Digital filter
                    dig_filter.load("digital_filter_config")
                    find_config = False
                    for current_config in dig_filter.digital_filter_config():
                        if current_config.code() == current_filter:
                            find_config = True
                            break
                    if not find_config:
                        sys.stderr.write("Can't find configuration %s for digital filter model %s\n",
                                         (current_model, current_filter))


def main():

    verbose = False
    host = None
    database = None
    username = None
    password = None
    pzroot = None

    try:
        opts, args = getopt.getopt(sys.argv[1:], "H:d:u:p:hvV",
                                   ["host=", "database="
                                    "user=", "password=", "pzroot=",
                                    "help", "verbose", "version"])
    except getopt.GetoptError as err:
        print(str(err))  # will print something like "option -current_argument not recognized"
        usage()
        sys.exit(1)

    for current_option, current_argument in opts:
        if current_option in ("-H", "--host"):
            host = current_argument
        elif current_option in ("-d", "--database"):
            database = current_argument
        elif current_option in ("-u", "--user"):
            username = current_argument
        elif current_option in ("-p", "--password"):
            password = current_argument
        elif current_option in ("--pzroot"):
            pzroot = current_argument
        elif current_option in ("-h", "--help"):
            usage()
            sys.exit()
        elif current_option in ("-v", "--verbose"):
            verbose = True
        elif current_option == "-V":
            print(sys.argv[0], " version ", VERSION)
        else:
            assert False, "unhandled option"

    # Verify mandatory parameters
    if host is None:
        print("You must specify database hostname with -H")
        sys.exit(1)

    if database is None:
        print("You must specify database name with -d")
        sys.exit(1)

    if username is None:
        print("You must specify database login name with -u")
        sys.exit(1)

    if pzroot is None:
        print("You must specify PZ root directory with --pzroot")
        sys.exit(1)

    database = DatabaseObjectFactory(host, username, database=database,
                                     password=password)
    phoenix_query = PhoenixQuery(database)

#    database.register_type("time_period_t")
    database.register_type("sensor_component_t")

    pzaccess = PZAccess(pzroot)
    try:
        pzaccess.load_database()
    except ValueError as exception:
        sys.stderr.write("Error: %s\n" % exception)
        sys.exit(1)

    # Now verify that all element of databank is also present in database.
    databank = pzaccess.get_databank()
    if verbose:
        sys.stderr.write("\n")
        sys.stderr.write("**Verify sensor existence\n")
    verify_sensor_existence(phoenix_query, databank)
    if verbose:
        sys.stderr.write("\n")
        sys.stderr.write("\n")

    if verbose:
        sys.stderr.write("**Verify analog filter existence\n")
    verify_analog_filter_existence(phoenix_query, databank)
    if verbose:
        sys.stderr.write("\n")
        sys.stderr.write("\n")

    if verbose:
        sys.stderr.write("**Verify digitizer existence\n")
    verify_das_existence(phoenix_query, databank)
    if verbose:
        sys.stderr.write("\n")
        sys.stderr.write("\n")

    if verbose:
        sys.stderr.write("**Verify digital filter existence\n")
    verify_digital_filter_existence(phoenix_query, databank)
    if verbose:
        sys.stderr.write("\n")
        sys.stderr.write("\n")


if __name__ == '__main__':
    main()
