#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import sys
import json
import getopt
from oca.jinjautils.filters import empty
from jinja2.loaders import PackageLoader
from jinja2.environment import Environment
from respparse.respparser import RespParser
from oca.jinjautils.filters import dbirddate


def sensor_function_type(function_type):
    if function_type == 'A':
        return "LAPLACE"
    elif function_type == 'B':
        return "ANALOG"
    else:
        raise NotImplementedError("Sensor function type %s not yet managed" %
                                  function_type)


def digital_filter_symmetry(dataless_code):
    if dataless_code == 'A':
        return "FIR_ASYM"
    elif dataless_code == 'B':
        return "FIR_SYM_1"
    elif dataless_code == 'C':
        return "FIR_SYM_2"
    else:
        raise NotImplementedError("The digital filter symmetry code is unknown <%s>" % dataless_code)


def init_directory(dir_path, verbose):
    if os.path.exists(dir_path):
        if not os.path.isdir(dir_path):
            print("% is not a directory" % dir_path)
            sys.exit(1)
    else:
        if verbose:
            print("Create PZ directory %s" % dir_path)
        try:
            os.mkdir(dir_path)
        except IOError as exception:
            print("Error while creating PZ directory (%s): %s" %
                  (dir_path, exception))
            sys.exit(1)


def init_pz(pz_dir, verbose):
    # Verify PZ directory existence
    init_directory(pz_dir, verbose)

    sensor_path = "%s/%s" % (pz_dir, "sensor")
    init_directory(sensor_path, verbose)

    datalogger_path = "%s/%s" % (pz_dir, "digitizer")
    init_directory(datalogger_path, verbose)

    dig_filter_path = "%s/%s" % (pz_dir, "dig_filter")
    init_directory(dig_filter_path, verbose)


def usage():
    """
    A basic usage function
    """
    print("%s -r <resp_file> -d <pz directory> -o <output.meteor> -s <station code> [-v] [-h]" %
          os.path.basename(sys.argv[0]))
    print("")
    print("-r, --resp    -- Set RESP file (rdseed -sf <dataless file>)")
    print("-d, --pz      -- Set the PZ directory")
    print("-o, --output  -- Set the name of the meteor file")
    print("-v, --verbose -- Enable verbose mode")
    print("-h, --help    -- Print this help")


def main():
    resp_name = None
    resp_file = None
    pz_dir = None
    output_file = None
    verbose = False
    try:
        opts, args = getopt.getopt(sys.argv[1:],
                                   "r:d:o:vh",
                                   ['resp=', 'pz=', 'output=',
                                    'verbose', 'help'])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(str(err))  # will print something like "option -argument not recognized"
        usage()
        sys.exit(1)

    for option, argument in opts:
        if option in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif option in ("-r", "--resp"):
            resp_name = argument
        elif option in ("-d", "--pz"):
            pz_dir = argument
        elif option in ("-o", "--output"):
            output_file = argument
        elif option in ("-v", "--verbose"):
            verbose = True
        else:
            assert False, "unhandled option"

    if pz_dir is None:
        print("You must specify PZ directory with -d")
        sys.exit(1)

    if resp_name is None:
        print("You must specify RESP file code with -r")
        sys.exit(1)

    if output_file is None:
        print("You must specify output meteor file code with -o")
        sys.exit(1)

    try:
        if verbose:
            print("Opening %s..." % resp_name, end="")
        current_file = open(resp_name)
        if verbose:
            print("ok")
        resp_file = current_file
    except IOError as exception:
        print("Error opening %s : %s" % (resp_name, exception))
        sys.exit(1)

    init_pz(pz_dir, verbose)

    parser = RespParser(resp_file, pz_dir, verbose)
    result = parser.parse()

    environement = Environment(loader=PackageLoader('respparse',
                                                    'templates'),
                               line_statement_prefix="%",
                               trim_blocks=True)
    environement.filters['sensor_function_type'] = sensor_function_type
    environement.filters['digital_filter_symmetry'] = digital_filter_symmetry
    environement.tests['empty'] = empty

    sensor_template = environement.get_template('sensor.tpl')
    datalogger_template = environement.get_template('datalogger.tpl')
    digital_filter_template = environement.get_template('digital_filter.tpl')

    sensor_list = []
    for current_station in result:
        current_station.verify(verbose)

        for current_channel in current_station.channels():
            # Write sensor file
            sensor_filename = ("sensor/unknown-manufacturer#unknown-model##unknown-serial#%s#%s#%s" %
                               (current_channel.iris_code()[0],
                                dbirddate(current_channel.open_date()),
                                dbirddate(current_channel.close_date())))
            current_sensor_file = open(os.path.join(pz_dir, sensor_filename),
                                       "w")
            sensor = current_channel.get_set_sensor()
            sensor_list.append(sensor)
            sensor.set_pzfile(sensor_filename)
            current_sensor_file.write(sensor_template.render(sensor=sensor))
            current_sensor_file.close()

            # Write datalogger file
            datalogger_filename = ("digitizer/unknown-manufacturer#unknown-model##unknown-serial#%s#%s#%s" %
                                   (current_channel.iris_code()[0],
                                    dbirddate(current_channel.open_date()),
                                    dbirddate(current_channel.close_date())))
            current_datalogger_file = open(os.path.join(pz_dir, datalogger_filename),
                                           "w")
            datalogger = current_channel.get_set_datalogger()
            datalogger.set_pzfile(datalogger_filename)
            current_datalogger_file.write(datalogger_template.render(datalogger=datalogger))
            current_datalogger_file.close()

            # Write digital_filter files
            digital_filter_filename = ("dig_filter/unknown-manufacturer#unknown-model#unknown-filter-%s-%s-%s" %
                                       (dbirddate(current_channel.open_date()),
                                        dbirddate(current_channel.close_date()),
                                        ))
            current_dig_filter_file = open("%s/%s" % (pz_dir, digital_filter_filename),
                                           "w")
            digital_filter = current_channel.get_digital_filter()
            digital_filter.set_pzfile(digital_filter_filename)
            current_dig_filter_file.write(digital_filter_template.render(digital_filter_cascade=digital_filter))
            current_dig_filter_file.close()

    # Generate Meteor file
    meteor_content = {}
    meteor_file = open(output_file, "w")
    json.dump(meteor_content, meteor_file)

    # Generate sensor_list file
    sensor_list_file = open("%s/sensor_list" % pz_dir, "w")
    for sensor in sensor_list:
        input_unit = sensor.get_input_unit()
        if input_unit.lower() == "m/s":
            sensor_type = "VEL"
        elif input_unit.lower() == "m/s/s":
            sensor_type = "ACC"
        else:
            raise ValueError("Input unit <%s> is not recognized" % input_unit)

        sensor_list_file.write("%s %s LP %s\n" % (sensor.get_model(),
                                                  sensor_type,
                                                  sensor.get_model()))
    sensor_list_file.close()


if __name__ == '__main__':
    main()
