begin sensor
  Response_name : "SENSOR RESPONSE"
  Response_type : "THEORETICAL"
  Transfer_function_type : "{{ sensor.get_response_type() | sensor_function_type }}"
  Input_unit : "{{ sensor.get_input_unit() }}"
  Output_unit : "V"
  Sensitivity : {{ sensor.get_sensitivity() }}
  Transfer_normalization_frequency : {{ sensor.get_normalization_freq() }}
  Number_of_zeroes    : {{ sensor.get_zeros() | length }}
{% for current in sensor.get_zeros() %}
    {{current[0]}} {{current[1]}}
{% endfor %}
  Number_of_poles     : {{sensor.get_poles() | length}}
{% for current in sensor.get_poles() %}
    {{current[0]}} {{current[1]}}
{% endfor %}
end sensor
