begin digitizer
  Response_name : "DIGITIZER"
  Response_type : "THEORETICAL"
  Transfer_function_type : "AD_CONVERSION"
  Input_unit : "V"
  Output_unit : "COUNTS"
  Output_sampling_interval : {{ datalogger.get_output_sps() }}
  Sensitivity : {{ datalogger.get_sensitivity() }}
end digitizer
