
begin digital_filter
  Response_name : "DECIMATION"
  Response_type : "THEORETICAL"
  Transfer_function_type : "{{digital_filter_cascade.first_stage().get_symmetry_type() | digital_filter_symmetry}}"
  Input_sampling_interval : {{digital_filter_cascade.first_stage().get_input_sps() }}
  Decimation_factor : {{ digital_filter_cascade.first_stage().get_decimation_factor() }}
  Correction : {{ digital_filter_cascade.first_stage().get_correction() }}
  Delay : {{ digital_filter_cascade.first_stage().get_delay() }}
  Number_of_coefficients  : {{ digital_filter_cascade.first_stage().get_numerators() | length }}
  {% for current in digital_filter_cascade.first_stage().get_numerators() %}
    {{current}}
  {% endfor %}
end digital_filter

{% for current_stage in digital_filter_cascade.cascade() %}
begin stage
  Response_name : "DECIMATION"
  Response_type : "THEORETICAL"
  Transfer_function_type : "{{current_stage.get_symmetry_type() | digital_filter_symmetry}}"
  Input_sampling_interval : {{ current_stage.get_input_sps() }}
  Decimation_factor : {{current_stage.get_decimation_factor()}}
  Correction : {{ current_stage.get_correction() }}
  Delay : {{ current_stage.get_delay() }}
  Number_of_coefficients  : {{ current_stage.get_numerators() | length }}
  {% for current in current_stage.get_numerators() %}
    {{current}}
  {% endfor %}
end stage
{% endfor %}
