# -*- coding: utf-8 -*-


class PZBlk(object):

    def __init__(self, definition):
        self.__definition = definition

    def response_type(self):
        """
        Give Response type of this blockette
        """
        return self.__definition["response_type"]

    def input_unit(self):
        """
        Give input unit.
        """
        return self.__definition["input_unit"]

    def output_unit(self):
        """
        Give output unit.
        """
        return self.__definition["output_unit"]

    def a0(self):
        """
        Give A0 value of this PZ blockette
        """
        self.__definition["A0"]

    def normalization_frequency(self):
        """
        Give normalization frequency
        """
        return self.__definition["normalization_frequency"]

    def zeros(self):
        """
        Give the list of zeros of this PZ blockette.
        The element list have the form
        (<real part>, <imaginary part>,
         <real part error>, <imaginary part error>)
        """
        return self.__definition["zeros"]

    def poles(self):
        """
        Give the list of poles of this PZ blockette.
        The element list have the form
        (<real part>, <imaginary part>,
         <real part error>, <imaginary part error>)
        """
        return self.__definition["poles"]


class CoefBlk(object):
    def __init__(self, definition):
        self.__definition = definition

    def response_type(self):
        """
        Give Response type of this blockette
        """
        return self.__definition["response_type"]

    def input_unit(self):
        """
        Give input unit.
        """
        return self.__definition["input_unit"]

    def output_unit(self):
        """
        Give output unit.
        """
        return self.__definition["output_unit"]

    def numerators(self):
        """
        Give the list of numerators of this Coeficients blockette.
        The element list have the form
        (<numerator value>, <numerator error>)
        """
        return self.__definition["numerators"]

    def denominators(self):
        """
        Give the list of denominators of this Coeficients blockette.
        The element list have the form
        (<denominator value>, <denominator error>)
        """
        return self.__definition["denominators"]


class DecimBlk(object):
    def __init__(self, definition):
        self.__definition = definition

    def input_sps(self):
        """
        Give input sample rate of this stage
        """
        return self.__definition["input_sps"]

    def decimation_factor(self):
        """
        Give the decimation factor of this stage
        """
        return self.__definition["decimation_factor"]

    def decimation_offset(self):
        """
        Give the decimation offset of this stage
        """
        return self.__definition["decimation_offset"]

    def delay(self):
        """
        Give the delay of this stage
        """
        return self.__definition["delay"]

    def correction(self):
        """
        Give the correction of this stage
        """
        return self.__definition["correction"]


class GainBlk(object):
    def __init__(self, definition):
        self.__definition = definition

    def sensitivity(self):
        """
        Give the sensitivity of this stage
        """
        return self.__definition["sensitivity"]

    def frequency(self):
        """
        Give the frequency of this sensitivity blockette
        """
        return self.__definition["frequency"]


class FirBlk(object):
    def __init__(self, definition):
        self.__definition = definition

    def name(self):
        """
        Give the name associated with this FIR blockette
        """
        return self.__definition["name"]

    def symmetry(self):
        """
        Give the symmetry code of this FIR blockette
        """
        return self.__definition["symmetry"]

    def input_unit(self):
        """
        Give input unit.
        """
        return self.__definition["input_unit"]

    def output_unit(self):
        """
        Give output unit.
        """
        return self.__definition["output_unit"]

    def coefficients(self):
        """
        Give the list of coefficients of this FIR filter
        """
        return self.__definition["coeficients"]


class PolyBlk(object):
    def __init__(self, definition):
        self.__definition = definition

    def input_unit(self):
        """
        Give input unit.
        """
        return self.__definition["input_unit"]

    def output_unit(self):
        """
        Give output unit.
        """
        return self.__definition["output_unit"]

    def approximation_type(self):
        """
        Give the approximation type of this polynomial blockette
        """
        return self.__definition["approximation_type"]

    def valid_frequency_unit(self):
        """
        Give the valid frequency unit of this polynomial blockette
        """
        return self.__definition["valid_frequency_unit"]

    def lower_valid_frequency_bound(self):
        """
        Give the lower valid frequency bound of this polynomial blockette
        """
        return self.__definition["lower_valid_frequency_bound"]

    def upper_valid_frequency_bound(self):
        """
        Give the upper valid frequency bound of this polynomial blockette
        """
        return self.__definition["upper_valid_frequency_bound"]

    def lower_bound_approximation(self):
        """
        Give the lower bound approximation of this polynomial blockette
        """
        return self.__definition["lower_bound_approximation"]

    def upper_bound_approximation(self):
        """
        Give the upper bound approximation of this polynomial blockette
        """
        return self.__definition["upper_bound_approximation"]

    def maximum_absolute_error(self):
        """
        Give the maximum absolute error if this polynomial blockette
        """
        return self.__definition["maximum_absolute_error"]

    def coefficients(self):
        """
        Give the list of coefficients of this polynomial blockette
        """
        return self.__definition["coeficients"]


class Stage(object):

    def __init__(self, sequence_number):
        """
        Initialize this Stage

        sequence_number -- The sequence number of this Stage
        """
        self.__sequence_number = sequence_number
        self.__pz_blk = None
        self.__coef_blk = None
        self.__decim_blk = None
        self.__gain_blk = None
        self.__fir_blk = None
        self.__poly_blk = None

    def sequence_number(self):
        """
        Give the sequence number of this Stage
        """
        return self.__sequence_number

    def add_pz(self, pz_blk):
        """
        Add poles and zeros blockette definition to this stage

        pz_blk -- A poles and zeros blockette as dictionnary
        """
        self.__pz_blk = PZBlk(pz_blk)

    def pz_blk(self):
        """
        Give PZBlk associated to this stage or None
        """
        return self.__pz_blk

    def add_coef(self, coef_blk):
        """
        Add Coefficient blockette definition to this stage

        coef_blk -- A coefficient blockette as dictionnary
        """
        self.__coef_blk = CoefBlk(coef_blk)

    def coef_blk(self):
        """
        Give CoefBlk associated to this stage or None
        """
        return self.__coef_blk

    def add_decim(self, decim_blk):
        """
        Add PZ blockette definition to this stage

        decim_blk -- A decimation blockette as dictionnary
        """
        self.__decim_blk = DecimBlk(decim_blk)

    def decim_blk(self):
        """
        Give DecimBlk associated to this stage or None
        """
        return self.__decim_blk

    def add_gain(self, gain_blk):
        """
        Add sensitivity/gain blockette definition to this stage

        gain_blk -- A sensitivity/gain blockette as dictionnary
        """
        self.__gain_blk = GainBlk(gain_blk)

    def gain_blk(self):
        """
        Give GainBlk associated to this stage or None
        """
        return self.__gain_blk

    def add_fir(self, fir_blk):
        """
        Add FIR response blockette definition to this stage

        fir_blk -- A FIR response blockette as dictionnary
        """
        self.__fir_blk = FirBlk(fir_blk)

    def fir_blk(self):
        """
        Give FirBlk associated to this stage or None
        """
        return self.__fir_blk

    def add_poly(self, poly_blk):
        """
        Add polynomial response blockette definition to this stage

        poly_blk -- A polynomial response blockette as dictionnary
        """
        self.__poly_blk = PolyBlk(poly_blk)

    def poly_blk(self):
        """
        Give PolyBlk associated to this stage or None
        """
        return self.__poly_blk
