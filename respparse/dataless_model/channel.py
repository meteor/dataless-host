# -*- coding: utf-8 -*-


class Channel(object):

    def __init__(self, iris_code, location, open_date, close_date, network_code):
        """
        Initialize this dataless channel

        iris_code    -- The channel IRIS code
        location     -- The channel location
        open_date    -- The channel open date
        close_date   -- The channel close date or None if channel is open
        network_code -- The channel network code
        """
        self.__iris_code = iris_code
        self.__location = location
        self.__open_date = open_date
        self.__close_date = close_date
        self.__network_code = network_code
        self.__stages = list()
        self.__comments = list()

    def iris_code(self):
        return self.__iris_code

    def location(self):
        return self.__location

    def open_date(self):
        return self.__open_date

    def close_date(self):
        return self.__close_date

    def network_code(self):
        return self.__network_code

    def set_flags(self, flags_list):
        """
        Set channel flags list
        """
        self.__flags_list = flags_list

    def flags(self):
        """
        Give the flags list of this channel
        """
        return self.__flags_list

    def set_format(self, format):
        """
        Set channel format
        """
        self.__format = format

    def format(self):
        """
        Give the format of this channel
        """
        return self.__format

    def add_stage(self, stage):
        """
        Add a new stage to this channel

        stage -- The new stage to add
        """
        self.__stages.append(stage)

    def stages(self):
        """
        Give the list of stages for this channel.
        """
        return self.__stages

    def add_comment(self, comment):
        """
        Add a new comment for this channel

        comment -- The new comment
        """
        self.__comments.append(comment)

    def verify(self, verbose):
        if verbose:
            print("Channel %s : location = %s open date = %s close date = %s" %
                  (self.__code, self.__location_code, self.__open_date,
                   self.__close_date))
        assert(self.__code and self.__sensor and self.__datalogger and
               self.__open_date)
        # TODO Add stages verification
        self.__sensor.verify(verbose)
        self.__datalogger.verify(verbose)
        self.__digital_filter_cascade.verify(verbose)
