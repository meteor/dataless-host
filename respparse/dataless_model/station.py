# -*- coding: utf-8 -*-
from respparse.dataless_model.channel import Channel
from respparse.dataless_model.location import Location


class Station(object):

    def __init__(self, iris_code, latitude, longitude, elevation, first_install, close_date):
        """
        Initialize this station

        iris_code     -- The station IRIS code
        latitude      -- The station latitude
        longitude     -- The station longitude
        elevation     -- The station elevation
        first_install -- The station first installation date
        close_date    -- The station close date or None if station is open
        """
        self.__iris_code = iris_code
        self.__latitude = latitude
        self.__longitude = longitude
        self.__elevation = elevation
        self.__name = None
        self.__owner = None
        self.__first_install = first_install
        self.__close_date = close_date
        self.__locations = list()
        self.__channels = list()
        self.__comments = list()
        self.__sensors = None
        self.__dataloggers = None
        self.__digital_filters = None

    def iris_code(self):
        """
        Give the code of this station.

        return -- The station code.
        """
        return self.__code

    def latitude(self):
        """
        This station latitude.
        """
        return self.__latitude

    def longitude(self):
        """
        This station longitude.
        """
        return self.__longitude

    def elevation(self):
        """
        This station elevation.
        """
        return self.__elevation

    def set_name(self, name):
        """
        Set the name of this station.
        """
        self.__name = name

    def name(self):
        return self.__name

    def set_owner(self, owner):
        """
        Set the owner of this station.
        """
        self.__owner = owner

    def owner(self):
        return self.__owner

    def first_install(self):
        """
        This station first installation date.
        """
        return self.__first_install

    def close_date(self):
        """
        This station close date or datetime.max if open
        """
        return self.__close_date

    def new_channel(self, code, location):
        """
        Add a channel to this station.

        code          -- The code of the channel
        location_code -- The location of the channel

        return        -- The newly created channel
        """
        if location not in self.__locations:
            raise ValueError("Location %s : %s %s %s %s is not defined for station %s" %
                             (location.code(), location.latitude(), location.longitude(),
                              location.elevation(), location.depth(), self.__code))
        new_cha = Channel(code, location)
        self.__channels.append(new_cha)
        return new_cha

    def channels(self):
        """
        Give channels list of this station.
        """
        return self.__channels

    def add_comment(self, comment):
        """
        Add a new comment for this station

        comment -- The new comment for this station
        """
        self.__comments.append(comment)

    def comments(self):
        """
        Give the list of comment associated with this station
        """
        return self.__comments

    def get_location(self, code, latitude, longitude, elevation, depth):
        """
        Give the location with given values or create a new one.

        code      -- The location code
        latitude  -- The location latitude
        longitude -- The location longitude
        elevation -- The location elevation
        depth     -- The location depth

        return    -- The location with given values
        """
        for current_location in self.__locations:
            if(current_location.code() == code and
               current_location.latitude() == latitude and
               current_location.longitude() == longitude and
               current_location.elevation() == elevation and
               current_location.depth() == depth):
                return current_location
        # Location with given values don't exist create a new one.
        new_location = Location(code, latitude, longitude, elevation, depth)
        self.__locations.append(new_location)
        return new_location

    def locations(self):
        """
        Give the list of location associated with this station
        """
        return self.__locations

    def verify(self, verbose):
        if verbose:
            print("Station %s : latitude = %s longitude = %s elevation = %s name = %s network = %s" %
                  (self.__code, self.__latitude, self.__longitude,
                   self.__elevation, self.__name, self.__network))
        assert(self.__code and self.__latitude and self.__longitude and
               self.__elevation and self.__name and self.__network)
        for location in self.__locations:
            location.verify(verbose)
