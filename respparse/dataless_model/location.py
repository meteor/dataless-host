# -*- coding: utf-8 -*-


class Location(object):

    def __init__(self, code, latitude, longitude, elevation, depth):
        """
        Initialize this dataless Location

        code      -- The location code
        latitude  -- The latitude of this Location
        longitude -- The longitude of this Location
        elevation -- The elevation of this Location
        depth     -- The depth of this Location
        """
        self.__code = code
        self.__latitude = latitude
        self.__longitude = longitude
        self.__elevation = elevation
        self.__depth = depth

    def code(self):
        """
        Give the code of this Location
        """
        return self.__code

    def latitude(self):
        """
        Give the latitude of this Location
        """
        return self.__latitude

    def longitude(self):
        """
        Give the longitude of this Location
        """
        return self.__longitude

    def elevation(self):
        """
        Give the elevation of this Location
        """
        return self.__elevation

    def depth(self):
        """
        Give the depth of this Location
        """
        return self.__depth

    def verify(self, verbose):
        """
        Verify this Location validity.

        verbose -- The verbose mode
        """
        if verbose:
            print("Location %s : latitude = %s longitude = %s elevation = %s depth = %s" %
                  (self.__code, self.__latitude, self.__longitude,
                   self.__elevation, self.__depth))
        assert(self.__code is not None and self.__latitude and self.__longitude and
               self.__elevation and self.__depth is not None)
        for current_channel in self.__channels:
            current_channel.verify(verbose)
