#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
from datetime import datetime
from respparser.dataless_model.stage import Stage
from respparse.dataless_model.station import Station


class RespParserException(BaseException):
    def __init__(self, *arg, **kargs):
        BaseException.__init__(self, arg, kargs)


class RespParser(object):
    """
    Basic parser of RESP file.
    """

    def __init__(self, resp_file, pz_dir, verbose=False):
        self.__resp_file = resp_file
        self.__pz_dir = pz_dir
        self.__station_list = []
        self.__current_stage = None
        self.__current_station = None
        self.__current_network = None
        self.__verbose = verbose

    def parse(self):
        """
        Parse RESP file and return corresponding stations list structure

        return -- The list of stations
        """

        resp_lines = self.__resp_file.readlines()

        while(resp_lines):
            current_line = resp_lines[0]
            current_line = current_line.strip()

            # Skip empty line and comment line
            if not current_line or current_line[0] == '#':
                resp_lines.pop(0)
                continue

            matching = re.match("B([0-9]+)F[0-9]+", current_line)
            if not matching:
                raise RespParserException("Can't parse <%s>" % current_line)
            blockette = int(matching.groups()[0])
            if self.__verbose:
                print("Read blockette %s" % blockette)
            if blockette == 10:
                self._blk10(resp_lines)
            elif blockette == 11:
                self._blk11(resp_lines)
            elif blockette == 30:
                self._blk30(resp_lines)
            elif blockette == 41:
                self._blk41(resp_lines)
            elif blockette == 43:
                self._blk43(resp_lines)
            elif blockette == 44:
                self._blk44(resp_lines)
            elif blockette == 47:
                self._blk47(resp_lines)
            elif blockette == 48:
                self._blk48(resp_lines)
            elif blockette == 50:
                self._blk50(resp_lines)
            elif blockette == 51:
                self.__blk51(resp_lines)
            elif blockette == 52:
                self._blk52(resp_lines)
            elif blockette == 53:
                self._blk53(resp_lines)
            elif blockette == 54:
                self._blk54(resp_lines)
            elif blockette == 57:
                self._blk57(resp_lines)
            elif blockette == 58:
                self._blk58(resp_lines)
            elif blockette == 59:
                self._blk59(resp_lines)
            elif blockette == 60:
                self._blk60(resp_lines)
            elif blockette == 61:
                self._blk61(resp_lines)
            elif blockette == 62:
                self._blk62(resp_lines)
            else:
                raise NotImplementedError("Management of blockette %s is not yet implemented" % blockette)

        return self.__station_list

    def _get_station(self, station_code):
        """
        Give the station with the given IRIS code.
        If the station does not exists it's created.

        station_code -- The station IRIS code

        return -- The station with given IRIS code.
        """
        for current in self.__station_list:
            if current.code() == station_code:
                return current
        new_station = Station(station_code)
        self.__station_list.append(new_station)
        return new_station

    def _next_line_field(self, resp_lines):
        """
        Give the next line and the blockette field numbers.

        resp_lines -- The list of RESP file lines

        result -- The cuple (<next line>, <field number>) with:
                    <next line>   : The next line as string
                    <field number>: The current blockette field number or
                                    None if line is commented (begin by #)
        """
        current_line = resp_lines.pop(0)

        if current_line[0] == '#':
            current_line, None

        # Get field number
        matching = re.match("B[0-9]+F([0-9]+)", current_line)
        if not matching:
            raise RespParserException("Can't parse <%s>" % current_line)
        field = int(matching.groups()[0])
        return current_line, field

    def _get_line_value(self, current_line):
        """
        Give value of a given line.

        current_line -- The line to parse
        """
        matching = re.match("B[0-9]+F[0-9]+[^:]+:(.+)", current_line)
        if not matching:
            raise RespParserException("Can't parse <%s>" % current_line)
        return matching.groups()[0].strip()

    def _blk10(self, resp_lines):
        # Skipping blockette 10 lines
        while(re.match("B010", resp_lines[0])):
            resp_lines.pop(0)

    def _blk11(self, resp_lines):
        # Skipping blockette 11 lines
        while(re.match("B011", resp_lines[0])):
            resp_lines.pop(0)

    def _blk30(self, resp_lines):
        # Skipping blockette 30 lines
        while(re.match("B030", resp_lines[0])):
            resp_lines.pop(0)

    def _blk41(self, resp_lines):
        fir_blk = {"numerator": list()}
        while(resp_lines[0].startswith("B041") or resp_lines[0][0] == '#'):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            if field is not 9:
                # Get field content
                value = self._get_line_value(current_line)
            else:
                # Get coefficient value
                matching = re.match("B[0-9]+F[0-9]+[ \t]+[0-9]+[ \t]+([^ \t]+)",
                                    current_line)
                if not matching:
                    raise RespParserException("Can't parse <%s>" % current_line)
                value = matching.groups()[0].strip()
            if field == 3:
                if value in self.__dictionnary_blk:
                    raise RespParserException("A response with same lookup key already exists")
                self.__dictionnary_blk[value] = fir_blk
            elif field == 4:
                fir_blk["name"] = value
            elif field == 5:
                fir_blk["symmetry"] = value
            elif field == 6:
                # digital_filter.set_input_unit(value.split()[0])
                # Skip input unit
                fir_blk["input_unit"] = value
            elif field == 7:
                # digital_filter.set_output_unit(value.split()[0])
                # Skip output unit
                fir_blk["output_unit"] = value
            elif field == 8:
                # skip number of numerator
                pass
            elif field == 9:
                fir_blk["numerator"].append(float(value))
            else:
                raise NotImplementedError("Field %s is not managed for blockette 41" % field)

    def _blk43(self, resp_lines):
        pz_blk = {"zeros": list(), "poles": list()}
        while(resp_lines[0].startswith("B043") or resp_lines[0][0] == '#'):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            if field not in [11, 16]:
                # Get field content
                value = self._get_line_value(current_line)
            else:
                # Get field content
                matching = re.match("B[0-9]+F[0-9]+-[0-9]+[ \t]+[0-9]+[ \t]+([^ \t]+)[\t ]+([^ \t]+)[ \t]+([^ \t]+)[ \t]+([^ \t]+)",
                                    current_line)
                if not matching:
                    raise RespParserException("Can't parse <%s>" % current_line)

            if field == 3:
                if value in self.__dictionnary_blk:
                    raise RespParserException("A response with same lookup key already exists")
                self.__dictionnary_blk[value] = pz_blk
            elif field == 4:
                pz_blk["name"] = value
            if field == 5:
                pz_blk["response_type"] = value
            elif field == 6:
                pz_blk["input_unit"] = value
            elif field == 7:
                pz_blk["output_unit"] = value
            elif field == 8:
                pz_blk["A0"] = float(value)
            elif field == 9:
                pz_blk["normalization_frequency"] = float(value)
            elif field in (10, 15):
                # Skip number of poles and zeros
                pass
            elif field == 11:
                # Manage zeros
                zero = (float(matching.groups()[0]), float(matching.groups()[1]),
                        float(matching.groups()[2]), float(matching.groups()[3]))
                pz_blk["zeros"].append(zero)
            elif field == 16:
                # Manage poles
                pole = (float(matching.groups()[0]), float(matching.groups()[1]),
                        float(matching.groups()[2]), float(matching.groups()[3]))
                pz_blk["poles"].append(pole)
            else:
                raise NotImplementedError("Field %s is not managed for blockette 43" % field)

    def _blk44(self, resp_lines):
        coef_blk = {"numerators": list(), "denominators": list()}
        while(resp_lines[0].startswith("B044") or resp_lines[0][0] == '#'):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            if field not in [9, 12]:
                # Get field content
                value = self._get_line_value(current_line)
            else:
                # Get field content
                matching = re.match("B[0-9]+F[0-9]+-[0-9]+[ \t]+[0-9]+[ \t]+([^ \t]+)[\t ]+([^ \t]+)[ \t]+",
                                    current_line)
                if not matching:
                    raise RespParserException("Can't parse <%s>" % current_line)

            if field == 5:
                coef_blk["response_type"] = value[0]
            elif field == 6:
                coef_blk["input_unit_key"] = value.split()[0]
            elif field == 7:
                coef_blk["output_unit_key"] = value.split()[0]
            elif field == 8:
                # skip numbers of numerators
                pass
            elif field == 9:
                numerator = (matching.groups()[0], matching.groups()[1])
                coef_blk["numerators"].append(numerator)
            elif field == 11:
                # skip numbers of denominators
                pass
            elif field == 12:
                denominator = (matching.groups()[0], matching.groups()[1])
                coef_blk["denominators"].append(denominator)
            else:
                raise NotImplementedError("Field %s is not managed for blockette 44" % field)

    def _blk47(self, resp_lines):
        decim_blk = {}
        while(resp_lines[0].startswith("B047") or resp_lines[0][0] == '#'):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            value = self._get_line_value(current_line)
            if field == 5:
                decim_blk["input_sps"] = 1.0 / float(value)
            elif field == 6:
                decim_blk["decimation_factor"] = float(value)
            elif field == 7:
                decim_blk["decimation_offset"] = float(value)
            elif field == 8:
                decim_blk["delay"] = float(value)
            elif field == 9:
                decim_blk["correction"] = float(value)
            else:
                raise NotImplementedError("Field %s is not managed for blockette 48" % field)

    def _blk48(self, resp_lines):
        gain_blk = {}
        while(resp_lines[0].startswith("B048") or resp_lines[0][0] == '#'):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            # Get value
            value = self._get_line_value(current_line)
            if field == 5:
                gain_blk["sensitivity"] = float(value)
            elif field == 6:
                gain_blk["frequency"] = float(value)
            elif field == 7:
                # Calibration is not yet managed
                pass
            else:
                raise NotImplementedError("Field %s is not managed for blockette 48" % field)

    def _blk50(self, resp_lines):
        while(resp_lines[0].startswith("B050") or resp_lines[0][0] == '#'):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            # Get value
            value = self._get_line_value(current_line)
            if field == 3:
                self.__current_station = self._get_station(value)
                self.__station_list.append(self.__current_station)
            elif field == 4:
                self.__current_station.set_latitude(value)
            elif field == 5:
                self.__current_station.set_longitude(value)
            elif field == 6:
                self.__current_station.set_elevation(value)
            elif field == 9:
                self.__current_station.set_name(value)
            elif field == 10:
                self.__current_station.set_owner(value)
            elif field == 11:
                # skip 32 bit word order
                pass
            elif field == 12:
                # skip 16 bit word order
                pass
            elif field == 13:
                open_date = datetime.strptime(value, "%Y,%j,%H:%M:%S.%f")
                self.__current_station.set_open_date(open_date)
            elif field == 14:
                if value == "(null)":
                    close_date = None
                else:
                    close_date = datetime.strptime(value, "%Y,%j,%H:%M:%S.%f")
                self.__current_station.set_close_date(close_date)
            elif field == 15:
                # skip update flags
                pass
            elif field == 16:
                self.__current_network = value
            else:
                raise RespParserException("Can't parse <%s> (field = %s, value = %s)"
                                          % (current_line, field, value))

    def _blk51(self, resp_lines):
        current_comment = {}
        while(resp_lines[0].startswith("B051") or resp_lines[0][0] == '#'):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            # Get value
            value = self._get_line_value(current_line)

            if field == 3:
                # Starting effective date
                current_comment["start_date"] = datetime.strptime(value, "%Y,%j,%H:%M:%S.%f")
            elif field == 4:
                # Ending effective date
                current_comment["end_date"] = datetime.strptime(value, "%Y,%j,%H:%M:%S.%f")
            elif field == 5:
                current_comment["message"] = value.split()[1].strip()
            elif field == 6:
                # Skip comment level
                pass
            else:
                raise RespParserException("Can't parse <%s> (field = %s, value = %s)"
                                          % (current_line, field, value))
        self.__current_station.add_comment(current_comment)

    def _blk52(self, resp_lines):
        current_channel = {}
        channel_location = {}
        while(resp_lines[0].startswith("B052") or
              resp_lines[0].startswith("B030") or
              resp_lines[0][0] == '#'):
            if resp_lines[0].startswith("B030"):
                # Skip Format lookup description
                if resp_lines[0].startswith("B030F03"):
                    # format name
                    if "STEIM2" in resp_lines[0].upper():
                        current_channel["format"] = "STEIM2"
                    elif "STEIM" in resp_lines[0].upper():
                        current_channel["format"] = "STEIM"
                    else:
                        raise ValueError("Can't decode format type: %s" % resp_lines[0])
                resp_lines.pop(0)
                continue

            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            # Get value
            value = self._get_line_value(current_line)
            if field == 3:
                if value == "":
                    channel_location["code"] = "  "
                else:
                    channel_location["code"] = value
            elif field == 4:
                current_channel["code"] = value
            elif field == 5:
                # Skip subchannel
                pass
            elif field == 6:
                # Instrument lookup, skip index
                current_channel["instrument"] = " ".join(value.strip().split()[1:])
            elif field == 7:
                # Skip comment
                pass
            elif field == 8:
                # Get input unit (used to known sensor type ?)
                # unit = value.split()[1].strip()
                pass
            elif field == 9:
                self.__current_channel.get_set_sensor().set_calibration_unit(value.split()[1].strip())
                # Skip Calibration unit
                pass
            elif field == 10:
                # Latitude
                channel_location["latitude"] = value
            elif field == 11:
                # Longitude
                channel_location["longitude"] = value
            elif field == 12:
                # elevation
                channel_location["elevation"] = value
            elif field == 13:
                # depth
                channel_location["depth"] = value
            elif field == 14:
                # Azimuth of this component
                current_channel["azimuth"] = float(value)
            elif field == 15:
                # Dip of this component
                current_channel["dip"] = float(value)
            elif field == 16:
                # Skip format lookup code
                pass
            elif field == 17:
                # Skip data record length
                pass
            elif field == 18:
                # Sampling rate (for now skip)
                current_channel["sample_rate"] = float(value)
            elif field == 19:
                # Clock tolerance
                pass
            elif field == 21:
                # Channel flags
                current_channel["flags"] = value
            elif field == 22:
                current_channel["open_date"] = datetime.strptime(value,
                                                                 "%Y,%j,%H:%M:%S.%f")
            elif field == 23:
                if value == "(null)":
                    current_channel["close_date"] = None
                else:
                    current_channel["close_date"] = datetime.strptime(value,
                                                                      "%Y,%j,%H:%M:%S.%f")
            elif field == 24:
                # Update flags
                pass
            else:
                raise RespParserException("Can't parse <%s>" % current_line)

        current_location = self.__current_station.get_location(channel_location["code"],
                                                               channel_location["latitude"],
                                                               channel_location["longitude"],
                                                               channel_location["elevation"],
                                                               channel_location["depth"])

        self.__current_channel = self.__current_station.new_channel(current_channel["code"],
                                                                    current_location,
                                                                    current_channel["open_date"],
                                                                    current_channel["close_date"],
                                                                    self.__current_network)
        self.__current_channel.set_flags(current_channel["flags"])
        self.__current_channel.set_format(current_channel["format"])

        return

    def _blk53(self, resp_lines):
        pz_blk = {"zeros": list(), "poles": list()}
        # Skipping blockette 58 lines
        while(resp_lines[0].startswith("B053") or resp_lines[0][0] == "#"):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            if field not in [10, 15]:
                # Get value
                value = self._get_line_value(current_line)
            else:
                # Get field content
                matching = re.match("B[0-9]+F[0-9]+-[0-9]+[ \t]+[0-9]+[ \t]+([^ \t]+)[\t ]+([^ \t]+)[ \t]+([^ \t]+)[ \t]+([^ \t]+)",
                                    current_line)
                if not matching:
                    raise RespParserException("Can't parse <%s>" % current_line)

            if field == 3:
                pz_blk["response_type"] = value
            elif field == 4:
                if self.__current_stage is None:
                    # create a new stage
                    self.__current_stage = Stage(int(value))
                elif self.__current_stage.sequence_number() != int(value):
                    self.__current_channel.add_stage(self.__current_stage)
                    self.__current_stage = Stage(int(value))
                    # Close current stage and begin a new one
            elif field == 5:
                pz_blk["input_unit"] = value
            elif field == 6:
                pz_blk["output_unit"] = value
            elif field == 7:
                pz_blk["A0"] = float(value)
            elif field == 8:
                pz_blk["normalization_frequency"] = float(value)
            elif field in (9, 14):
                # Skip number of poles and zeros
                pass
            elif field == 10:
                # Manage zeros
                zero = (float(matching.groups()[0]), float(matching.groups()[1]),
                        float(matching.groups()[2]), float(matching.groups()[3]))
                pz_blk["zeros"].append(zero)
            elif field == 15:
                # Manage poles
                pole = (float(matching.groups()[0]), float(matching.groups()[1]),
                        float(matching.groups()[2]), float(matching.groups()[3]))
                pz_blk["poles"].append(pole)
            else:
                raise NotImplementedError("Field %s is not managed for blockette 53" % field)
        self.__current_stage.add_pz(pz_blk)

    def _blk54(self, resp_lines):
        coef_blk = {"numerators": list(), "denominators": list()}
        while(resp_lines[0].startswith("B054") or resp_lines[0][0] == "#"):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            if field not in [8, 11]:
                # Get field content
                value = self._get_line_value(current_line)
            else:
                # Get field content
                matching = re.match("B[0-9]+F[0-9]+-[0-9]+[ \t]+[0-9]+[ \t]+([^ \t]+)[\t ]+([^ \t]+)[ \t]+",
                                    current_line)
                if not matching:
                    raise RespParserException("Can't parse <%s>" % current_line)

            if field == 3:
                coef_blk["response_type"] = value[0]
            elif field == 4:
                if self.__current_stage is None:
                    # create a new stage
                    self.__current_stage = Stage(int(value))
                elif self.__current_stage.sequence_number() != int(value):
                    self.__current_channel.add_stage(self.__current_stage)
                    self.__current_stage = Stage(int(value))
                    # Close current stage and begin a new one
            elif field == 5:
                coef_blk["input_unit"] = value.split()[0]
            elif field == 6:
                coef_blk["output_unit"] = value.split()[0]
            elif field == 7:
                # skip numbers of numerators
                pass
            elif field == 8:
                numerator = (matching.groups()[0], matching.groups()[1])
                coef_blk["numerators"].append(numerator)
            elif field == 10:
                # skip numbers of denominators
                pass
            elif field == 11:
                denominator = (float(matching.groups()[0]), float(matching.groups()[1]))
                coef_blk["denominators"].append(denominator)
            else:
                raise NotImplementedError("Field %s is not managed for blockette 54" % field)
        self.__current_stage.add_coef(coef_blk)

    def _blk57(self, resp_lines):
        decim_blk = {}
        while(resp_lines[0].startswith("B057") or resp_lines[0][0] == '#'):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            value = self._get_line_value(current_line)
            if field == 3:
                if self.__current_stage is None:
                    # create a new stage
                    self.__current_stage = Stage(int(value))
                elif self.__current_stage.sequence_number() != int(value):
                    self.__current_channel.add_stage(self.__current_stage)
                    self.__current_stage = Stage(int(value))
                    # Close current stage and begin a new one
            elif field == 4:
                decim_blk["input_sps"] = 1.0 / float(value)
            elif field == 5:
                decim_blk["decimation_factor"] = float(value)
            elif field == 6:
                decim_blk["decimation_offset"] = float(value)
            elif field == 7:
                decim_blk["delay"] = float(value)
            elif field == 8:
                decim_blk["correction"] = float(value)
            else:
                raise NotImplementedError("Field %s is not managed for blockette 57" % field)
        self.__current_stage.add_decim(decim_blk)

    def _blk58(self, resp_lines):
        gain_blk = {}
        while(resp_lines[0].startswith("B058") or resp_lines[0][0] == '#'):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            # Get value
            value = self._get_line_value(current_line)
            if field == 3:
                if self.__current_stage is None:
                    # create a new stage
                    self.__current_stage = Stage(int(value))
                elif self.__current_stage.sequence_number() != int(value):
                    self.__current_channel.add_stage(self.__current_stage)
                    self.__current_stage = Stage(int(value))
                    # Close current stage and begin a new one
            elif field == 4:
                gain_blk["sensitivity"] = float(value)
            elif field == 5:
                gain_blk["frequency"] = float(value)
            elif field == 6:
                # Calibration is not yet managed
                pass
            else:
                raise NotImplementedError("Field %s is not managed for blockette 58" % field)
        self.__current_stage.add_gain(gain_blk)

    def _blk59(self, resp_lines):
        current_comment = {}
        while(resp_lines[0].startswith("B058") or resp_lines[0][0] == '#'):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            # Get value
            value = self._get_line_value(current_line)

            if field == 3:
                # Starting effective date
                current_comment["start_date"] = datetime.strptime(value, "%Y,%j,%H:%M:%S.%f")
            elif field == 4:
                # Ending effective date
                current_comment["end_date"] = datetime.strptime(value, "%Y,%j,%H:%M:%S.%f")
            elif field == 5:
                current_comment["message"] = value.split()[1].strip()
            elif field == 6:
                # Skip comment level
                pass
            else:
                raise RespParserException("Can't parse <%s> (field = %s, value = %s)"
                                          % (current_line, field, value))

        self.__current_channel.add_comment(current_comment)

    def _blk60(self, resp_lines):
        raise NotImplementedError("Blockette 60 is not yet implemnented")

    def _blk61(self, resp_lines):
        fir_blk = {"coeficients": list()}
        while(resp_lines[0].startswith("B061") or resp_lines[0][0] == '#'):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            if field != 9:
                # Get field content
                value = self._get_line_value(current_line)
            else:
                # Get field content
                matching = re.match("B[0-9]+F[0-9]+[ \t]+[0-9]+[ \t]+([^ \t]+)[ \t]+",
                                    current_line)
                if not matching:
                    raise RespParserException("Can't parse <%s>" % current_line)

                value = float(matching.groups()[0])

            if field == 3:
                if self.__current_stage is None:
                    # create a new stage
                    self.__current_stage = Stage(int(value))
                elif self.__current_stage.sequence_number() != int(value):
                    self.__current_channel.add_stage(self.__current_stage)
                    self.__current_stage = Stage(int(value))
                    # Close current stage and begin a new one
            elif field == 4:
                fir_blk["name"] = value
            elif field == 5:
                fir_blk["symmetry"] = value
            elif field == 6:
                fir_blk["input_unit"] = value
            elif field == 7:
                fir_blk["output_unit"] = value
            elif field == 9:
                fir_blk["coeficients"].append(value)
            else:
                raise NotImplementedError("Field %s is not managed for blockette 58" % field)
        self.__current_stage.add_fir(fir_blk)

    def _blk62(self, resp_lines):
        poly_blk = {"coefficients": list()}
        while(resp_lines[0].startswith("B062") or resp_lines[0][0] == '#'):
            current_line, field = self._next_line_field(resp_lines)
            if field is None:
                continue

            if field != 15:
                # Get field content
                value = self._get_line_value(current_line)
            else:
                # Get field content
                matching = re.match("B[0-9]+F[0-9]+[ \t]+[0-9]+[ \t]+([^ \t]+)[ \t]+([^ \t]+)[ \t]+",
                                    current_line)
                if not matching:
                    raise RespParserException("Can't parse <%s>" % current_line)

            if field == 3:
                # Skip Transfer function type
                pass
            elif field == 4:
                if self.__current_stage is None:
                    # create a new stage
                    self.__current_stage = Stage(int(value))
                elif self.__current_stage.sequence_number() != int(value):
                    self.__current_channel.add_stage(self.__current_stage)
                    self.__current_stage = Stage(int(value))
                    # Close current stage and begin a new one
            elif field == 5:
                poly_blk["input_unit"] = value
            elif field == 6:
                poly_blk["output_unit"] = value
            elif field == 7:
                poly_blk["approximation_type"] = value
            elif field == 8:
                poly_blk["valid_frequency_unit"] = value
            elif field == 9:
                poly_blk["lower_valid_frequency_bound"] = float(value)
            elif field == 10:
                poly_blk["upper_valid_frequency_bound"] = float(value)
            elif field == 11:
                poly_blk["lower_bound_approximation"] = float(value)
            elif field == 12:
                poly_blk["upper_bound_approximation"] = float(value)
            elif field == 13:
                poly_blk["maximum_absolute_error"] = float(value)
            elif field == 15:
                coefficient = (float(matching.groups()[0]), float(matching.groups()[1]))
                poly_blk["coefficients"].append(coefficient)
            else:
                raise NotImplementedError("Field %s is not managed for blockette 58" % field)
        self.__current_stage.add_poly(poly_blk)
