# -*- coding: utf-8 -*-
from oca.jinjautils.filters import dbirddate


class MeteorJsonGenerator(object):

    def __init__(self, station_list, pz_access):
        """
        Initialize this MeteorJsonGenerator

        station_list -- The list of station present in this JSON meteor
        pz_access    -- The PZAccess used to update PZ databank
        """
        self.__station_list = station_list
        self.__pz_access = pz_access
        self.__network_list = None

        # Associate network code with meteor id
        self.__network_meteor_id = dict()

        # Associate location object with meteor id
        self.__location_meteor_id = dict()

    def __next_meteor_id(self):
        """
        Give the next meteor id
        """
        result = self.__next_meteor_id
        self.__next_meteor_id += 1
        return result

    def __network_list(self):
        """
        Give the list of network code used by channels stations

        return -- The network code list
        """
        if self.__network_list is not None:
            return self.__network_list

        network_set = set()
        for current_station in self.__station_list:
            for current_channel in current_station.channels():
                network_set.add(current_channel.network())
        self.__network_list = list(network_set)
        return self.__network_list

    def __network_collection(self):
        """
        Generate network collection dictionnary
        """
        network_collection = {
            "id": self.__next_meteor_id(),
            "label": "Network",
            "addNode": True,
            "nodeType": "networkCollection",
            "children": list()
        }
        for current_network in self.__network_list():
            new_network = {
                "id": self.__next_meteor_id(),
                "label": current_network,
                "nodeType": "network",
                "values": {
                    "fdsn": current_network,
                    "open": "1990-01-01T00:00:00Z"
                }
            }
            self.__network_meteor_id[current_network] = new_network["id"]
            network_collection["children"].append(new_network)
        return network_collection

    def __location_collection(self, location_list):
        """
        Generate meteor locationCollection dictionnary according to
        given location list

        location_list -- The list of location
        """
        location_collection = {
            "id": self.__next_meteor_id(),
            "label": "Location",
            "addNode": True,
            "nodeType": "locationCollection",
            "children": list()
        }
        for current_location in location_list:
            new_location = {
                "id": self.__next_meteor_id(),
                "label": current_location.code(),
                "nodeType": "location",
                "values": {
                    "code": current_location.code(),
                    "lat_value": current_location.latitude(),
                    "lon_value": current_location.longitude(),
                    "alt_value": current_location.elevation(),
                    "depth": current_location.depth(),
                    "vault": "unknown",
                    "geology": "unknown"
                }
            }
            self.__location_meteor_id[current_location] = new_location["id"]
            location_collection["children"].append(new_location)
        return location_collection

    def __update_channel_group(self, channel_group_list, channel):
        """
        Search channel group matching given channel in channel_group_list and
        create a new one if no one is found.
        If new channel group is created, it's added to channel group list.

        channel_group_list -- The list of existing channel group
        channel            -- The channel
        """
        for current_group in channel_group_list:
            if(channel_group_list["network_code"] == channel.network_code() and
               channel_group_list["location"] == channel.location() and
               channel_group_list["channel_code_prefix"] == channel.iris_code()[:2] and
               channel_group_list["open_date"] == channel.open_date() and
               channel_group_list["close_date"] == channel.close_date()):
                current_group["channel_list"].append(channel)
                return current_group
        # We don't find a matching group, create a new one
        new_group = {
            "network_code": channel.network_code(),
            "location": channel.location(),
            "channel_code_prefix": channel.iris_code()[:2],
            "open_date": channel.open_date(),
            "close_date": channel.close_date(),
            "channel_list": list(),
            "channel_group": {
                "id": self.__next_meteor_id(),
                "label": channel.iris_code()[:2],
                "nodeType": "channelGroup",
                "value": {
                    "device": list(),
                    "channel": list(),
                    "network": self.__next_meteor_id[channel.network_code()],
                    "location": self.__location_meteor_id[channel.location()],
                    "flags": channel.flags(),
                    "format": channel.format(),
                    "open": channel.open_date().strftime("%Y-%m-%dT%H:%M:%SZ"),
                    "close": None if channel.close_date() is None else channel.close_date().strftime("%Y-%m-%dT%H:%M:%SZ")
                }
            }
        }
        new_group["channel_list"].append(channel)
        channel_group_list.append(new_group)
        return new_group

    def __fill_channel_group_definition(self, channel_group, channel):
        """
        Update channel group definition with given channel.

        channel_group -- The channel group definition
        channel       -- The channel
        """
        current_input_unit = None
        for current_stage in channel.stages():
            if current_stage.sequence_number() == 0:
                # Global sensitivity stage
                pass
            elif current_stage.sequence_number() == 1:
                # Sensor stage
                current_input_unit = "V"
            elif current_input_unit == "V":
                # analog filter or digitizer
                if len(channel_group["device"] < current_stage.sequence_number()):
                    # First channel of group
                    pass
                else:
                    # Retreive previously created device
                    device = channel_group["device"][current_stage.sequence_number()]
                    if device["device"][0] == "digitizer":
                        # digitizer
                        pass
                    else:
                        # analog filter
                        pass
            else:
                # digital filter
                pass

    def __create_channel_groups(self, channel_list):
        """
        Create channel group according to given channel list

        channel_list -- The channel list to process
        """
        result = list()
        # Create channel group according to channel list
        for current_channel in channel_list:
            self.__update_channel_group(result, current_channel)

        # Fill channel group definition
        for current_group in result:
            for current_channel in current_group["channel_list"]:
                self.__fill_channel_group_definition(current_group,
                                                     current_channel)
        return result

    def __channel_group_collection(self, channel_list):
        """
        Generate channel group collection according to given channel list

        channel_list -- The channel list
        """
        channel_group_collection = {
            "id": self.__next_meteor_id(),
            "label": "Channel",
            "addNode": True,
            "nodeType": "channelGroupCollection",
            "children": list()
        }

        channel_group = self.__create_channel_group(channel_list)
        for current_group in channel_group:
            channel_group_collection["children"].append(current_group["channel_group"])
        return channel_group_collection

    def ___station(self, station):
        """
        Generate meteor station dictionnary according to given
        response station.

        station -- The station response
        """
        new_station = {
            "id": self.__next_meteor_id(),
            "label": station.iris_code(),
            "nodeType": "station",
            "values": {
                "iris_code": station.iris_code(),
                "toponyme": station.name(),
                "owner": station.owner(),
                "first_install": dbirddate(station.first_install()),
                "close": dbirddate(station.close_date()),
                "lat_value": station.latitude(),
                "lon_value": station.longitude(),
                "alt_value": station.elevation()
            },
            "children": list()
        }
        # Add station locations
        new_station["children"].append(self.__location_collection(station.locations()))

        # Add station channels
        new_station["children"].append(self.__channel_group_collection(station.channels()))
        return new_station

    def __station_collection(self):
        """
        Generate station collection dictionnary
        """
        station_collection = {
            "id": self.__next_meteor_id(),
            "label": "Station",
            "addNode": True,
            "nodeType": "stationCollection",
            "children": list()
        }
        for current_station in self.__station_list:
            station_collection["children"].append(self.__station(current_station))
        return station_collection

    def generate(self):
        """
        Generate JSON meteor

        return -- The JSON meteor as dictionnary
        """
        self.__next_meteor_id = 0

        return [self.__network_collection(),
                self.__station_collection()]
