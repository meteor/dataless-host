# -*- coding: utf-8 -*-
import os
import json


def convert_sensor(sensor_file):
    nbPoles = None
    nbZeros = None
    nbCoef = None
    sensor_dict = dict()
    sensor_dict["Response"] = dict()
    for current_line in sensor_file:
        if nbPoles is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                sensor_dict["Response"]["PolesDef"].append([float(split_line[0]),
                                                            float(split_line[1])])
                nbPoles -= 1
                if nbPoles == 0:
                    nbPoles = None
        elif nbZeros is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                sensor_dict["Response"]["ZeroesDef"].append([float(split_line[0]),
                                                             float(split_line[1])])
                nbZeros -= 1
                if nbZeros == 0:
                    nbZeros = None
        elif nbCoef is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                sensor_dict["Response"]["Coefficients"].append(float(split_line[0]))
                nbCoef -= 1
                if nbCoef == 0:
                    nbCoef = None
        elif ':' in current_line:
            # First suppress comment
            current_line = current_line.split("#")[0]
            split_line = current_line.split(':')
            keyword = split_line[0].strip().upper()
            if keyword == "TRANSFER_FUNCTION_TYPE":
                sensor_dict["TransferFunctionType"] = split_line[1].strip()[1:-1]
            elif keyword == "INPUT_UNIT":
                sensor_dict["InputUnit"] = split_line[1].strip()[1:-1]
            elif keyword == "OUTPUT_UNIT":
                sensor_dict["OutputUnit"] = split_line[1].strip()[1:-1]
            elif keyword == "SENSITIVITY":
                sensor_dict["Response"]["Sensitivity"] = float(split_line[1].strip())
            elif keyword == "TRANSFER_NORMALIZATION_FREQUENCY":
                sensor_dict["Response"]["TransferNormalizationFrequency"] = float(split_line[1].strip())
            elif keyword == "LOWER_VALID_FREQUENCY_BAND":
                sensor_dict["Response"]["LowerValidFrequencyBound"] = float(split_line[1].strip())
            elif keyword == "UPPER_VALID_FREQUENCY_BAND":
                sensor_dict["Response"]["UpperValidFrequencyBound"] = float(split_line[1].strip())
            elif keyword == "LOWER_BOUND_APPROXIMATION":
                sensor_dict["Response"]["LowerBoundApproximation"] = float(split_line[1].strip())
            elif keyword == "UPPER_BOUND_APPROXIMATION":
                sensor_dict["Response"]["UpperBoundApproximation"] = float(split_line[1].strip())
            elif keyword == "VALID_FREQUENCY_UNIT":
                sensor_dict["Response"]["ValidFrequencyUnit"] = split_line[1].strip()[1:-1]
            elif keyword == "NUMBER_OF_COEFFICIENTS":
                nbCoef = int(split_line[1].strip())
                if nbCoef == 0:
                    nbCoef = None
                sensor_dict["Response"]["Coefficients"] = list()
            elif keyword == "NUMBER_OF_ZEROES":
                nbZeros = int(split_line[1].strip())
                if nbZeros == 0:
                    nbZeros = None
                sensor_dict["Response"]["ZeroesDef"] = list()
            elif keyword == "NUMBER_OF_POLES":
                nbPoles = int(split_line[1].strip())
                if nbPoles == 0:
                    nbPoles = None
                sensor_dict["Response"]["PolesDef"] = list()
            elif keyword == "NO_MIN_FREQ":
                sensor_dict["Response"]["NoMinFreq"] = True
        else:
            if current_line.strip() == "":
                continue
            split_line = current_line.strip().split()
            if split_line[0].upper() == "INCLUDE":
                sensor_dict["Include"] = split_line[1][1:-1]
    return json.dumps(sensor_dict, indent=2)


def convert_sensor_dir(input_directory, output_directory, verbose):
    for current_response in os.listdir(input_directory):
        if not os.path.isfile(os.path.join(input_directory, current_response)):
            continue
        if verbose:
            print("Processing %s..." % current_response)
        current_file = open(os.path.join(input_directory, current_response))
        json_content = convert_sensor(current_file)
        new_file = open(os.path.join(output_directory, current_response), "w")
        new_file.write(json_content)
        new_file.close()
        current_file.close()


def convert_sensor_include(include_file):
    nbPoles = None
    nbZeros = None
    nbCoef = None
    current_include = dict()
    for current_line in include_file:
        if nbPoles is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                current_include["PolesDef"].append([float(split_line[0]),
                                                    float(split_line[1])])
                nbPoles -= 1
                if nbPoles == 0:
                    nbPoles = None
        elif nbZeros is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                current_include["ZeroesDef"].append([float(split_line[0]),
                                                     float(split_line[1])])
                nbZeros -= 1
                if nbZeros == 0:
                    nbZeros = None
        elif nbCoef is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                current_include["Coefficients"].append(float(split_line[0]))
                nbCoef -= 1
                if nbCoef == 0:
                    nbCoef = None
        elif ':' in current_line:
            # First suppress comment
            current_line = current_line.split("#")[0]
            split_line = current_line.split(':')
            keyword = split_line[0].strip().upper()
            if keyword == "NUMBER_OF_ZEROES":
                nbZeros = int(split_line[1].strip())
                if nbZeros == 0:
                    nbZeros = None
                current_include["ZeroesDef"] = list()
            elif keyword == "NUMBER_OF_POLES":
                nbPoles = int(split_line[1].strip())
                if nbPoles == 0:
                    nbPoles = None
                current_include["PolesDef"] = list()
            elif keyword == "NUMBER_OF_COEFFICIENTS":
                nbCoef = int(split_line[1].strip())
                if nbCoef == 0:
                    nbCoef = None
                current_include["Coefficients"] = list()

    return json.dumps(current_include, indent=2)


def convert_sensor_include_dir(input_directory, output_directory, verbose):
    for current_response in os.listdir(input_directory):
        if not os.path.isfile(os.path.join(input_directory, current_response)):
            continue
        if verbose:
            print("Processing %s..." % current_response)
        current_file = open(os.path.join(input_directory, current_response))
        json_content = convert_sensor_include(current_file)
        new_file = open(os.path.join(output_directory, current_response), "w")
        new_file.write(json_content)
        new_file.close()
        current_file.close()
