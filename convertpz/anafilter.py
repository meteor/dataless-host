# -*- coding: utf-8 -*-
import os
import json


def convert_anafilter(anafilter_file):
    nbPoles = None
    nbZeros = None
    anafilter_cascade = list()
    current_filter = None
    for current_line in anafilter_file:
        if nbPoles is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                current_filter["Response"]["PolesDef"].append([float(split_line[0]),
                                                               float(split_line[1])])
                nbPoles -= 1
                if nbPoles == 0:
                    nbPoles = None
        elif nbZeros is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                current_filter["Response"]["ZeroesDef"].append([float(split_line[0]),
                                                                float(split_line[1])])
                nbZeros -= 1
                if nbZeros == 0:
                    nbZeros = None
        elif ':' in current_line:
            # First suppress comment
            current_line = current_line.split("#")[0]
            split_line = current_line.split(':')
            keyword = split_line[0].strip().upper()
            if keyword == "TRANSFER_FUNCTION_TYPE":
                current_filter["TransferFunctionType"] = split_line[1].strip()[1:-1]
            elif keyword == "TRANSFER_NORMALIZATION_FREQUENCY":
                current_filter["Response"]["TransferNormalizationFrequency"] = float(split_line[1].strip())
            elif keyword == "INPUT_UNIT":
                current_filter["InputUnit"] = split_line[1].strip()[1:-1]
            elif keyword == "OUTPUT_UNIT":
                current_filter["OutputUnit"] = split_line[1].strip()[1:-1]
            elif keyword == "SENSITIVITY":
                current_filter["Response"]["Sensitivity"] = float(split_line[1].strip())
            elif keyword == "NUMBER_OF_ZEROES":
                nbZeros = int(split_line[1].strip())
                if nbZeros == 0:
                    nbZeros = None
                current_filter["Response"]["ZeroesDef"] = list()
            elif keyword == "NUMBER_OF_POLES":
                nbPoles = int(split_line[1].strip())
                if nbPoles == 0:
                    nbPoles = None
                current_filter["Response"]["PolesDef"] = list()
        else:
            if current_line.strip() == "":
                continue
            split_line = current_line.strip().split()
            if split_line[0].upper() == "INCLUDE":
                current_filter["Include"] = split_line[1][1:-1]
            elif split_line[0].upper() == "BEGIN" and split_line[1].upper() == "ANA_FILTER":
                current_filter = dict()
                current_filter["Response"] = dict()
                anafilter_cascade.append(current_filter)
    return json.dumps(anafilter_cascade, indent=2)


def convert_anafilter_dir(input_directory, output_directory, verbose):
    for current_response in os.listdir(input_directory):
        if not os.path.isfile(os.path.join(input_directory, current_response)):
            continue
        if verbose:
            print("Processing %s..." % current_response)
        current_file = open(os.path.join(input_directory, current_response))
        json_content = convert_anafilter(current_file)
        new_file = open(os.path.join(output_directory, current_response), "w")
        new_file.write(json_content)
        new_file.close()
        current_file.close()


def convert_include_file(include_file):
        nbPoles = None
        nbZeros = None
        current_include = dict()
        for current_line in include_file:
            if nbPoles is not None:
                # First suppress comment
                current_line = current_line.split("#")[0]
                current_line = current_line.strip()
                if current_line != "":
                    split_line = current_line.split()
                    current_include["PolesDef"].append([float(split_line[0]),
                                                        float(split_line[1])])
                    nbPoles -= 1
                    if nbPoles == 0:
                        nbPoles = None
            elif nbZeros is not None:
                # First suppress comment
                current_line = current_line.split("#")[0]
                current_line = current_line.strip()
                if current_line != "":
                    split_line = current_line.split()
                    current_include["ZeroesDef"].append([float(split_line[0]),
                                                         float(split_line[1])])
                    nbZeros -= 1
                    if nbZeros == 0:
                        nbZeros = None
            elif ':' in current_line:
                # First suppress comment
                current_line = current_line.split("#")[0]
                split_line = current_line.split(':')
                keyword = split_line[0].strip().upper()
                if keyword == "NUMBER_OF_ZEROES":
                    nbZeros = int(split_line[1].strip())
                    if nbZeros == 0:
                        nbZeros = None
                    current_include["ZeroesDef"] = list()
                elif keyword == "NUMBER_OF_POLES":
                    nbPoles = int(split_line[1].strip())
                    if nbPoles == 0:
                        nbPoles = None
                    current_include["PolesDef"] = list()
        return json.dumps(current_include, indent=2)


def convert_anafilter_include_dir(input_directory, output_directory, verbose):
    for current_response in os.listdir(input_directory):
        if not os.path.isfile(os.path.join(input_directory, current_response)):
            continue
        if verbose:
            print("Processing %s..." % current_response)
        current_file = open(os.path.join(input_directory, current_response))
        json_content = convert_include_file(current_file)
        new_file = open(os.path.join(output_directory, current_response), "w")
        new_file.write(json_content)
        new_file.close()
        current_file.close()
