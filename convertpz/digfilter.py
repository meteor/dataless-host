# -*- coding: utf-8 -*-
import os
import json


def convert_digfilter(digfilter_file):
    nbPoles = None
    nbZeros = None
    nbCoef = None
    digfilter_cascade = list()
    current_filter = None
    for current_line in digfilter_file:
        if nbPoles is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                current_filter["Response"]["PolesDef"].append([float(split_line[0]),
                                                               float(split_line[1])])
                nbPoles -= 1
                if nbPoles == 0:
                    nbPoles = None
        elif nbZeros is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                current_filter["Response"]["ZeroesDef"].append([float(split_line[0]),
                                                                float(split_line[1])])
                nbZeros -= 1
                if nbZeros == 0:
                    nbZeros = None
        elif nbCoef is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                for current_coef in split_line:
                    current_filter["Response"]["Coefficients"].append(float(current_coef))
                    nbCoef -= 1
                    if nbCoef == 0:
                        nbCoef = None
        elif ':' in current_line:
            # First suppress comment
            current_line = current_line.split("#")[0]
            split_line = current_line.split(':')
            keyword = split_line[0].strip().upper()
            if keyword == "RESPONSE_TYPE" and split_line[1].strip()[1:-1].upper() == "FAKE":
                current_filter["Fake"] = True
            if keyword == "TRANSFER_FUNCTION_TYPE":
                current_filter["TransferFunctionType"] = split_line[1].strip()[1:-1]
            elif keyword == "TRANSFER_NORMALIZATION_FREQUENCY":
                current_filter["FrequencyOfGain"] = float(split_line[1].strip())
            elif keyword == "INPUT_SAMPLING_INTERVAL":
                current_filter["InputSamplingInterval"] = float(split_line[1].strip())
            elif keyword == "DECIMATION_FACTOR":
                current_filter["DecimationFactor"] = int(split_line[1].strip())
            elif keyword == "DELAY":
                current_filter["Delay"] = float(split_line[1].strip())
            elif keyword == "CORRECTION":
                current_filter["Correction"] = float(split_line[1].strip())
            elif keyword == "INPUT_SAMPLING_INTERVAL":
                current_filter["InputSamplingInterval"] = float(split_line[1].strip())
            elif keyword == "INPUT_UNIT":
                current_filter["InputUnit"] = split_line[1].strip()[1:-1]
            elif keyword == "OUTPUT_UNIT":
                current_filter["OutputUnit"] = split_line[1].strip()[1:-1]
            elif keyword == "SENSITIVITY":
                current_filter["Sensitivity"] = float(split_line[1].strip())
            elif keyword == "FREQUENCY_OF_GAIN":
                current_filter["FrequencyOfGain"] = float(split_line[1].strip())
            elif keyword == "NUMBER_OF_COEFFICIENTS":
                nbZeros = int(split_line[1].strip())
                if nbCoef == 0:
                    nbCoef = None
                current_filter["Response"]["Coefficients"] = list()
            elif keyword == "NUMBER_OF_ZEROES":
                nbZeros = int(split_line[1].strip())
                if nbZeros == 0:
                    nbZeros = None
                current_filter["Response"]["ZeroesDef"] = list()
            elif keyword == "NUMBER_OF_POLES":
                nbPoles = int(split_line[1].strip())
                if nbPoles == 0:
                    nbPoles = None
                current_filter["Response"]["PolesDef"] = list()
        else:
            if current_line.strip() == "":
                continue
            split_line = current_line.strip().split()
            if split_line[0].upper() == "INCLUDE":
                current_filter["Include"] = split_line[1][1:-1]
            elif split_line[0].upper() == "BEGIN" and split_line[1].upper() in ("DIGITAL_FILTER", "STAGE"):
                current_filter = dict()
                current_filter["Sensitivity"] = 1.0
                current_filter["Response"] = dict()
                digfilter_cascade.append(current_filter)
    return json.dumps(digfilter_cascade, indent=2)


def convert_digfilter_dir(input_directory, output_directory, verbose):
    for current_response in os.listdir(input_directory):
        if not os.path.isfile(os.path.join(input_directory, current_response)):
            continue
        if current_response[0] == '.':
            continue
        if verbose:
            print("Processing %s..." % current_response)
        current_file = open(os.path.join(input_directory, current_response))
        json_content = convert_digfilter(current_file)
        new_file = open(os.path.join(output_directory, current_response), "w")
        new_file.write(json_content)
        new_file.close()
        current_file.close()


def convert_digfilter_include(digfilter_file):
    nbCoef = None
    nbPoles = None
    nbZeros = None
    nbNum = None
    nbDen = None
    current_include = dict()
    for current_line in digfilter_file:
        if nbPoles is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                current_include["PolesDef"].append([float(split_line[0]),
                                                    float(split_line[1])])
                nbPoles -= 1
                if nbPoles == 0:
                    nbPoles = None
        elif nbZeros is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                current_include["ZeroesDef"].append([float(split_line[0]),
                                                     float(split_line[1])])
                nbZeros -= 1
                if nbZeros == 0:
                    nbZeros = None
        elif nbNum is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                current_include["Numerators"].append(float(split_line[0]))
                nbNum -= 1
                if nbNum == 0:
                    nbNum = None
        elif nbDen is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                current_include["Denominators"].append(float(split_line[0]))
                nbDen -= 1
                if nbDen == 0:
                    nbDen = None
        elif nbCoef is not None:
            # First suppress comment
            current_line = current_line.split("#")[0]
            current_line = current_line.strip()
            if current_line != "":
                split_line = current_line.split()
                for current_coef in split_line:
                    current_include["Coefficients"].append(float(current_coef))
                    nbCoef -= 1
                    if nbCoef == 0:
                        nbCoef = None
        elif ':' in current_line:
            # First suppress comment
            current_line = current_line.split("#")[0]
            split_line = current_line.split(':')
            keyword = split_line[0].strip().upper()
            if keyword == "NUMBER_OF_COEFFICIENTS":
                nbCoef = int(split_line[1].strip())
                if nbCoef == 0:
                    nbCoef = None
                current_include["Coefficients"] = list()
            elif keyword == "NUMBER_OF_ZEROES":
                nbZeros = int(split_line[1].strip())
                if nbZeros == 0:
                    nbZeros = None
                current_include["ZeroesDef"] = list()
            elif keyword == "NUMBER_OF_POLES":
                nbPoles = int(split_line[1].strip())
                if nbPoles == 0:
                    nbPoles = None
                current_include["PolesDef"] = list()
            elif keyword == "NUMBER_OF_NUMERATORS":
                nbNum = int(split_line[1].strip())
                if nbNum == 0:
                    nbNum = None
                current_include["Numerators"] = list()
            elif keyword == "NUMBER_OF_DENOMINATORS":
                nbDen = int(split_line[1].strip())
                if nbDen == 0:
                    nbDen = None
                current_include["Denominators"] = list()
    return json.dumps(current_include, indent=2)


def convert_digfilter_include_dir(input_directory, output_directory, verbose):
    for current_response in os.listdir(input_directory):
        if not os.path.isfile(os.path.join(input_directory, current_response)):
            continue
        if verbose:
            print("Processing %s..." % current_response)
        current_file = open(os.path.join(input_directory, current_response))
        json_content = convert_digfilter_include(current_file)
        new_file = open(os.path.join(output_directory, current_response), "w")
        new_file.write(json_content)
        new_file.close()
        current_file.close()
