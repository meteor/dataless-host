# -*- coding: utf-8 -*-
import os
import json


def convert_digitizer(digitizer_file):
    digitizer_dict = dict()
    for current_line in digitizer_file:
        if ':' in current_line:
            # First suppress comment
            current_line = current_line.split("#")[0]
            split_line = current_line.split(':')
            keyword = split_line[0].strip().upper()
            if keyword == "INPUT_UNIT":
                digitizer_dict["InputUnit"] = split_line[1].strip()[1:-1]
            elif keyword == "OUTPUT_UNIT":
                digitizer_dict["OutputUnit"] = split_line[1].strip()[1:-1]
            elif keyword == "SENSITIVITY":
                digitizer_dict["Sensitivity"] = float(split_line[1].strip())
            elif keyword == "OUTPUT_SAMPLING_INTERVAL":
                digitizer_dict["OutputSamplingInterval"] = float(split_line[1].strip())
    return json.dumps(digitizer_dict, indent=2)


def convert_digitizer_dir(input_directory, output_directory, verbose):
    for current_response in os.listdir(input_directory):
        if not os.path.isfile(os.path.join(input_directory, current_response)):
            continue
        if current_response[0] == '.':
            continue
        if verbose:
            print("Processing %s..." % current_response)
        current_file = open(os.path.join(input_directory, current_response))
        json_content = convert_digitizer(current_file)
        new_file = open(os.path.join(output_directory, current_response), "w")
        new_file.write(json_content)
        new_file.close()
        current_file.close()
