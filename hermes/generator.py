#!/usr/bin/env python
# encoding: utf-8


import os
from lxml import etree
from jinja2.loaders import PackageLoader
from jinja2.environment import Environment
from oca.jinjautils.filters import xmldate, empty, pztransferfunction, \
    channel_type, sensor_type, optional_string, startswith


class XMLGenerator(object):
    """
    """
    XML_SCHEMA_PATH = os.path.dirname(__file__) + '/templates/fdsn-station-1.0.xsd'

    def __init__(self):
        self.__environement = Environment(loader=PackageLoader('hermes',
                                                               'templates'),
                                          block_start_string="<!--",
                                          block_end_string="-->",
                                          line_statement_prefix="%",
                                          trim_blocks=True)
        self.__environement.filters['xmldate'] = xmldate
        self.__environement.tests['empty'] = empty
        self.__environement.tests['startswith'] = startswith
        self.__environement.filters['pztransferfunction'] = pztransferfunction
        self.__environement.filters['sensor_type'] = sensor_type
        self.__environement.filters['channel_type'] = channel_type
        self.__environement.filters["optional_string"] = optional_string

        self.__template = self.__environement.get_template('fdsn-station.tmpl')

    def generate(self, stations, output_file=None):
        """
        Generate station XML file according to given station list.

        stations    -- The list of station
        output_file -- The output file used to dump file

        return      -- The generated XML file as string or "" in case of output
                       is not None.
        """
        if output_file is not None:
            self.__template.stream(stations).dump(output_file)
            return ""
        return self.__template.render(stations)

    def verify(self, xml_file):
        """
        Verify validity of a given station XML file.

        xml_file -- The XML file
        """

        schema_file = open(XMLGenerator.XML_SCHEMA_PATH)
        xmlschema_doc = etree.parse(schema_file)
        xmlschema = etree.XMLSchema(xmlschema_doc)
        try:
            stationxml = etree.parse(xml_file)
            xmlschema.assertValid(stationxml)
        except etree.DocumentInvalid as exception:
            return (False, exception.message)
        except etree.XMLSyntaxError as exception:
            return (False, exception.message)
        return (True, "")
