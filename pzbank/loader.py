#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
import os
import sys
import datetime
from _collections import defaultdict


PRESENT_DATETIME = datetime.datetime(2100, 1, 1)
DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"


def json_formatter(obj):
    if isinstance(obj, set):
        return list(obj)
    return obj


class TimeInterval(object):

    def __str__(self, *args, **kwargs):
        return str([self.__start, self.__end])
#        return "%s,%s,%s" % (self.__start, self.__end, self.__data)

    def __init__(self, start_date, end_date, data=None):
        """
        Initialize this interval.

        start_date -- The beginning of this interval
        end_date   -- The ending of this interval
        data       -- Optional data associated with this interval
        """
        self.__start = start_date
        self.__end = end_date
        self.__data = data

    def contains(self, tested_date):
        """
        Say if a given date is in this interval
        """
        if tested_date is None:
            return self.__end == PRESENT_DATETIME
        if self.__start <= tested_date and self.__end >= tested_date:
            return True
        return False

    def get_interval(self):
        return [self.__start, self.__end]

    def get_data(self):
        return self.__data


class PZAccess(object):
    '''
    Load PZ data bank and permit to get access by date.
    '''
    FIELD_SEPARATOR_RE = "#"
    MANUFACTURER_MODEL_RE = "[a-zA-Z0-9_-]+"
    DEVICE_MODEL_RE = r"[a-zA-Z0-9_\.-]+"
    ANA_FILTER_DEVICE_MODEL_RE = r"[a-zA-Z0-9_\.-]+"
    SERIAL_NUMBER_RE = "[a-zA-Z0-9_-]+"
    CONFIG_RE = "[a-zA-Z0-9_-]*"
    FILTER_TYPE_RE = "([a-zA-Z0-9_:@-]+)"
    COMPONENT_RE = "[a-zA-Z0-9-]+"
    DATE_RE = "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z"
    DATE_WITH_PRST_RE = "(%s|present)" % DATE_RE

    SENSOR_DAS_FILE_RE = (MANUFACTURER_MODEL_RE + FIELD_SEPARATOR_RE
                          + DEVICE_MODEL_RE + FIELD_SEPARATOR_RE
                          + CONFIG_RE + FIELD_SEPARATOR_RE
                          + SERIAL_NUMBER_RE + FIELD_SEPARATOR_RE
                          + COMPONENT_RE + "(" + FIELD_SEPARATOR_RE
                          + DATE_RE + FIELD_SEPARATOR_RE
                          + DATE_WITH_PRST_RE + ")?$")

    ANALOG_FILTER_FILE_RE = (MANUFACTURER_MODEL_RE + FIELD_SEPARATOR_RE
                             + ANA_FILTER_DEVICE_MODEL_RE + FIELD_SEPARATOR_RE
                             + FILTER_TYPE_RE + "?" + FIELD_SEPARATOR_RE
                             + SERIAL_NUMBER_RE + FIELD_SEPARATOR_RE
                             + COMPONENT_RE + "(" + FIELD_SEPARATOR_RE
                             + DATE_RE + FIELD_SEPARATOR_RE
                             + DATE_WITH_PRST_RE + ")?$")

    DIGITAL_FILTER_RE = (MANUFACTURER_MODEL_RE + FIELD_SEPARATOR_RE
                         + DEVICE_MODEL_RE + FIELD_SEPARATOR_RE
                         + FILTER_TYPE_RE + "$")

    SOFTWARE_DIGITAL_FILTER_FILE = (DEVICE_MODEL_RE + "$")

    FIELD_SEPARATOR = "#"
    THEORETICAL = "theoretical"
    (SENSOR_METATYPE, ANALOG_FILTER_METATYPE,
     DIGITIZER_METATYPE, DIGITAL_FILTER_METATYPE,
     SOFTWARE_DIGITAL_FILTER_METATYPE) = ("SENSOR", "ANALOGIC_FILTER",
                                          "DIGITIZER", "DIGITAL_FILTER",
                                          "SOFT_DIGITAL_FILTER")

    SENSOR_DIRECTORY = "sensor"
    ANALOG_FILTER_DIRECTORY = "ana_filter"
    DIGITIZER_DIRECTORY = "digitizer"
    DIGITAL_FILTER_DIRECTORY = "dig_filter"
    SOFTWARE_FILTER_DIRECTORY = "software_filter"

    METATYPE2DIR = {SENSOR_METATYPE: SENSOR_DIRECTORY,
                    ANALOG_FILTER_METATYPE: ANALOG_FILTER_DIRECTORY,
                    DIGITIZER_METATYPE: DIGITIZER_DIRECTORY,
                    DIGITAL_FILTER_METATYPE: DIGITAL_FILTER_DIRECTORY,
                    SOFTWARE_DIGITAL_FILTER_METATYPE: SOFTWARE_FILTER_DIRECTORY}

    def __init__(self, pz_root_list):
        '''
        Initialize this poles and zeros database access.

        pz_root -- The list PZ root directory.
        '''
        self.__pz_root_list = pz_root_list
        self.__databank = {PZAccess.SENSOR_METATYPE: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: dict()))))),
                           PZAccess.ANALOG_FILTER_METATYPE: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: dict()))))),
                           PZAccess.DIGITIZER_METATYPE: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: dict()))))),
                           PZAccess.DIGITAL_FILTER_METATYPE: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: dict()))))),
                           PZAccess.SOFTWARE_DIGITAL_FILTER_METATYPE: defaultdict(lambda: dict())
                           }

    def get_databank(self):
        return self.__databank

    def str2date(self, str_date):
        """
        Convert a string to a datetime object.
        The format of the date is "YYYY-MM-DDTHH:MM:SS" or "present".

        str_date -- The string date representation

        return   -- The datetime object corresponding to str_date.
                    "present" correspond to 2100_01_01.
        """
        if str_date == "present":
            return PRESENT_DATETIME
        else:
            return datetime.datetime.strptime(str_date, DATETIME_FORMAT)

    def date2str(self, date_time):
        """
        Convert a given datetime to string representation.
        The format of the datetime string is "YYYY-MM-DDTHH:MM:SS" or "present".

        date_time -- The datetime to convert

        return    -- The string representation of date_time
        """
        if date_time == PRESENT_DATETIME:
            return "present"
        else:
            return datetime.datetime.strftime(date_time, DATETIME_FORMAT)

    def __metatype2dir(self, metatype):
        """
        Give directory name associated with a given metatype.

        metatype -- The metatype

        return   -- Directory name storing this metatype
        """
        return PZAccess.METATYPE2DIR[metatype]

    def __add_digital_filter(self, manufacturer, model, name, filename):
            """
            Associate a digital filter with a PZ file.

            manufacture  -- Manufacturer of the filter
            model        -- Model of the filter
            name         -- Name of the filter
            filename     -- PZ filename

            """
            databank = self.__databank[PZAccess.DIGITAL_FILTER_METATYPE]
            if name in databank[manufacturer][model]:
                raise ValueError("Multiple definition for digital filter %s.%s.%s" %
                                 (manufacturer, model, name))
            databank[manufacturer][model][name] = filename

    def __add_software_filter(self, model, filename):
            """
            Associate a software digital filter with a PZ file.

            model        -- Model/Name of the filter
            filename     -- PZ filename
            """
            databank = self.__databank[PZAccess.SOFTWARE_DIGITAL_FILTER_METATYPE]
            if model in databank:
                raise ValueError("Multiple definition for software filter %s" %
                                 (model))
            databank[model] = filename

    def __add_theorical_pzfile(self, metatype, manufacturer, model,
                               configuration, component, filename):
            """
            Associate a device (or a part of device) with a theoretical
            PZ file.

            manufacturer -- Manufacturer/Producer of filter
            model        -- Model of the device
            filename     -- PZ filename
            """
            databank = self.__databank[metatype]
            if metatype in (PZAccess.SENSOR_METATYPE,
                            PZAccess.ANALOG_FILTER_METATYPE,
                            PZAccess.DIGITIZER_METATYPE):

                if component in databank[manufacturer][model][configuration][PZAccess.THEORETICAL]:
                    raise ValueError("Multiple definition for %s.%s.%s.%s" %
                                     (manufacturer, model,
                                      configuration, component))
                databank[manufacturer][model][configuration][PZAccess.THEORETICAL][component] = filename
            else:
                raise ValueError("Unknown metatype %s" % metatype)

    def __add_instance_pzfile(self, metatype, manufacturer, model,
                              configuration, serial_number, component,
                              begin_date, end_date, filename):
            """
            Associate a device (or a part of device) with a
            PZ file.

            manufacturer -- Manufacturer/Producer of filter
            model        -- Model of the device
            filename     -- PZ filename
            """
            databank = self.__databank[metatype]
            validity_period = TimeInterval(begin_date, end_date)
            for current_interval in databank[manufacturer][model][configuration][serial_number][component]:
                if current_interval.contains(begin_date) or current_interval.contains(end_date):
                    raise ValueError("Find intersection in validity period for %s.%s.%s.%s.%s" %
                                     (manufacturer, model, configuration, serial_number,
                                      component))
            databank[manufacturer][model][configuration][serial_number][component][validity_period] = filename

    def __add_pzfile(self, filename, metatype, manufacturer, model=None,
                     configuration=None, serial_number=None, component=None,
                     begin_date=None, end_date=None):
        if metatype == PZAccess.SOFTWARE_DIGITAL_FILTER_METATYPE:
            self.__add_software_filter(manufacturer, filename)
        elif metatype == PZAccess.DIGITAL_FILTER_METATYPE:
            self.__add_digital_filter(manufacturer, model, configuration, filename)
        else:
            if serial_number == PZAccess.THEORETICAL:
                self.__add_theorical_pzfile(metatype, manufacturer, model,
                                            configuration, component, filename)
            else:
                self.__add_instance_pzfile(metatype, manufacturer, model,
                                           configuration, serial_number,
                                           component, begin_date, end_date,
                                           filename)

    def __load_directory(self, pz_root, directory_name, metatype, verbose=0):
        """
        Load all PZ files for a given databank and meta-type
        (SENSOR, ANALOGIC_FILTER, DIGITIZER, DIGITAL_FILTER,
        SOFTWARE_DIGITAL_FILTER_METATYPE)

        pz_root        -- The PZ root directory
        directory_name -- The name of the directory
        metatype       -- Metatype
        verbose        -- Verbosity level
        """

        if not os.path.isdir(os.path.join(pz_root, directory_name)):
            print("The file %s is not a directory" %
                  os.path.join(pz_root, directory_name))
            exit(1)

        for current in os.listdir(os.path.join(pz_root,
                                               directory_name)):

            # Skip include directory
            if current == "include":
                continue
            # Verify that current file is a regular one
            if not os.path.isfile(os.path.join(pz_root, directory_name,
                                               current)):
                if verbose > 0:
                    sys.stderr.write("\tSkipping %s (not a file)\n" % current)
                continue

            if metatype in (PZAccess.SENSOR_METATYPE,
                            PZAccess.DIGITIZER_METATYPE):
                match = re.match(PZAccess.SENSOR_DAS_FILE_RE, current)
            elif metatype is PZAccess.ANALOG_FILTER_METATYPE:
                match = re.match(PZAccess.ANALOG_FILTER_FILE_RE, current)
            elif metatype is PZAccess.DIGITAL_FILTER_METATYPE:
                match = re.match(PZAccess.DIGITAL_FILTER_RE, current)
            elif metatype is PZAccess.SOFTWARE_DIGITAL_FILTER_METATYPE:
                match = re.match(PZAccess.SOFTWARE_DIGITAL_FILTER_FILE, current)

            if not match:
                if verbose > 0:
                    print("\tSkipping %s (filename not matching)" % current)
                continue

            if metatype in (PZAccess.SENSOR_METATYPE,
                            PZAccess.DIGITIZER_METATYPE):
                if len(current.split(PZAccess.FIELD_SEPARATOR)) == 7:
                    # <manufacturer>+<model>+<configuration>+<serial>+<component>+<begin_date>+<end_date>
                    splitted_file = current.split(PZAccess.FIELD_SEPARATOR)
                    begin_date = self.str2date(splitted_file[-2])
                    end_date = self.str2date(splitted_file[-1])
                    self.__add_pzfile(current, metatype, splitted_file[0],
                                      splitted_file[1], splitted_file[2],
                                      splitted_file[3], splitted_file[4],
                                      begin_date, end_date)

                else:
                    # <manufacturer>+<model>+<configuration>+"theoretical"+<component>
                    splitted_file = current.split(PZAccess.FIELD_SEPARATOR)
                    self.__add_pzfile(current, metatype, splitted_file[0],
                                      splitted_file[1], splitted_file[2],
                                      PZAccess.THEORETICAL, splitted_file[4])

            elif metatype is PZAccess.ANALOG_FILTER_METATYPE:
                if len(current.split(PZAccess.FIELD_SEPARATOR)) == 7:
                    # <manufacturer>+<model>+<filter type>+<serial>+<component>+<begin_date>+<end_date>
                    splitted_file = current.split(PZAccess.FIELD_SEPARATOR)
                    begin_date = self.str2date(splitted_file[-2])
                    end_date = self.str2date(splitted_file[-1])
                    self.__add_pzfile(current, metatype, splitted_file[0],
                                      splitted_file[1], splitted_file[2],
                                      splitted_file[3], splitted_file[4],
                                      begin_date, end_date)

                else:
                    # <manufacturer>+<model>+<filter type>+"theoretical"+<component>
                    splitted_file = current.split(PZAccess.FIELD_SEPARATOR)
                    if verbose > 2:
                        print("Add %s.%s.%s.%s.%s\n" %
                              (splitted_file[0],
                               splitted_file[1], splitted_file[2],
                               PZAccess.THEORETICAL, splitted_file[4]))
                    self.__add_pzfile(current, metatype, splitted_file[0],
                                      splitted_file[1], splitted_file[2],
                                      PZAccess.THEORETICAL, splitted_file[4])

            elif metatype is PZAccess.DIGITAL_FILTER_METATYPE:
                # <manufacturer>+<model>+<filter type>
                splitted_file = current.split(PZAccess.FIELD_SEPARATOR)
                self.__add_pzfile(current, metatype, splitted_file[0],
                                  splitted_file[1], splitted_file[2])
            elif metatype is PZAccess.SOFTWARE_DIGITAL_FILTER_METATYPE:
                # <model>
                splitted_file = current.split(PZAccess.FIELD_SEPARATOR)
                self.__add_pzfile(current, metatype, splitted_file[0])

    def load_database(self, verbose=0):
        """
        Load PZ database.

        verbose -- Verbosity level
        """
        for current_pz_root in self.__pz_root_list:
            if verbose > 2:
                print("Begin to load sensor data bank for %s" %
                      current_pz_root)
            self.__load_directory(current_pz_root, PZAccess.SENSOR_DIRECTORY,
                                  PZAccess.SENSOR_METATYPE, verbose)

            if verbose > 2:
                print("Begin to load analog  filter data bank %s" %
                      current_pz_root)
            self.__load_directory(current_pz_root, PZAccess.ANALOG_FILTER_DIRECTORY,
                                  PZAccess.ANALOG_FILTER_METATYPE, verbose)

            if verbose > 2:
                print("Begin to load digitizer data bank %s" %
                      current_pz_root)
            self.__load_directory(current_pz_root, PZAccess.DIGITIZER_DIRECTORY,
                                  PZAccess.DIGITIZER_METATYPE, verbose)

            if verbose > 2:
                print("Begin to load digital filter data bank %s" %
                      current_pz_root)
            self.__load_directory(current_pz_root,
                                  PZAccess.DIGITAL_FILTER_DIRECTORY,
                                  PZAccess.DIGITAL_FILTER_METATYPE,
                                  verbose)

            if verbose > 2:
                print("Begin to load software digital filter data bank %s" %
                      current_pz_root)
            self.__load_directory(current_pz_root,
                                  PZAccess.SOFTWARE_FILTER_DIRECTORY,
                                  PZAccess.SOFTWARE_DIGITAL_FILTER_METATYPE,
                                  verbose)

    def get_pz_content_description(self):
        """
        Give the content of PZ databank as a structured dictionnary with
        the following structure:
        {
          "sensor": {
            "<manufacturer>": {
              "<model>": {
                "serial_number": [],
                "component": [],
                "configuration": [],
                "analog_filter": [ "<filter name>"]
              }
            }
          },
          "analog_filter": {
             "<manufacturer>": {
              "<model>": {
                "serial_number": [],
                "component": []
              }
            }
          },
          "digitizer": {
             "<manufacturer>": {
              "<model>": {
                "serial_number": [],
                "component": [],
                "configuration": [],
                "analog_filter": [],
                "digital_filter": []
              }
            }
          },
          "digital_filter": {
            "<manufacturer>": {
              "<model>": []
            }
          },
          "software_filter": []
        }
        """
        result = {
            "sensor": {},
            "analog_filter": {},
            "digitizer": {},
            "digital_filter": {},
            "software_filter": []
        }

        # Sensor
        for manufacturer, manufacturer_dict in self.__databank[PZAccess.SENSOR_METATYPE].iteritems():
            if manufacturer not in result["sensor"]:
                result["sensor"][manufacturer] = {}
            for model, model_dict in manufacturer_dict.iteritems():
                if model not in result["sensor"][manufacturer]:
                    result["sensor"][manufacturer][model] = {
                        "serial_number": set(),
                        "component": set(),
                        "configuration": set(),
                        "analog_filter": set()
                    }
                    current_dict = result["sensor"][manufacturer][model]
                for configuration, configuration_dict in model_dict.iteritems():
                    for serial, serial_dict in configuration_dict.iteritems():
                        for component in serial_dict:
                            current_dict["serial_number"].add(serial)
                            current_dict["component"].add(component)
                            if configuration:
                                current_dict["configuration"].add(configuration)

        # Digitizer
        for manufacturer, manufacturer_dict in self.__databank[PZAccess.DIGITIZER_METATYPE].iteritems():
            if manufacturer not in result["digitizer"]:
                result["digitizer"][manufacturer] = {}
            for model, model_dict in manufacturer_dict.iteritems():
                if model not in result["digitizer"][manufacturer]:
                    result["digitizer"][manufacturer][model] = {
                        "serial_number": set(),
                        "component": set(),
                        "configuration": set(),
                        "analog_filter": set(),
                        "digital_filter": set()
                    }
                    current_dict = result["digitizer"][manufacturer][model]
                for configuration, configuration_dict in model_dict.iteritems():
                    for serial, serial_dict in configuration_dict.iteritems():
                        for component in serial_dict:
                            current_dict["serial_number"].add(serial)
                            current_dict["component"].add(component)
                            if configuration:
                                current_dict["configuration"].add(configuration)

        # Analog filter
        for manufacturer, manufacturer_dict in self.__databank[PZAccess.ANALOG_FILTER_METATYPE].iteritems():
            for model, model_dict in manufacturer_dict.iteritems():
                for filter_name, filter_name_dict in model_dict.iteritems():
                    if(manufacturer in result["sensor"] and model in result["sensor"][manufacturer]
                       and manufacturer in result["digitizer"] and model in result["digitizer"][manufacturer]):
                        # digital sensor, we must determine if analog filter is
                        # associated with sensor or DIGITIZER
                        component = filter_name_dict.values()[0].values()[0]
                        if component in result["sensor"][manufacturer][model]["component"]:
                            # sensor analog filter
                            result["sensor"][manufacturer][model]["analog_filter"].add(filter_name)
                        else:
                            # digitizer analog filter
                            result["digitizer"][manufacturer][model]["analog_filter"].add(filter_name)
                        continue
                    elif manufacturer in result["sensor"] and model in result["sensor"][manufacturer]:
                        # sensor analog filter
                        result["sensor"][manufacturer][model]["analog_filter"].add(filter_name)
                        continue
                    elif manufacturer in result["digitizer"] and model in result["digitizer"][manufacturer]:
                        # digitizer analog filter
                        result["digitizer"][manufacturer][model]["analog_filter"].add(filter_name)
                        continue

                    # analog filter instance
                    if manufacturer not in result["analog_filter"]:
                        result["analog_filter"][manufacturer] = {}
                    if model not in result["analog_filter"][manufacturer]:
                        result["analog_filter"][manufacturer][model] = {
                            "serial_number": set(),
                            "component": set(),
                        }
                    current_dict = result["analog_filter"][manufacturer][model]
                    for serial, serial_dict in filter_name_dict.iteritems():
                        for component in serial_dict:
                            current_dict["serial_number"].add(serial)
                            current_dict["component"].add(component)
        # Digital filter
        for manufacturer, manufacturer_dict in self.__databank[PZAccess.DIGITAL_FILTER_METATYPE].iteritems():
            for model, model_dict in manufacturer_dict.iteritems():
                for filter_name, filter_name_dict in model_dict.iteritems():
                    if manufacturer in result["digitizer"] and model in result["digitizer"][manufacturer]:
                        # digitizer digital filter
                        result["digitizer"][manufacturer][model]["digital_filter"].add(filter_name)
                        continue

                    # digital filter instance
                    if manufacturer not in result["digital_filter"]:
                        result["digital_filter"][manufacturer] = {}
                    if model not in result["digital_filter"][manufacturer]:
                        result["digital_filter"][manufacturer][model] = set()
                    result["digital_filter"][manufacturer][model].add(filter_name)

        # Software digital filter
        for filter_name in self.__databank[PZAccess.SOFTWARE_DIGITAL_FILTER_METATYPE]:
            result["software_filter"].append(filter_name)

        return result

    def __get_filename(self, metatype, manufacturer, model, component,
                       configuration=None, serial_number=None,
                       start_date=None, end_date=None):
        """
        Give the filename for given device definition.

        metatype      -- The device meta-type (SENSOR, ANALOGIC_FILTER,
                                               DIGITIZER, DIGITAL_FILTER,
                                               SOFTWARE_DIGITAL_FILTER_METATYPE)
        manufacturer  -- The device manufacturer
        model         -- The device model
        component     -- The component
        configuration -- The device configuration
        serial_number -- The device serial number
        start_date    -- The period beginning (datetime)
        end_date      -- The period ending (datetime)
        """
        if model is not None:
            model = model.lower()

        # Create filename prefix
        filename = "%s#%s" % (manufacturer, model)

        # Add configuration
        if configuration is None:
            filename = "%s#" % filename
        else:
            filename = "%s#%s" % (filename, configuration)

        if serial_number is None:
            # Process theoretical response
            filename = "%s#%s#%s" % (filename, PZAccess.THEORETICAL, component)

        else:
            # Process instance response
            if end_date is None:
                end_date = PRESENT_DATETIME
            filename = "%s#%s#%s#%s#%s" (filename, serial_number, component,
                                         self.date2str(start_date),
                                         self.date2str(end_date))
        return filename

    def new_root_databank(self, root_databank):
        """
        Create new empty root databank

        root_databank -- The new empty root databank
        """
        if root_databank in self.__pz_root_list:
            raise ValueError("The root databank %s is already declared" %
                             root_databank)

        self.__pz_root_list.append(root_databank)

    def new_pz_file(self, root_databank, file_content,
                    metatype, manufacturer, model, component,
                    configuration=None, serial_number=None,
                    start_date=None, end_date=None):
        """
        Add a new PZ file to the database

        root_databank -- The root databank where file must be created
        file_content  -- The content of the new PZ file
        metatype      -- The device meta-type (SENSOR, ANALOGIC_FILTER,
                                               DIGITIZER, DIGITAL_FILTER,
                                               SOFTWARE_DIGITAL_FILTER_METATYPE)
        manufacturer  -- The device manufacturer
        model         -- The device model
        configuration -- The device configuration
        serial_number -- The device serial number
        component     -- The component
        start_date    -- The period beginning (datetime)
        end_date      -- The period ending (datetime)
        """
        filename = self.__get_filename(metatype, manufacturer, model, component,
                                       configuration, serial_number, start_date,
                                       end_date)
        if serial_number is None:
            serial_number = PZAccess.THEORETICAL

        # Try to add to memory databank
        self.__add_pzfile(filename, metatype, manufacturer, model,
                          configuration, serial_number, component,
                          start_date, end_date)

        # Add file to filesystem databank
        filepath = os.path.join(root_databank, self.__metatype2dir(metatype),
                                filename)
        new_file = open(filepath, "w")
        new_file.write(file_content)
        new_file.close()

    def get_pz_file(self, metatype, manufacturer, model, configuration=None,
                    serial_number=None, component=None, start_date=None,
                    end_date=None):
        """
        Give the PZ file associated with a given stage for a specified date.

        metatype      -- The device meta-type (SENSOR, ANALOGIC_FILTER,
                                               DIGITIZER, DIGITAL_FILTER,
                                               SOFTWARE_DIGITAL_FILTER_METATYPE)
        manufacturer  -- The device manufacturer
        model         -- The device model
        configuration -- The device configuration
        serial_number -- The device serial number
        component     -- The component
        start_date    -- The period beginning (datetime)
        end_date      -- The period ending (datetime)

        return        -- The PZ filename
        """
        if model is not None:
            model = model.lower()
        filename = None
        databank = self.__databank[metatype]
        if metatype == PZAccess.SOFTWARE_DIGITAL_FILTER_METATYPE:
            if manufacturer not in databank:
                raise ValueError("Can't find PZ file for %s.%s" %
                                 (metatype, manufacturer))
            filename = databank[manufacturer]
        elif metatype == PZAccess.DIGITAL_FILTER_METATYPE:
            if configuration not in databank[manufacturer][model]:
                raise ValueError("Can't find PZ file for %s.%s.%s.%s" %
                                 (metatype, manufacturer, model, configuration))
            filename = databank[manufacturer][model][configuration]
        else:
            if configuration is None:
                configuration = ''
            if serial_number is None:
                if component not in databank[manufacturer][model][configuration][PZAccess.THEORETICAL]:
                    raise ValueError("Can't find theoritical file for %s.%s.%s.%s" %
                                     (manufacturer, model, configuration, component))
                filename = databank[manufacturer][model][configuration][PZAccess.THEORETICAL][component]
            else:
                # First try to find PZ file for this serial number
                for current_interval in databank[manufacturer][model][configuration][serial_number][component]:
                    if current_interval.contains(start_date):
                        if not current_interval.contains(end_date):
                            raise ValueError("Error: validity period of file %s not contains request validity period %s-%s" %
                                             (databank[manufacturer][model][configuration][serial_number][component][current_interval],
                                              start_date, end_date))
                        filename = databank[manufacturer][model][configuration][serial_number][component][current_interval]
                        break
                # If can't find specific PZ file take theoretical one
                if not filename:
                    if component not in databank[manufacturer][model][configuration][PZAccess.THEORETICAL]:
                        raise ValueError("Can't find theoritical file for %s.%s.%s.%s.%s" %
                                         (metatype, manufacturer, model, configuration, component))
                    filename = databank[manufacturer][model][configuration][PZAccess.THEORETICAL][component]

        return os.path.join(self.__metatype2dir(metatype), filename)


if __name__ == "__main__":
    import json
    pz_access = PZAccess(["/home/leon/PZdatabank"])
    pz_access.load_database(3)
    sys.stderr.write("******************************\n\n")
    result = pz_access.get_pz_content_description()
    print(json.dumps(result, default=json_formatter, indent=2))
#    current_file = pz_access.get_pz_file(PZAccess.SENSOR_METATYPE, 'manufacturer', 'model',
#                                         'config', 'serial', 'comp',
#                                         datetime.datetime(2013, 01, 23),
#                                         datetime.datetime(2013, 01, 28))
#   print(current_file)
#   try:
#       current_file = pz_access.get_pz_file(PZAccess.SENSOR_METATYPE, 'manufacturer', 'model',
#                                            'config', 'serial', 'comp',
#                                             datetime.datetime(2013, 01, 23),
#                                            datetime.datetime(2014, 03, 25))
#   except ValueError, exception:
#       pass
#    else:
#       sys.stderr.write("Excepting exception and get file %s" % current_file)
#
#   current_file = pz_access.get_pz_file(PZAccess.SENSOR_METATYPE,
#                                         'manufacturer', 'model',
#                                        'config', None, 'comp',
#                                        datetime.datetime(2013, 01, 23),
#                                        datetime.datetime(2013, 01, 28))
#    print(current_file)
