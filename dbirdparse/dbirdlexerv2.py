#!/usr/bin/python
# -*- coding: utf-8 -*-

import ply.lex as lex
from datetime import datetime, timedelta

sample_dbird = """
version 2
originating_organization "RESIF Datacentre" "noeudA_LB@goazur.unice.fr" "https://geoazur.oca.eu/" ""

begin_network
  NET1 "FR" "RESIF and other Broad-band and accelerometric permanent networks in metropolitan France" "FR@resif.fr" 1994-01-01T00:00:00Z present "RLBP"
end_network

begin_station
  "VIEF" 1996-04-12T00:00:00Z present (42.8825, none,none) (0.0229, 0.0,0.0) (1000.0, 0.0,0.0) "Viey - 65469 - Hautes-Pyrénées - Languedoc-Roussillon-Midi-Pyrénées - France"
  Contact "network.omp@resif.fr"
  Owner "Observatoire Midi-Pyrénées (OMP)"

  begin_location
    LOC1 "00" (42.8825, 0.0,0.0) (0.0229, 0.0,0.0) (1000.0, 0.0,0.0) 1.0 "unknown" "unknown"
  end_location

  begin_sensor
    VEL1 "TRILLIUM-120PA" "001627" "sensor/nanometrics#trillium-120pa##theoretical#Z" 0.0 90.0
    VEL2 "TRILLIUM-120PA" "001627" "sensor/nanometrics#trillium-120pa##theoretical#N" 180.0 0.0
    VEL3 "TRILLIUM-120PA" "001627" "sensor/nanometrics#trillium-120pa##theoretical#E" 270.0 0.0
    VEL4 "TRILLIUM-120PA" "001627" "sensor/nanometrics#trillium-120pa##theoretical#Z" 0.0 -90.0
    VEL5 "TRILLIUM-120PA" "001627" "sensor/nanometrics#trillium-120pa##theoretical#N" 0.0 0.0
    VEL6 "TRILLIUM-120PA" "001627" "sensor/nanometrics#trillium-120pa##theoretical#E" 90.0 0.0
  end_sensor

  begin_ana_filter
    DPRE1 "KEPHREN6" "770002" "ana_filter/agecodagis#kephren6#kephren6-gain1#theoretical#1-1"
    DPRE2 "KEPHREN6" "770002" "ana_filter/agecodagis#kephren6#kephren6-gain1#theoretical#1-2"
    DPRE3 "KEPHREN6" "770002" "ana_filter/agecodagis#kephren6#kephren6-gain1#theoretical#1-3"
  end_ana_filter
  begin_digitizer
    DIG1 "KEPHREN6" "770002" "digitizer/agecodagis#kephren6##theoretical#1-1"
    DIG2 "KEPHREN6" "770002" "digitizer/agecodagis#kephren6##theoretical#1-2"
    DIG3 "KEPHREN6" "770002" "digitizer/agecodagis#kephren6##theoretical#1-3"
  end_digitizer

  begin_decimation
    DPOST1 "KEPHREN6" "770002" "dig_filter/agecodagis#kephren6#kephren6-100"
    DPOST2 "KEPHREN6" "770002" "dig_filter/agecodagis#kephren6#kephren6-1"
    DPOST3 "KEPHREN6" "770002" "dig_filter/agecodagis#kephren6#kephren6-20"
  end_decimation
  begin_channel
    LOC1 HHZ VEL1 [DPRE1] DIG1 [DPOST1] CG STEIM1 2014-04-24T12:00:00Z 2015-06-19T10:00:00Z NET1
    LOC1 HHN VEL2 [DPRE2] DIG2 [DPOST1] CG STEIM1 2014-04-24T12:00:00Z 2015-06-19T10:00:00Z NET1
    LOC1 HHE VEL3 [DPRE3] DIG3 [DPOST1] CG STEIM1 2014-04-24T12:00:00Z 2015-06-19T10:00:00Z NET1
    LOC1 LHZ VEL1 [DPRE1] DIG1 [DPOST2] CG STEIM1 2014-04-24T12:00:00Z 2015-06-19T10:00:00Z NET1
    LOC1 LHN VEL2 [DPRE2] DIG2 [DPOST2] CG STEIM1 2014-04-24T12:00:00Z 2015-06-19T10:00:00Z NET1
    LOC1 LHE VEL3 [DPRE3] DIG3 [DPOST2] CG STEIM1 2014-04-24T12:00:00Z 2015-06-19T10:00:00Z NET1
    LOC1 BHZ VEL1 [DPRE1] DIG1 [DPOST3] CG STEIM1 2014-04-24T12:00:00Z 2015-06-19T10:00:00Z NET1
    LOC1 BHN VEL2 [DPRE2] DIG2 [DPOST3] CG STEIM1 2014-04-24T12:00:00Z 2015-06-19T10:00:00Z NET1
    LOC1 BHE VEL3 [DPRE3] DIG3 [DPOST3] CG STEIM1 2014-04-24T12:00:00Z 2015-06-19T10:00:00Z NET1
    LOC1 HHZ VEL4 [DPRE1] DIG1 [DPOST1] CG STEIM1 2015-06-19T13:00:00Z present NET1
    LOC1 HHN VEL5 [DPRE2] DIG2 [DPOST1] CG STEIM1 2015-06-19T13:00:00Z present NET1
    LOC1 HHE VEL6 [DPRE3] DIG3 [DPOST1] CG STEIM1 2015-06-19T13:00:00Z present NET1
    LOC1 LHZ VEL4 [DPRE1] DIG1 [DPOST2] CG STEIM1 2015-06-19T13:00:00Z present NET1
    LOC1 LHN VEL5 [DPRE2] DIG2 [DPOST2] CG STEIM1 2015-06-19T13:00:00Z present NET1
    LOC1 LHE VEL6 [DPRE3] DIG3 [DPOST2] CG STEIM1 2015-06-19T13:00:00Z present NET1
    LOC1 BHZ VEL4 [DPRE1] DIG1 [DPOST3] CG STEIM1 2015-06-19T13:00:00Z present NET1
    LOC1 BHN VEL5 [DPRE2] DIG2 [DPOST3] CG STEIM1 2015-06-19T13:00:00Z present NET1
    LOC1 BHE VEL6 [DPRE3] DIG3 [DPOST3] CG STEIM1 2015-06-19T13:00:00Z present NET1
  end_channel
end_station
"""


class DbirdLexer(object):

    keywords = ('VERSION',
                'ORIGINATING_ORGANIZATION',
                'BEGIN_AUTHOR',
                'END_AUTHOR',
                'BEGIN_COMMENT',
                'END_COMMENT',
                'BEGIN_NETWORK',
                'END_NETWORK',
                'BEGIN_STATION',
                'END_STATION',
                'CONTACT',
                'OWNER',
                'HISTORICAL_CODE',
                'ALTERNATE_CODE',
                'BEGIN_LOCATION',
                'END_LOCATION',
                'BEGIN_SENSOR',
                'END_SENSOR',
                'BEGIN_ANA_FILTER',
                'END_ANA_FILTER',
                'BEGIN_DIGITIZER',
                'END_DIGITIZER',
                'BEGIN_DECIMATION',
                'END_DECIMATION',
                'BEGIN_CHANNEL',
                'END_CHANNEL',
                'NONE'
                )

    tokens = keywords + ('INTEGER',
                         'DOUBLE',
                         'DATE',
                         'WORD',
                         'STRING',
                         'LBRACKET',
                         'COMMA',
                         'RBRACKET',
                         'OPEN_UNCERTAINTY',
                         'CLOSE_UNCERTAINTY'
                         )

    # A string containing ignored charater
    t_ignore = ' \t'

    t_LBRACKET = r'\['
    t_COMMA = r','
    t_RBRACKET = r'\]'
    t_OPEN_UNCERTAINTY = r'\('
    t_CLOSE_UNCERTAINTY = r'\)'

    def __init__(self):
        self.lexer = lex.lex(module=self, lextab="dbirdtab", debug=False)

    def t_DATE(self, t):
        # 1994-07-19T00:00:00Z
        r'([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z)|(present)'
        if t.value != "present":
            t.value = datetime.strptime(t.value, "%Y-%m-%dT%H:%M:%SZ")
        else:
            t.value = datetime.utcnow() + timedelta(days=10 * 365)
        return t

    def t_WORD(self, t):
        r'[A-Za-z][_A-Za-z0-9]*'
        if t.value.upper() in DbirdLexer.keywords:
            t.type = t.value.upper()
        return t

    def t_DOUBLE(self, t):
        r'[-+]?([0-9]*\.[0-9]+|[0-9]+\.)([eE]([+-])?[0-9]+)?'
        t.value = float(t.value)
        return t

    def t_INTEGER(self, t):
        r'[-+]?\d+'
        t.value = int(t.value)
        return t

    def t_STRING(self, t):
        r'"[^"]*"'
        t.value = t.value[1:-1]
        return t

    # A rule to track line numbers
    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    def t_COMMENT(self, t):
        r'\#.*'
        pass

    # A basic error handling rule
    def t_error(self, t):
        print("Illegal charater '%s' at line %s" %
              (t.value[0], t.lexer.lineno))
        t.lexer.skip(1)

    def test(self, data):
        self.lexer.input(data)
        while True:
            tok = self.lexer.token()
            if not tok:
                break
            print(tok)


def main():
    lexer = DbirdLexer()
    lexer.test(sample_dbird)


if __name__ == "__main__":
    main()
