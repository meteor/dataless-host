#!/usr/bin/env python
# encoding: utf-8

import sys
import json
import copy
import ply.yacc as yacc
import traceback
from datetime import datetime
from dbirdparse.dbirdlexerv2 import DbirdLexer
from pzloader.unitlistparser import UnitListParser
from pzloader.sensorlistparser import SensorListParser
from pzloader.stageparser import StageParser, StageParserException


class DbirdParserException(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)


class DbirdParserV2(DbirdLexer):
    """
    Dbird V2 parser.
    """
    def __init__(self, sensor_list_content, unit_list_content,
                 pz_root_dir, verbose=False):
        """
        Initialize this dbird parser

        sensor_list_content -- Content of sensor_list file
        unit_list_content   -- Content of unit_list file
        pz_root_dir         -- The PZ root directory
        verbose             -- Verbose flag
        """
        DbirdLexer.__init__(self)

        self.__sensor_list_parser = SensorListParser()
        self.__sensor_list_parser.parse(sensor_list_content)

        self.__unit_list_parser = UnitListParser()
        self.__unit_list_parser.parse(unit_list_content)

        self.__pz_root_dir = pz_root_dir
        self.__verbose = verbose
        self.__parser = yacc.yacc(module=self, write_tables=False, debug=False)

        self.__current_stage_index = 1

    def p_dbird(self, p):
        '''dbird : version origin author_decl networks stations_list'''
        networks = p[4]
        for current_net in p[5]:
            net = DbirdParserV2.get_object(networks, 'label',
                                           current_net['label'])
            if net is None:
                raise StageParserException("Can't find network %s in network definition" %
                                           current_net['label'])
            net['stations'] += current_net['stations']
        # Fix authors reference in network/station/channel comment
        authors = p[3]
        for current_net in networks:
            for current_comment in current_net['comments']:
                new_author_list = []
                for current_author in current_comment['authors']:
                    author = DbirdParserV2.get_object(authors, 'label', current_author)
                    if author is None:
                        raise StageParserException("Can't find author with %s label for comment %s" %
                                                   (current_author, current_comment['label']))
                    new_author_list.append(author)
                current_comment['authors'] = new_author_list

            for current_station in current_net['stations']:
                for current_comment in current_station['comments']:
                    new_author_list = []
                    for current_author in current_comment['authors']:
                        author = DbirdParserV2.get_object(authors, 'label', current_author)
                        if author is None:
                            raise StageParserException("Can't find author with %s label for comment %s" %
                                                       (current_author, current_comment['label']))
                        new_author_list.append(author)
                    current_comment['authors'] = new_author_list

        p[0] = {'version': p[1],
                'source': p[2],
                'sender': 'RESIF',
                'module': 'pydataless',
                'created': datetime.utcnow(),
                'networks': networks
                }

    def p_version(self, p):
        '''version : VERSION INTEGER'''
        p[0] = p[2]

    def p_origin(self, p):
        #                                     desc   mail   URI    phone
        '''origin : ORIGINATING_ORGANIZATION STRING STRING STRING STRING'''
        p[0] = {'description': p[2],
                'email': p[3],
                'uri': p[4],
                'phone': p[5]
                }

    #
    # Phone
    #
    def p_phone_list(self, p):
        '''phone_list : phone_number
                      | phone_list COMMA phone_number
                      | '''
        if len(p) == 2:
            p[0] = [p[1]]
        elif len(p) == 4:
            p[0] = p[1] + [p[3]]
        else:
            p[0] = list()

    def p_phone_number(self, p):
        '''phone_number : INTEGER INTEGER STRING
                        | INTEGER STRING'''
        if len(p) == 4:
            p[0] = {
                'country_code': p[1],
                'area_code': p[2],
                'phone_number': p[3]
            }
        else:
            p[0] = {
                'country_code': None,
                'area_code': p[1],
                'phone_number': p[2]
            }

    #
    # Authors
    #
    def p_author_decl(self, p):
        '''author_decl : BEGIN_AUTHOR author_list END_AUTHOR
                       |'''
        if len(p) == 4:
            p[0] = p[2]
        else:
            p[0] = []

    def p_author_list(self, p):
        '''author_list : author_list author_def
                       | author_def
                       |'''
        if len(p) == 3:
            p[0] = p[1] + [p[2]]
        elif len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = list()

    def p_author_def(self, p):
        #                CODE            names                          agency                        mail                        phones
        '''author_def : WORD LBRACKET string_list RBRACKET  LBRACKET string_list RBRACKET LBRACKET string_list RBRACKET LBRACKET phone_list RBRACKET'''
        p[0] = {'label': p[1],
                'names': p[3],
                'agency': p[6],
                'mail': p[9],
                'phone': p[12]
                }

    #
    # Networks
    #

    # Network comment
    def p_network_comment_decl(self, p):
        '''network_comment_decl : BEGIN_COMMENT network_comment_list END_COMMENT
                                | BEGIN_COMMENT END_COMMENT'''
        if len(p) == 3:
            p[0] = []
        else:
            p[0] = p[2]

    def p_network_comment_list(self, p):
        '''network_comment_list : network_comment
                                | network_comment_list network_comment'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

    def p_network_comment(self, p):
        '''network_comment : WORD STRING DATE DATE LBRACKET word_list RBRACKET'''
        p[0] = {
            'label': p[1],
            'comment': p[2],
            'start_date': p[3],
            'end_date': p[4],
            'authors': p[6]
        }

    def p_networks(self, p):
        '''networks : BEGIN_NETWORK network_list END_NETWORK
                    | BEGIN_NETWORK network_comment_decl network_list END_NETWORK'''
        if len(p) == 4:
            p[0] = p[2]
            for current_net in p[2]:
                if len(current_net['comments']) != 0:
                    raise StageParserException("Error : Network %s have comments and no comments define" %
                                               current_net['label'])
        else:
            p[0] = p[3]
            # Now replace comment code by comment ref
            for current_net in p[3]:
                comment_list = []
                for current_label in current_net['comments']:
                    comment = DbirdParserV2.get_object(p[2], 'label',
                                                       current_label)
                    comment_list.append(comment)
                current_net['comments'] = comment_list

    def p_network_list(self, p):
        '''network_list : network_def
           | network_list network_def'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

    def p_network_def(self, p):
        #                CODE FDSN   DESC   mail   BEGIN END  ALTERNATE            COMMENT
        '''network_def : WORD STRING STRING STRING DATE  DATE  STRING    LBRACKET word_list RBRACKET'''
        p[0] = {'label': p[1],
                'code': p[2],
                'description': unicode(p[3], "utf8"),
                'mail': p[4],
                'start_date': p[5],
                'end_date': p[6],
                'alternate_name': p[7],
                'comments': p[9],
                'stations': []
                }

    #
    # Station/Channel comment
    #
    def p_comment_decl(self, p):
        '''comment_decl : BEGIN_COMMENT comment_list END_COMMENT
                        | BEGIN_COMMENT END_COMMENT
                        | empty'''
        if len(p) == 4:
            p[0] = p[2]
        else:
            p[0] = []

    def p_comment_list(self, p):
        '''comment_list : comment
                        | comment_list comment'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

    def p_comment(self, p):
        #            label   comment  start   end               author             S|C
        '''comment : WORD    STRING   DATE    DATE   LBRACKET word_list  RBRACKET WORD'''
        p[0] = {
            'label': p[1],
            'comment': p[2],
            'start_date': p[3],
            'end_date': p[4],
            'authors': p[6],
            'type': p[8]
        }

    #
    # Stations
    #
    def p_station_list(self, p):
        '''stations_list : station
           | stations_list station'''
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = p[1]
            for current_net in p[2]:
                net = DbirdParserV2.get_object(p[1], "label",
                                               current_net['label'])
                if net is None:
                    p[0].append(current_net)
                else:
                    net['stations'] += current_net['stations']

    def p_station(self, p):
        '''station : BEGIN_STATION station_infos contact owner historical_code alternate_code comment_decl location_decl sensor_decl ana_filter_decl digitizer_decl decimation_decl channel_decl END_STATION'''
        station_infos = p[2]
        contact = p[3]
        owner = p[4]
        historical_code = p[5]
        alternate_code = p[6]
        comments = p[7]
        locations = p[8]
        sensors = p[9]
        anafilters = p[10]
        digitizers = p[11]
        digfilters = p[12]
        channels = p[13]

        result = []
        for current_channel in channels:
            current_net = DbirdParserV2.get_object(result, "label",
                                                   current_channel['network'])
            if current_net is None:
                current_net = {'label': current_channel['network'],
                               'stations': []}
                result.append(current_net)

            current_station = self.get_object(current_net['stations'], "code",
                                              station_infos['code'])
            if current_station is None:
                current_station = {'code': station_infos['code'],
                                   'owner': owner,
                                   'start_date': station_infos['start_date'],
                                   'end_date': station_infos['end_date'],
                                   'latitude': station_infos['latitude'],
                                   'longitude': station_infos['longitude'],
                                   'elevation': station_infos['elevation'],
                                   'contact': contact,
                                   'toponyme': station_infos['toponyme'],
                                   'comments': copy.deepcopy(comments),
                                   'channels': []}
                if historical_code is not None:
                    current_station['historical_code'] = historical_code
                if alternate_code is not None:
                    current_station['alternate_code'] = alternate_code

                current_net['stations'].append(current_station)

            channel_location = DbirdParserV2.get_object(locations,
                                                        'label',
                                                        current_channel['location'])
            channel_sensor = DbirdParserV2.get_object(sensors,
                                                      'label',
                                                      current_channel['sensor'])
            channel_ana_filter = []
            for current in current_channel['analog_filter']:
                channel_ana_filter.append(DbirdParserV2.get_object(anafilters, 'label', current))

            channel_digitizer = DbirdParserV2.get_object(digitizers,
                                                         'label',
                                                         current_channel['digitizer'])
            channel_dig_filter = []
            for current in current_channel['digital_filter']:
                channel_dig_filter.append(DbirdParserV2.get_object(digfilters, 'label', current))

            if channel_location is None:
                raise StageParserException("Error : Can't find declation of location %s" %
                                           current_channel['location'])

            if channel_sensor is None:
                raise StageParserException("Error : Can't find declation of sensor %s" %
                                           current_channel['sensor'])

            if channel_ana_filter is None and current_channel['analog_filter'] != "none":
                raise StageParserException("Error : Can't find declation of analog filter %s" %
                                           current_channel['analog_filter'])

            if channel_digitizer is None:
                raise StageParserException("Error : Can't find declation of digitizer %s" %
                                           current_channel['digitizer'])

            if channel_dig_filter is None and current_channel['digital_filter'] != "none":
                raise StageParserException("Error : Can't find declation of digital filter %s" %
                                           current_channel['digital_filter'])

            comment_list = []
            for current_comment in current_channel['comments']:
                comment = DbirdParserV2.get_object(current_station['comments'],
                                                   'label', current_comment)
                if comment['type'] != 'C':
                    raise StageParserException("Expecting 'C' comment type and get %s" % comment['type'])
                comment_list.append(comment)

            new_channel = {'location_code': channel_location['code'],
                           'name': current_channel['code'],
                           'start_date': current_channel['start_date'],
                           'end_date': current_channel['end_date'],
                           'latitude': channel_location['latitude'],
                           'longitude': channel_location['longitude'],
                           'elevation': channel_location['elevation'],
                           'depth': channel_location['depth'],
                           'azimut': channel_sensor['azimuth'],
                           'dip': channel_sensor['dip'],
                           'sample_rate': 0,
                           'comments': comment_list,
                           'sensor': copy.deepcopy(channel_sensor),
                           'ana_filter': copy.deepcopy(channel_ana_filter),
                           'digitizer': copy.deepcopy(channel_digitizer),
                           'dig_filter': copy.deepcopy(channel_dig_filter),
                           'encoding': current_channel['encoding'],
                           'flags': current_channel['flags']
                           }

            try:
                new_channel['response'] = {}
                if new_channel['sensor']['pz_file_content']['TRANSFER_FUNCTION_TYPE'] in ('ANALOG', 'LAPLACE'):
                    new_channel['response']['normalization_frequency'] = new_channel['sensor']['pz_file_content']['TRANSFER_NORMALIZATION_FREQUENCY']
                    new_channel['response']['sensitivity'] = new_channel['sensor']['pz_file_content']['SENSITIVITY']
                elif new_channel['sensor']['pz_file_content']['TRANSFER_FUNCTION_TYPE'] in ('POLYNOMIAL'):
                    new_channel['response']['sensitivity'] = 1
                new_channel['sensor']['pz_file_content']['STAGE_INDEX'] = 1
                current_stage_index = 2

                # Fix index of stage for analog filter
                if len(new_channel['ana_filter']):
                    for current_filter in new_channel['ana_filter']:
                        if('ZEROES' in current_filter['pz_file_content'][0] and len(current_filter['pz_file_content'][0]['ZEROES']) != 0 or
                           'POLES' in current_filter['pz_file_content'][0] and len(current_filter['pz_file_content'][0]['POLES']) != 0):
                            new_channel['response']['normalization_frequency'] = current_filter['pz_file_content'][0]['TRANSFER_NORMALIZATION_FREQUENCY']
                        if('TRANSFER_NORMALIZATION_FREQUENCY' not in current_filter['pz_file_content'][0] and
                           new_channel['sensor']['pz_file_content']['TRANSFER_FUNCTION_TYPE'] in ('ANALOG', 'LAPLACE')):
                            current_filter['pz_file_content'][0]['TRANSFER_NORMALIZATION_FREQUENCY'] = new_channel['response']['normalization_frequency']
                            new_channel['response']['sensitivity'] *= current_filter['pz_file_content'][0]['SENSITIVITY']
                        for current_stage in current_filter['pz_file_content']:
                            current_stage['STAGE_INDEX'] = current_stage_index
                            current_stage_index += 1

                # Fix index of stage for digitizer
                new_channel['response']['sensitivity'] *= new_channel['digitizer']['pz_file_content']['SENSITIVITY']
                new_channel['digitizer']['pz_file_content']['STAGE_INDEX'] = current_stage_index
                current_stage_index += 1

                # Fix index of stage for digital filter
                if len(new_channel['dig_filter']):
                    for current_filter in new_channel['dig_filter']:
                        for current_stage in current_filter['pz_file_content']:
                            current_stage['STAGE_INDEX'] = current_stage_index
                            current_stage_index += 1
            except Exception as exception:
                raise ValueError("Error in %s_%s_%s_%s: %s" %
                                 (current_channel['network'], station_infos['code'],
                                  new_channel['location_code'], new_channel['name'],
                                  exception))
            current_station['channels'].append(new_channel)

        for current_net in result:
            for current_station in current_net['stations']:
                current_station['open_channel'] = self.open_channel(current_station['channels'])

        self.process_station(result)
        p[0] = result

    def p_optional_double(self, p):
        '''optional_double : DOUBLE
                           | NONE'''
        if type(p[1]) == str and p[1].upper() == 'NONE':
            p[0] = None
            return
        p[0] = p[1]

    def p_uncertainty_double(self, p):
        # ( 12.4 , 0.0, 0.0 )
        '''uncertainty_double : OPEN_UNCERTAINTY DOUBLE COMMA optional_double COMMA optional_double CLOSE_UNCERTAINTY'''
        p[0] = {
            'value': p[2],
            'min_error': p[4],
            'plus_error': p[6]
        }

    def p_station_infos(self, p):
        #                  name   sdate                edate           lat                  long               ele               toponyme
        #                  "CALF" 1994-07-19T00:00:00Z present (43.7528, 0.0, 0.0) (6.9218, 0.0, 0.0) (1242.0, 0.0, 0.0) "plateau de Calern - 06037, Caussols - Alpes-Maritimes - Provence-Alpes-Côte d'Azur - France"
        '''station_infos : STRING  DATE                 DATE   uncertainty_double  uncertainty_double uncertainty_double  STRING'''
        p[0] = {'code': p[1],
                'start_date': p[2],
                'end_date': p[3],
                'latitude': p[4],
                'longitude': p[5],
                'elevation': p[6],
                'toponyme': unicode(p[7], "utf8")}

    def p_contact(self, p):
        '''contact : CONTACT STRING'''
        p[0] = {'email': p[1]}

    def p_owner(self, p):
        '''owner : OWNER STRING'''
        p[0] = unicode(p[2], "utf8")

    #
    # Alternate and historical code
    #
    def p_alternate_code(self, p):
        '''alternate_code : ALTERNATE_CODE STRING
                          | empty'''
        if len(p) == 3:
            p[0] = p[2]
        else:
            p[0] = None

    def p_historical_code(self, p):
        '''historical_code : HISTORICAL_CODE STRING
                           | empty'''
        if len(p) == 3:
            p[0] = p[2]
        else:
            p[0] = None

    #
    # Locations
    #
    def p_location_decl(self, p):
        '''location_decl : BEGIN_LOCATION  location_list END_LOCATION'''
        p[0] = p[2]

    def p_location_list(self, p):
        '''location_list : location
           | location_list location'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

    def p_location(self, p):
        #                 label       code         Latitude            Longitude            Elevation          Depth   vault  geology
        '''location :     WORD       STRING   uncertainty_double    uncertainty_double   uncertainty_double    DOUBLE  STRING STRING'''
        p[0] = {'label': p[1],
                'code': p[2],
                'latitude': p[3],
                'longitude': p[4],
                'elevation': p[5],
                'depth': p[6],
                'vault': p[7],
                'geology': p[8]
                }

    #
    # Sensors
    #
    def p_sensor_decl(self, p):
        '''sensor_decl : BEGIN_SENSOR sensor_list END_SENSOR'''
        p[0] = p[2]

    def p_sensor_list(self, p):
        '''sensor_list : sensor
           | sensor_list sensor'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

    def p_sensor(self, p):
        #           Label  Name  Serial number  pz file  azimuth   dip
        '''sensor : WORD   STRING    STRING     STRING   DOUBLE  DOUBLE'''
        sensor_desc = self.__sensor_list_parser.get_sensor_def(p[2])

        # Loading PZ file associated with this sensor component
        if self.__verbose:
            sys.stderr.write("Loading %s\n" % p[4])
        try:
            pz_file = open("%s/%s" % (self.__pz_root_dir, p[4]))
            pz_file_content = pz_file.read()
        except IOError as e:
            raise e
        parser = StageParser(self.__pz_root_dir, self.__verbose)
        try:
            content = parser.parse_str(pz_file_content, pz_file)
        except Exception as e:
            sys.stderr.write("Error during %s parsing : %s\n" % (p[4], e))
            sys.stderr.write("%s\n" % traceback.format_exc())
            raise e

        if content is not None:
            try:
                self._fix_unit_name(content)
            except DbirdParserException as exception:
                sys.stderr.write(content)
                raise StageParserException("%s when reading %s" % (exception, pz_file))
        if(content["INPUT_UNIT"] != 'M/S**2' and
           ('NO_MIN_FREQ' not in content or content['NO_MIN_FREQ'].upper() != "TRUE") and
           'POLES' in content):
            StageParser.verify_tnf(content['POLES'],
                                   content['TRANSFER_NORMALIZATION_FREQUENCY'],
                                   content['TRANSFER_FUNCTION_TYPE'])
        p[0] = {'label': p[1],
                'name': p[2],
                'serial_number': p[3],
                'pz_file': p[4],
                'pz_file_content': content,
                'azimuth': p[5],
                'dip': p[6],
                'type': sensor_desc['type'],
                'period': sensor_desc['period'],
                'description': sensor_desc['description']}

    #
    # Analogic filters
    #
    def p_ana_filter_decl(self, p):
        '''ana_filter_decl : empty
           | BEGIN_ANA_FILTER ana_filter_list END_ANA_FILTER'''
        if len(p) == 2:
            p[0] = []
        else:
            p[0] = p[2]

    def p_ana_filter_list(self, p):
        '''ana_filter_list : ana_filter
          | ana_filter_list ana_filter'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

    def p_ana_filter(self, p):
        #              Label  Type  Serial number  pz file
        '''ana_filter : WORD STRING    STRING      STRING'''
        # Loading PZ file associated with this digitizer component
        if self.__verbose:
            sys.stderr.write("Loading %s\n" % p[4])

        try:
            pz_file = open("%s/%s" % (self.__pz_root_dir, p[4]))
            pz_file_content = pz_file.read()
        except IOError as e:
            raise e

        parser = StageParser(self.__pz_root_dir)
        try:
            content = parser.parse_str(pz_file_content, pz_file)
        except StageParserException as e:
            sys.stderr.write("Error during %s parsing : %s\n" % (p[4], e))
            raise e
        except Exception as e:
            sys.stderr.write("Error during %s parsing : %s\n" % (p[4], e))
            raise e

        if content is not None:
            try:
                self._fix_unit_name(content[0])
            except DbirdParserException as exception:
                raise StageParserException("%s when reading %s" % (exception, pz_file))
        p[0] = {'label': p[1],
                'type': p[2],
                'serial_number': p[3],
                'pz_file': p[4],
                'pz_file_content': content}

    #
    # Digitizers
    #
    def p_digitizer_decl(self, p):
        '''digitizer_decl : empty
           | BEGIN_DIGITIZER digitizer_list END_DIGITIZER'''
        if len(p) == 2:
            p[0] = []
        else:
            p[0] = p[2]

    def p_digitizer_list(self, p):
        '''digitizer_list : digitizer
           | digitizer_list digitizer'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

    def p_digitizer(self, p):
        #              Label  Type  Serial number  pz file
        '''digitizer : WORD STRING      STRING     STRING'''

        # Loading PZ file associated with this digitizer component
        if self.__verbose:
            sys.stderr.write("Loading %s\n" % p[4])

        try:
            pz_file = open("%s/%s" % (self.__pz_root_dir, p[4]))
            pz_file_content = pz_file.read()
        except IOError as e:
            raise e
        parser = StageParser(self.__pz_root_dir)
        try:
            content = parser.parse_str(pz_file_content, pz_file)
        except StageParserException as e:
            sys.stderr.write("Error during %s parsing : %s\n" % (p[4], e))
            raise e

        if content is not None:
            try:
                self._fix_unit_name(content)
            except DbirdParserException as exception:
                raise StageParserException("%s when reading %s" % (exception, pz_file))
        p[0] = {'label': p[1],
                'type': p[2],
                'serial_number': p[3],
                'pz_file': p[4],
                'pz_file_content': content}

    #
    # Digital filters
    #
    def p_decimation_decl(self, p):
        '''decimation_decl : empty
           | BEGIN_DECIMATION decimation_list END_DECIMATION'''
        if len(p) == 2:
            p[0] = []
        else:
            p[0] = p[2]

    def p_decimation_list(self, p):
        '''decimation_list : decimation
           | decimation_list decimation'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

    def p_decimation(self, p):
        #               Label  Type  Serial number  pz file
        '''decimation : WORD  STRING     STRING     STRING'''

        # Loading PZ file associated with this digitizer component
        if self.__verbose:
            sys.stderr.write("Loading %s\n" % p[4])

        try:
            pz_file = open("%s/%s" % (self.__pz_root_dir, p[4]))
            pz_file_content = pz_file.read()
        except IOError as e:
            raise e
        parser = StageParser(self.__pz_root_dir)
        try:
            content = parser.parse_str(pz_file_content, pz_file)
        except Exception as e:
            sys.stderr.write("Error during %s parsing : %s\n" % (p[4], e))
            sys.stderr.write("%s\n" % traceback.format_exc())
            raise e

#        if content is not None:
#            try:
#                for current in content:
#                    self._fix_unit_name(current)
#            except DbirdParserException as exception:
#                print(current)
#                print("%s when reading %s" % (exception, pz_file))
#                sys.exit(1)

        p[0] = {'label': p[1],
                'type': p[2],
                'serial_number': p[3],
                'pz_file': p[4],
                'pz_file_content': content}

    #
    # Channels
    #
    def p_channel_decl(self, p):
        '''channel_decl : BEGIN_CHANNEL channel_list  END_CHANNEL'''
        p[0] = p[2]

    def p_channel_list(self, p):
        '''channel_list : channel
           | channel_list channel'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

    def p_channel(self, p):
        #            loc name sensor        afilterlist         dig            dfilterlist         flags  encoding      sdate                  edate           net
        #            LOC1 HHZ  VEL1           [PRE1]            DIG1           [DPOST1]             CG     STEIM1 1996-08-22T08:31:00Z 1997-03-26T10:00:00Z    NET1            comment_list
        '''channel : WORD WORD WORD LBRACKET word_list RBRACKET WORD  LBRACKET word_list RBRACKET  WORD     WORD     DATE                 DATE                 WORD    LBRACKET word_list RBRACKET'''
        p[0] = {'location': p[1],
                'code': p[2],
                'sensor': p[3],
                'analog_filter': p[5],
                'digitizer': p[7],
                'digital_filter': p[9],
                'flags': [f for f in p[11]],
                'encoding': p[12],
                'start_date': p[13],
                'end_date': p[14],
                'network': p[15],
                'comments': p[17]}

    def p_word_list(self, p):
        '''word_list : empty
           | WORD
           | WORD COMMA word_list'''
        if len(p) == 2:
            if p[1] is None:
                p[0] = []
            else:
                p[0] = [p[1]]
        else:
            p[0] = [p[1]] + p[3]

    def p_string_list(self, p):
        '''string_list : empty
           | STRING
           | STRING COMMA string_list'''
        if len(p) == 2:
            if p[1] is None:
                p[0] = []
            else:
                p[0] = [unicode(p[1], "utf8")]
        else:
            p[0] = [unicode(p[1], "utf8")] + p[3]

    def p_empty(self, p):
        '''empty :'''
        pass

    def p_error(self, p):
        """
        Very simple error processing.
        """
        if p is not None:
            sys.stderr.write("dbird parser error when reading %s (%s) at line %d\n" %
                             (p.type, p.value, self.lexer.lineno))
        else:
            sys.stderr.write("dbird parser error at line %d\n" %
                             self.lexer.lineno)

#
# Utility functions
#
    @staticmethod
    def compute_sample_rate(sample_rate_str):
        """
        Get string representing sample per second and
        return sampling rate as double.

        sample_rate_str -- The string

        return          -- Sampling rate as double
        """
        if sample_rate_str == 's1':
            return 125.0 / 128
        elif sample_rate_str[0] == 's':
            return (1.0 / int(sample_rate_str[1:]))
        else:
            return int(sample_rate_str)

    def process_station(self, networks):
        """
        Fix channel sample rate for every stations of every networks.

        networks -- The list of network
        """
        for current_net in networks:
            for current_station in current_net['stations']:
                for current_channel in current_station['channels']:
                    # Channel sample rate computation
                    if(current_channel['dig_filter'] is not None and
                       len(current_channel['dig_filter']) != 0 and
                       current_channel['dig_filter'][-1]['pz_file'] is not None):
                        if '#' in current_channel['dig_filter'][-1]['pz_file']:
                            # Classic digital filter manufcturer#model#<string finish with sample rate>
                            sample_rate_str = current_channel['dig_filter'][-1]['pz_file'].split('#')[2].split('-')[-1]
                        else:
                            # Software digital filter name-<input sample rate>-<output sample rate>
                            sample_rate_str = current_channel['dig_filter'][-1]['pz_file'].split('-')[-1]
                        current_channel['sample_rate'] = DbirdParserV2.compute_sample_rate(sample_rate_str)
                    else:
                        current_channel['sample_rate'] = 1 / current_channel['digitizer']['pz_file_content']["OUTPUT_SAMPLING_INTERVAL"]
                    # Now set upper valid frequency bound if need
                    if(current_channel['sensor']['pz_file_content']['TRANSFER_FUNCTION_TYPE'] == 'POLYNOMIAL' and
                       'UPPER_VALID_FREQUENCY_BOUND' not in current_channel['sensor']['pz_file_content']):
                        current_channel['sensor']['pz_file_content']['UPPER_VALID_FREQUENCY_BOUND'] = float(current_channel['sample_rate']) / 2

    @staticmethod
    def get_object(object_list, index, value):
        """
        Return an object from a list according to criteria.

        object_list -- The list of object
        index       -- The index of criteria (can be integer or string)
        value       -- The value used to select object (object[index] == value)

        return      -- The corresponding object if find and or None otherwise.
        """
        for current_object in object_list:
            if current_object[index] == value:
                return current_object
        return None

    def open_channel(self, channel_list):
        """
        Give the number of open channel in a given channel list.

        channel_list -- The channel list

        return       -- The number of open channel in the given list.
        """
        result = 0
        for current_channel in channel_list:
            if current_channel['end_date'] > datetime.utcnow():
                result += 1
        return result

    def _fix_unit_name(self, pz_file_content):
        """
        Fix the unit name present in given pz_file_content.

        pz_file_content -- The PZ file content as dictionnary
        """
        for current_unit in ['INPUT_UNIT', 'OUTPUT_UNIT']:
            if current_unit not in pz_file_content:
                raise DbirdParserException("Can't find %s" % current_unit)
            to_change = pz_file_content[current_unit].upper()
            tmp = self.__unit_list_parser.get_unit_def(to_change)
            if tmp is None:
                raise DbirdParserException("%s is not a valid unit" % pz_file_content[current_unit])
            pz_file_content[current_unit] = tmp['xml_code']

    def parse_str(self, inputs):
        """
        Parse the given string as a Dbird file content.
        """
        result = self.__parser.parse(inputs)
        return result


def main():
    """
    Basic main function.
    """
    # Loadding sensor list file
    try:
        sensor_list_file = open("/home/leon/tmp/PZ/sensor_list")
        sensor_list_content = sensor_list_file.readlines()
    except IOError as exception:
        raise exception

    try:

        if len(sys.argv) >= 2:
            dbird_file = open(sys.argv[1])
        else:
            dbird_file = open("../hermes/dbirdv2.tgrs")
        dbird_content = dbird_file.read()
    except IOError as exception:
        raise exception

    parser = DbirdParserV2(sensor_list_content, "/home/leon/tmp/PZ", True)
    # parser = DbirdParserV2(StringIO(sensor_list_content), "/home/leon/tmp/PZ", False)
    result = parser.parse_str(dbird_content)
    json.dump(result, fp=sys.stdout, indent=2,
              default=lambda obj: str(obj))


if __name__ == "__main__":
    main()
