#!/usr/bin/python
# -*- coding: utf-8 -*-


class SensorListParser(object):
    '''
    Sensor_list parser.
    '''

    def __init__(self):
        self.__result = {}

    def parse(self, content):
        """
        Parse content as a sensor_list file content.

        line beginning by '#' are ignored (comment line)
        each other line must have the following form

        # code            super_type  Type    Period          Description
        CMG-5TD_DM24MK3    CMG-5       ACC      LP       Numerical accelerometer
        """
        line_index = 1
        for current_line in content:
            current_line = current_line.strip()
            if len(current_line) == 0 or current_line[0] is '#':
                line_index += 1
                continue
            current_line_field = current_line.split()
            if len(current_line_field) < 5:
                raise SyntaxError("line %d (%s) is invalid" %
                                  (line_index, current_line))
            if current_line_field[0] in self.__result:
                raise SyntaxError("The sensor <%s> is define more than one time"
                                  % current_line_field[0])
            self.__result[current_line_field[0]] = {'type': current_line_field[2],
                                                    'period': current_line_field[3],
                                                    'description': " ".join(current_line_field[4:])}
            line_index += 1

    def get_sensor_def(self, name):
        """
        Give the sensor definition according to given name.

        name   -- The sensor name

        return -- The sensor description or None if sensor name is unknown
        """
        if name in self.__result:
            return self.__result[name]
        return None
