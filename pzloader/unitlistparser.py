#!/usr/bin/python
# -*- coding: utf-8 -*-


class UnitListParser(object):
    '''
    unit_list parser.
    '''

    def __init__(self):
        self.__result = {}

    def parse(self, content):
        """
        Parse content as a unit_list file content.

        line beginning by '#' are ignored (comment line)
        each other line must have the following form


        # PZ code   XML code               Description
        #  M/S/S     M/S**2     meter per second per second (acceleration)
        """
        line_index = 1
        for current_line in content:
            current_line = current_line.strip()
            if len(current_line) == 0 or current_line[0] is '#':
                line_index += 1
                continue
            current_line_field = current_line.split()
            if len(current_line_field) < 3:
                raise SyntaxError("line %d (%s) is invalid" %
                                  (line_index, current_line))
            if current_line_field[0] in self.__result:
                raise SyntaxError("The unit <%s> is define more than one time"
                                  % current_line_field[0])
            self.__result[current_line_field[0]] = {'xml_code': current_line_field[1],
                                                    'description': " ".join(current_line_field[2:])}
            line_index += 1

    def get_unit_def(self, name):
        """
        Give the unit definition according to given name.

        name   -- The unit name

        return -- The unit description or None if unit name is unknown
        """
        if name in self.__result:
            return self.__result[name]
        return None
