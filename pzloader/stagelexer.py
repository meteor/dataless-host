#!/usr/bin/python
# -*- coding: utf-8 -*-

import ply.lex as lex


sample_stage = """
begin sensor
  Response_name : "SENSOR RESPONSE" # optionel
  Response_type : "THEORETICAL" # optionel
  Transfer_function_type : "ANALOG"
  Poles_and_Zeroes_unit : "Hz" # optionel
  Input_unit : "m/s/s"
  Output_unit : "V"
  Sensitivity : 1.0254
  Transfer_normalization_frequency : 1
 Number_of_zeroes : 1 #  en Hz
 0.000000e+00 0.000000e+00
 Number_of_poles : 4 #  en Hz
 -5.000000e-02 0.000000e+00
 -6.560000e+01 1.040000e+02
 -6.560000e+01 -1.040000e+02
 -5.000000e+01 0.000000e+00
end sensor
"""


class StageLexer(object):

    keywords = ('BEGIN',
                'SENSOR',
                'ANA_FILTER',
                'DIGITIZER',
                'DIGITAL_FILTER',
                'STAGE',
                'INCLUDE',
                'END',
                'NUMBER_OF_POLES',
                'NUMBER_OF_ZEROES',
                'NUMBER_OF_COEFFICIENTS',
                'NUMBER_OF_NUMERATORS',
                'NUMBER_OF_DENOMINATORS')

    tokens = keywords + ('COLON',
                         'INTEGER',
                         'DOUBLE',
                         'WORD',
                         'STRING'
                         )

    # A string containing ignored charaters
    t_ignore = ' \t'

    t_COLON = r'\:'

    def __init__(self):
        self.lexer = lex.lex(module=self, debug=False)
        self.__filename = None

    def t_WORD(self, t):
        r'[A-Za-z][_A-Za-z0-9]*'
        if t.value.upper() in StageLexer.keywords:
            t.type = t.value.upper()
        return t

    def t_DOUBLE(self, t):
        r'[-+]?([0-9]*\.[0-9]+|[0-9]+\.)([eE]([+-])?[0-9]+)?'
        t.value = float(t.value)
        return t

    def t_INTEGER(self, t):
        r'\d+'
        t.value = int(t.value)
        return t

    def t_STRING(self, t):
        r'"[^"]*"'
        t.value = t.value[1:-1]
        return t

    # A rule to track line numbers
    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    def t_COMMENT(self, t):
        r'\#.*'
        pass

    # A basic error handling rule
    def t_error(self, t):
        print("Illegal charater '%s' at line %s (%s)" %
              (t.value[0], t.lexer.lineno, self.__filename))
        t.lexer.skip(1)

    def set_filename(self, filename):
        """
        Set the name of the file file currently parsed.

        filename -- The name of the currently parsed file.
        """
        self.__filename = filename

    def test(self, data):
        self.lexer.input(data)
        while True:
            tok = self.lexer.token()
            if not tok:
                break
            print(tok)


def main():
    lexer = StageLexer()
    lexer.test(sample_stage)


if __name__ == "__main__":
    main()
