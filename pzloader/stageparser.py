#!/usr/bin/env python
# encoding: utf-8
import sys
import json
import copy
import math
import cmath
from ply import yacc
from pzloader.stagelexer import StageLexer, sample_stage


class StageParserException(BaseException):
    pass


class StageParser(StageLexer):
    '''
    classdocs
    '''

    def __init__(self, pz_root, verbose=False):
        '''
        Initialize this stage parser.

        pz_root  -- The PZ root directory
        '''
        StageLexer.__init__(self)
        self.__pz_root = pz_root
        self.__verbose = verbose
        self.__parser = yacc.yacc(module=self, write_tables=False, debug=False)

    def p_stage(self, p):
        """stage : BEGIN element"""
        p[0] = p[2]

    def p_element(self, p):
        """element :  sensor
                    | analogic_filters_cascade
                    | digitizer
                    | digital_filters_cascade"""
        p[0] = p[1]

    def p_sensor(self, p):
        """sensor : SENSOR definitions_list END SENSOR"""
        p[0] = p[2]

        if 'ZEROES' not in p[0] and 'POLES' not in p[0]:
            return
        p[0]['A0'] = StageParser.calc_filter_A0(p[0]['ZEROES'],
                                                p[0]['POLES'],
                                                p[0]['TRANSFER_NORMALIZATION_FREQUENCY'],
                                                p[0]['TRANSFER_FUNCTION_TYPE'],
                                                None)
        # Now we verify that transfer normalization frequency is valid
        # StageParser.verify_tnf(p[0]['POLES'],
        #                       p[0]['TRANSFER_NORMALIZATION_FREQUENCY'],
        #                       p[0]['TRANSFER_FUNCTION_TYPE'])

    def p_analogic_filters_cascade(self, p):
        """analogic_filters_cascade : analogic_filter
                                   | analogic_filters_cascade BEGIN analogic_filter"""
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = p[1] + p[3]

    def p_analogic_filter(self, p):
        """analogic_filter : ANA_FILTER definitions_list END ANA_FILTER"""
        p[0] = [p[2]]
        if(('ZEROES' not in p[2] or len(p[2]['ZEROES']) == 0) and
           ('POLES' not in p[2] or len(p[2]['POLES']) == 0)):
            return
        p[2]['A0'] = StageParser.calc_filter_A0(p[2]['ZEROES'],
                                                p[2]['POLES'],
                                                p[2]['TRANSFER_NORMALIZATION_FREQUENCY'],
                                                p[2]['TRANSFER_FUNCTION_TYPE'],
                                                None)

    def p_digitizer(self, p):
        """digitizer : DIGITIZER definitions_list END DIGITIZER"""
        p[0] = p[2]

    def p_digital_filters_cascade(self, p):
        """digital_filters_cascade : digital_filter
                                  | digital_filter  stage_filter_list"""
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = p[1] + p[2]
            for current_stage in p[0][1:]:
                if 'RESPONSE_NAME' not in current_stage:
                    current_stage['RESPONSE_NAME'] = p[1][0]['RESPONSE_NAME']

    def p_digital_filter(self, p):
        """digital_filter :  DIGITAL_FILTER definitions_list END DIGITAL_FILTER"""
        if "RESPONSE_TYPE" not in p[2] or p[2]["RESPONSE_TYPE"] != "FAKE":
            if "COEFFICIENTS" in p[2]:
                if p[2]['TRANSFER_FUNCTION_TYPE'] == "FIR_SYM_2":  # number of coefs is even
                    p[2]['COEFFICIENTS'] = p[2]['COEFFICIENTS'][0:len(p[2]['COEFFICIENTS']) / 2]
                elif p[2]['TRANSFER_FUNCTION_TYPE'] == "FIR_SYM_1":  # number of coefs is odd
                    p[2]['COEFFICIENTS'] = p[2]['COEFFICIENTS'][0:(len(p[2]['COEFFICIENTS']) / 2) + 1]
            if p[2]['TRANSFER_FUNCTION_TYPE'] == 'IIR_PZ':
                p[2]['INPUT_UNIT'] = 'COUNTS'
                p[2]['OUTPUT_UNIT'] = 'COUNTS'
                p[2]['A0'] = StageParser.calc_filter_A0(p[2]['ZEROES'], p[2]['POLES'],
                                                        p[2]['TRANSFER_NORMALIZATION_FREQUENCY'],
                                                        p[2]['TRANSFER_FUNCTION_TYPE'],
                                                        p[2]['INPUT_SAMPLING_INTERVAL'])

        p[0] = [p[2]]

    def p_stage_filter_list(self, p):
        """stage_filter_list : stage_filter
                             | stage_filter_list stage_filter"""
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = p[1] + p[2]

    def p_stage_filter(self, p):
        """stage_filter : BEGIN STAGE definitions_list END STAGE"""
        if "RESPONSE_TYPE" not in p[3] or p[3]["RESPONSE_TYPE"] != "FAKE":
            if "COEFFICIENTS" in p[3]:
                if p[3]['TRANSFER_FUNCTION_TYPE'] == "FIR_SYM_2":  # number of coefs is even
                    p[3]['COEFFICIENTS'] = p[3]['COEFFICIENTS'][0:len(p[3]['COEFFICIENTS']) / 2]
                elif p[3]['TRANSFER_FUNCTION_TYPE'] == "FIR_SYM_1":  # number of coefs is odd
                    p[3]['COEFFICIENTS'] = p[3]['COEFFICIENTS'][0:(len(p[3]['COEFFICIENTS']) / 2) + 1]
            if p[3]['TRANSFER_FUNCTION_TYPE'] == 'IIR_PZ':
                p[3]['INPUT_UNIT'] = 'COUNTS'
                p[3]['OUTPUT_UNIT'] = 'COUNTS'
                p[3]['A0'] = StageParser.calc_filter_A0(p[3]['ZEROES'], p[3]['POLES'],
                                                        p[3]['TRANSFER_NORMALIZATION_FREQUENCY'],
                                                        p[3]['TRANSFER_FUNCTION_TYPE'],
                                                        p[3]['INPUT_SAMPLING_INTERVAL'])
        p[0] = [p[3]]

    def p_definitions_list(self, p):
        """definitions_list : definition
                            | definitions_list definition"""
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = copy.copy(p[1])
            p[0].update(p[2])

    def p_definitions_string(self, p):
        """definition : WORD COLON STRING"""
        p[0] = {p[1].upper(): p[3]}

    def p_definitions_word(self, p):
        """definition : WORD COLON WORD"""
        p[0] = {p[1].upper(): p[3]}

    def p_definitions_integer(self, p):
        """definition : WORD COLON INTEGER"""
        p[0] = {p[1].upper(): int(p[3])}

    def p_definitions_double(self, p):
        """definition : WORD COLON DOUBLE"""
        p[0] = {p[1].upper(): float(p[3])}

    def p_definition(self, p):
        """definition : INCLUDE STRING
                      | NUMBER_OF_POLES COLON INTEGER
                      | NUMBER_OF_POLES COLON INTEGER pz_data
                      | NUMBER_OF_ZEROES COLON INTEGER
                      | NUMBER_OF_ZEROES COLON INTEGER pz_data
                      | NUMBER_OF_COEFFICIENTS COLON INTEGER
                      | NUMBER_OF_COEFFICIENTS COLON INTEGER coefs_list
                      | NUMBER_OF_NUMERATORS COLON INTEGER coefs_list
                      | NUMBER_OF_DENOMINATORS COLON INTEGER coefs_list"""
        if len(p) == 3:
            p[0] = self.load_pz_file(p[2])
        elif len(p) == 4:
            p[0] = {p[1].split("_")[2].upper(): []}  # List size is zero
            if p[3] != 0:
                raise StageParserException("There is no coefficient/poles/zeros list and number is not 0 (%s)" %
                                           p[3])
        elif len(p) == 5:
            if p[1].split("_")[2].upper() in ("ZEROES", "POLES"):
                index = 0
                tmp_list = []
                for _ in p[4]:
                    if index % 2 == 0:
                        tmp_list.append({'real': p[4][index],
                                         'imaginary': p[4][index + 1],
                                         'index': index / 2})
                    index += 1
                p[0] = {p[1].split("_")[2].upper(): tmp_list}
                if len(tmp_list) != p[3]:
                    raise StageParserException("Expecting %s poles/zeros and get %s" %
                                               p[3], len(tmp_list))

            else:
                p[0] = {p[1].split("_")[2].upper(): p[4]}
                if len(p[4]) != p[3]:
                    raise StageParserException("Expecting %s coefficients and get %s" %
                                               p[3], len(p[4]))

    def p_pz_data(self, p):
        """pz_data : DOUBLE DOUBLE
                   | pz_data DOUBLE DOUBLE"""
        if len(p) == 3:
            p[0] = [p[1], p[2]]
        else:
            p[0] = p[1] + [p[2], p[3]]

    def p_coefs_list(self, p):
        """coefs_list : DOUBLE
                      | coefs_list DOUBLE"""
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

    def p_error(self, p):
        if p is not None:
            raise StageParserException("stage parser error when reading %s (%s) at line %d" %
                                       (p.type, p.value, self.lexer.lineno))
        else:
            raise StageParserException("stage parser error at line %d" %
                                       self.lexer.lineno)

    def parse_str(self, inputs, filename=None):
        self.set_filename(filename)
        result = self.__parser.parse(inputs)
        return result

    def load_pz_file(self, filename):
        """
        Load data stored in a given pz include file.

        filename  -- The pz filename relative to pz root directory

        return    -- Dictionnary with file content
        """
        result = {}
        if self.__verbose:
            print("Loading %s" % filename)
        try:
            pz_file = open("%s/%s" % (self.__pz_root, filename))
        except IOError as e:
            raise e
        lineno = 1
        read_numbers = False
        current_list = None
        current_list_entry = None
        coefficient_list_type = False
        for current_line in pz_file.readlines():
            current_line = current_line.strip().upper()
            if len(current_line) == 0 or current_line[0] == '#':  # Skip comment line
                lineno += 1
                continue
            # Suppress comment at the end of the line
            current_line = current_line.split('#')[0]

            if not read_numbers:
                # Reading definitions
                if ':' not in current_line:
                    raise StageParserException("Syntax error at line %d (%s)" %
                                               (lineno, current_line))
                current_line_split = current_line.split(':')
                result[current_line_split[0].strip().upper()] = current_line_split[1].strip()
                list_size = None
                if current_line_split[0].strip().upper() == "NUMBER_OF_ZEROES":
                    current_list = []
                    current_list_entry = "ZEROES"
                    result["NUMBER_OF_ZEROES"] = int(result["NUMBER_OF_ZEROES"])
                    list_size = result["NUMBER_OF_ZEROES"] * 2
                    read_numbers = True
                    coefficient_list_type = False
                elif current_line_split[0].strip().upper() == "NUMBER_OF_POLES":
                    current_list = []
                    current_list_entry = "POLES"
                    result["NUMBER_OF_POLES"] = int(result["NUMBER_OF_POLES"])
                    list_size = result["NUMBER_OF_POLES"] * 2
                    read_numbers = True
                    coefficient_list_type = False
                elif current_line_split[0].strip().upper() == "NUMBER_OF_NUMERATORS":
                    current_list = []
                    current_list_entry = "NUMERATORS"
                    result["NUMBER_OF_NUMERATORS"] = int(result["NUMBER_OF_NUMERATORS"])
                    list_size = result["NUMBER_OF_NUMERATORS"]
                    read_numbers = True
                    coefficient_list_type = True
                elif current_line_split[0].strip().upper() == "NUMBER_OF_DENOMINATORS":
                    current_list = []
                    current_list_entry = "DENOMINATORS"
                    result["NUMBER_OF_DENOMINATORS"] = int(result["NUMBER_OF_DENOMINATORS"])
                    list_size = result["NUMBER_OF_DENOMINATORS"]
                    read_numbers = True
                    coefficient_list_type = True
                elif current_line_split[0].strip().upper() == "NUMBER_OF_COEFFICIENTS":
                    current_list = []
                    current_list_entry = "COEFFICIENTS"
                    result["NUMBER_OF_COEFFICIENTS"] = int(result["NUMBER_OF_COEFFICIENTS"])
                    list_size = result["NUMBER_OF_COEFFICIENTS"]
                    read_numbers = True
                    coefficient_list_type = True

                if list_size == 0:
                    coefficient_list_type = False
                    read_numbers = False
                    result[current_list_entry] = []

            else:
                # Reading list of numbers
                current_line_split = current_line.split()
                current_line_split = map(lambda x: float(x),
                                         current_line_split)
                current_list += current_line_split
                if len(current_list) == list_size:
                    read_numbers = False
                    if not coefficient_list_type:
                        # Transform float list to complex list
                        # by grouping float by couple
                        index = 0
                        tmp_list = []
                        for not_used in current_list:
                            if index % 2 == 0:
                                tmp_list.append({'real': current_list[index],
                                                 'imaginary': current_list[index + 1],
                                                 'index': index / 2})
                            index += 1
                        current_list = tmp_list
                    result[current_list_entry] = current_list
                    current_list = None
                    current_list_entry = None
                elif len(current_list) > list_size:
                    raise StageParserException("Line %d : While reading %s find too much value" %
                                               (lineno, current_list_entry.lower()))

            lineno += 1

        # Verify that we read enough values
        if current_list is not None:
            raise StageParserException("Expecting %s values and get only %s values" %
                                       (list_size, len(current_list)))
        return result

    @staticmethod
    def analog_trans(zeros, poles, h0, normalization_freq, filter_type):
        mod = 1.0
        pha = 0.0

        if filter_type == 'LAPLACE':
            normalization_freq = math.pi * 2 * normalization_freq

        for current_zero in zeros:
            real = -current_zero['real']
            imaginary = -current_zero['imaginary']
            if real == 0.0 and imaginary == 0.0:
                mod *= normalization_freq
                pha += math.pi / 2.0
            else:
                mod *= math.sqrt((normalization_freq + imaginary) *
                                 (normalization_freq + imaginary) + real * real)
                pha += math.atan2(normalization_freq + imaginary, real)

        for current_pole in poles:
            real = -current_pole['real']
            imaginary = -current_pole['imaginary']
            if real == 0.0 and imaginary == 0.0:
                mod /= normalization_freq
                pha -= math.pi / 2.0
            else:
                mod /= math.sqrt((normalization_freq + imaginary) *
                                 (normalization_freq + imaginary) + real * real)
                pha -= math.atan2(normalization_freq + imaginary, real)

        return (mod * math.cos(pha) * h0, mod * math.sin(pha) * h0)

    @staticmethod
    def calc_analog_A0(zeros, poles, normalization_freq, filter_type):
        result = StageParser.analog_trans(zeros, poles, 1.0,
                                          normalization_freq, filter_type)
        return 1.0 / math.sqrt(result[0] * result[0] + result[1] * result[1])

    @staticmethod
    def calc_iir_A0(zeros, poles, normalization_freq, filter_type, input_sampling_interval):
        zn = cmath.exp(complex(0, 2 * math.pi * normalization_freq * input_sampling_interval))
        numerator = complex(1, 0)
        denominator = complex(1, 0)

        for current in zeros:
            numerator *= (zn - complex(current['real'], current['imaginary']))

        for current in poles:
            denominator *= (zn - complex(current['real'], current['imaginary']))

        hp = denominator / numerator

        return math.sqrt(hp.real * hp.real + hp.imag * hp.imag)

    @staticmethod
    def calc_filter_A0(zeros, poles, normalization_freq, filter_type, input_sampling_interval):
        if filter_type == 'IIR_PZ':
            return StageParser.calc_iir_A0(zeros, poles, normalization_freq, filter_type, input_sampling_interval)
        else:
            return StageParser.calc_analog_A0(zeros, poles, normalization_freq, filter_type)

    @staticmethod
    def verify_tnf(poles, normalization_freq, transfer_function_type):
        """
        Verify transfer normalization frequency validity according to
        given poles.

        poles                  -- The poles array
        normalization_freq     -- The transfer normalization frequency
        transfer_function_type -- The transfer function type
                                  (ANALOG or LAPLACE)
        """
        if len(poles) == 0:
            return
        freq_min = sys.float_info.max
        freq_max = sys.float_info.min
        if transfer_function_type == 'LAPLACE':
            for current in poles:
                current_freq = math.sqrt(current['real'] * current['real'] + current['imaginary'] * current['imaginary']) / (math.pi * 2)
                if current_freq < freq_min:
                    freq_min = current_freq
                if current_freq > freq_max:
                    freq_max = current_freq
        elif transfer_function_type == 'ANALOG':
            for current in poles:
                current_freq = math.sqrt(current['real'] * current['real'] + current['imaginary'] * current['imaginary'])
                if current_freq < freq_min:
                    freq_min = current_freq
                if current_freq > freq_max:
                    freq_max = current_freq
        else:
            raise ValueError("Transfer function type %s is unknown" %
                             transfer_function_type)

        # Now verify that given normalization frequency is netween min and max
        if normalization_freq < (freq_min - freq_min * 0.05) or normalization_freq > (freq_max + freq_max * 0.05):
            raise ValueError("normalization frequency is %s and computed limit is %s-%s" %
                             (normalization_freq, freq_min, freq_max))


def main():

    parser = StageParser("/home/leon/tmp/PZ")
    result = parser.parse_str(sample_stage)
    print(json.dumps(result))


if __name__ == "__main__":
    main()
