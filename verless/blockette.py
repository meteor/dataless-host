#!/usr/bin/python
# coding=utf-8
# -*- coding : utf-8 -*-

import re
from abc import ABCMeta


class Blockette(object):
    """
    classdocs
    """
    ALL = r'\S\s'
    UPPER_CASE = r'A-Z'
    LOWER_CASE_UTF8 = r'éèêôâiîàçù'
    LOWER_CASE = r'a-z'
    DIGIT = r'0-9'
    PUNCTUATION = r',\.\'_/*\(\)°:-'
    SPACES = r'\s'
    UNDERLINE = r'_'

    __strict_mode = False

    __verbose_mode = False

    __metaclass__ = ABCMeta

    def __init__(self, blk_type, content):
        """
        Initialize this blockette.

        blk_type -- The blockette type
        content  -- The content of this blockette
        """
        self.__type = blk_type
        self.__content = content

    @staticmethod
    def set_strict_mode(flag):
        """
        Strict mode setter.

        flag -- The new strict mode
        """
        Blockette.__strict_mode = flag

    @staticmethod
    def __get_characters_from_mask(mask):
        """
        Give the regular expression corresponding to a given
        SEED characters mask

        mask   -- The SEED characters mask

        return -- Regular expression corresponding to given mask
        """
        valid_char = ""
        current_mask = mask
        while len(current_mask) != 0:
            if current_mask[0] == 'U':
                valid_char = Blockette.UPPER_CASE + valid_char
            elif current_mask[0] == 'L':
                if Blockette.__strict_mode:
                    valid_char = Blockette.LOWER_CASE + valid_char
                else:
                    valid_char = Blockette.LOWER_CASE_UTF8 + valid_char
            elif current_mask[0] == 'N':
                valid_char = Blockette.DIGIT + valid_char
            elif current_mask[0] == 'P':
                valid_char += Blockette.PUNCTUATION
            elif current_mask[0] == 'S':
                valid_char = Blockette.SPACES + valid_char
            elif current_mask[0] == '_':
                valid_char = Blockette.UNDERLINE + valid_char
            else:
                raise ValueError("Internal Error : %s is not a valid mask characters for string" %
                                 current_mask[0])
            current_mask = current_mask[1:]
        return valid_char

    def _get_content(self):
        """
        Give the content of this blockette.

        return -- This blockette content
        """
        return self.__content

    def type(self):
        """
        Give the type of this blockette.

        return -- The type of this blockette as integer.
        """
        return self.__type

    @staticmethod
    def _get_integer(input_str, size, signed=False):
        """
        Give integer corresponding to a given string.

        input_str -- The string to convert
        size      -- The number of character to convert
        signed    -- Say if the integer can be signed

        return    -- A couple (<string>, <integer>) corresponding to remaining
                     string and integer value.
        """
        str_pattern = r"[ ]*([0-9]+)"
        if signed:
            str_pattern = r"[ ]*([+-])[ ]*" + str_pattern
        pattern = re.match(str_pattern, input_str[:size])
        if pattern is None:
            raise ValueError("<%s> is not a valid integer" % input_str[:size])
        value = pattern.groups()[0]
        if len(pattern.groups()) == 2:
            value = pattern.groups()[1] + value
        return (input_str[size:], int(value))

    @staticmethod
    def _get_decimal(input_str, mask):
        """
        Convert a string to decimal (integer or float) according to a given
        format definition.

        input_str -- The string to convert
        mask      -- The format definition

        return    -- The couple (<string>, <decimal>) corresponding
                     to remaining string and decimal value.

        mask have the following form "[+-]?[#]+(.[#]+)?",
        each # corresponding to one digit.

        The full explanation can be found in
        SEED reference manual 2.4 May 2010 page 31.
        """

        signed = False
        if mask[0] in ('-', '+'):
            signed = True
        split_mask = mask[1:].split('.')
        fractional_part = ""
        if len(split_mask) == 2:
            fractional_part = split_mask[1]

        # In non strict mode assimilate blank to 0
        if(not Blockette.__strict_mode and
           len(input_str[:len(mask)].lstrip()) == 0):
            if len(fractional_part) == 0:
                return (input_str[len(mask):], int(0))
            else:
                return (input_str[len(mask):], float(0))

        str_pattern = r"[ ]*([0-9]+)"
        if len(fractional_part) != 0:
            str_pattern = r"((%s)?\.[0-9]+)" % str_pattern

        if signed:
            str_pattern = r"[ ]*([+-])?[ ]*" + str_pattern

        pattern = re.match(str_pattern, input_str[:len(mask)])
        if pattern is None and Blockette.__strict_mode:
            raise ValueError("<%s> is not a valid decimal (%s)" %
                             (input_str[:len(mask)], str_pattern))
        elif pattern is None:
            if Blockette.__verbose_mode:
                print("Warning <%s> is not a valid decimal (%s)" %
                      (input_str[:len(mask)], mask))
                print("Strict mode not enable try to continue")
            if len(fractional_part) == 0:
                return (input_str[len(mask):], int(input_str[:len(mask)]))
            else:
                return (input_str[len(mask):], float(input_str[:len(mask)]))

        if signed:
            value = pattern.groups()[1]
            if pattern.groups()[0] is not None:
                value = pattern.groups()[0] + value
        else:
            value = pattern.groups()[0]

        if len(fractional_part) == 0:
            return (input_str[len(mask):], int(value))
        else:
            return (input_str[len(mask):], float(value))

    @staticmethod
    def _get_float(input_str, mask):
        """
        Convert a string to decimal (integer or float) according to a given
        format definition.

        input_str -- The string to convert
        mask      -- The format definition

        return    -- The couple (<string>, <decimal>) corresponding
                     to remaining string and decimal value.

        mask have the following form "[+-]?[#]+(.[#]+)?(E[0-9]+)?",
        each # corresponding to one digit.

        The full explanation can be found in
        SEED reference manual 2.4 May 2010 page 31.
        """
        signed = False
        if mask[0] in ('-', '+'):
            signed = True

        split_mask = mask[1:].split('.')
        fractional_part = ""
        if len(split_mask) == 2:
            fractional_part = split_mask[1]

        exp_part = ""
        if 'E' in fractional_part:
            fractional_split = fractional_part.split('E')
            fractional_part = fractional_split[0]
            exp_part = fractional_split[1]

        # In non strict mode assimilate blank to 0
        if(not Blockette.__strict_mode and
           len(input_str[:len(mask)].lstrip()) == 0):
            if Blockette.__verbose_mode:
                print("Warning convert spaces to float equal to zero (%s,%s)" %
                      (input_str[:100], mask))
            return (input_str[len(mask):], float(0))

        str_pattern = r"[ ]*([0-9]+)"
        if len(fractional_part) != 0:
            str_pattern = r"((%s)?\.[0-9]+)" % str_pattern

        if len(exp_part) != 0:
            str_pattern = "%s[eE][ ]*([+-]?[0-9]+)" % str_pattern

        if signed:
            str_pattern = r"[ ]*([+-])?[ ]*" + str_pattern

        pattern = re.match(str_pattern, input_str[:len(mask)])
        if pattern is None and Blockette.__strict_mode:
            raise ValueError("<%s> is not a valid float (%s)" %
                             (input_str[:len(mask)], mask))
        elif pattern is None:
            if Blockette.__verbose_mode:
                print("Warning <%s> is not a valid float (%s)" %
                      (input_str[:len(mask)], mask))
                print("Strict mode not enable try to continue")
            return (input_str[len(mask):], float(input_str[:len(mask)]))

        if signed:
            value = pattern.groups()[1]
            if pattern.groups()[0] is not None:
                value = pattern.groups()[0] + value
            if len(exp_part) != 0:
                value = "%sE%s" % (value, pattern.groups()[4])
        else:
            value = pattern.groups()[0]
            if len(exp_part) != 0:
                value = "%sE%s" % (value, pattern.groups()[3])

        return (input_str[len(mask):], float(value))

    @staticmethod
    def _get_time(input_str):
        """
        Extract date definition from a given string.

        input_str -- The string to parse.

        return    -- A couple (<string>, <string>) corresponding to remaining
                     string and date string.
        """
        pattern = re.match("(^[0-9]{4},[0-9]{3}(,[0-9]{2}(:[0-9]{2}(:[0-9]{2}(.[0-9]{4})?)?)?)?)?~",
                           input_str)
        if pattern is None:
            raise ValueError("<%s> is not a valid date" % input_str[0:22])
        result = pattern.groups()
        if result[0] is None:
            return (input_str[1:], "")
        else:
            return (input_str[len(result[0]) + 1:], result[0])

    @staticmethod
    def _get_string(input_str, min_size=0, max_size=None, valid_char_mask=None):
        """
        Extract a string definition from a given string.

        input_str       -- The input string
        min_size        -- The minimum size of extracted string
        max_size        -- The maximum size of extracted string
        valid_char_mask -- The valid characters definition

        return          -- A couple (<string>, <string>) corresponding to
                           remaining string and parsed string.
        """
        # Get Valid characters from mask
        if valid_char_mask is None or not Blockette.__strict_mode:
            valid_char = r'^~'
        else:
            valid_char = Blockette.__get_characters_from_mask(valid_char_mask)

        # Variable of fixed length string
        if min_size == max_size:
            # Fixed lenght string
            pattern = re.match(r"^([%s]{,%d})" % (valid_char, min_size),
                               input_str.rstrip())
            if pattern is None:
                raise ValueError("<%s> is not a valid fixed length string" %
                                 input_str)
            result = pattern.groups()
            return (input_str[min_size:], result[0])
        elif max_size is None:
            # infinite variable length string
            pattern = re.match(r"^([%s]{%d,})~" % (valid_char, min_size),
                               input_str)
            if pattern is None:
                raise ValueError("<%s> is not a valid variable string length" %
                                 input_str)
        else:
            # finite variable length string
            pattern = re.match(r"^([%s]{%d,%d})~" % (valid_char,
                                                     min_size,
                                                     max_size),
                               input_str,
                               re.U)
            if pattern is None:
                raise ValueError("<%s...> is not a valid variable string length (%s, %d-%d)" %
                                 (input_str[:100], valid_char,
                                  min_size, max_size))

        result = pattern.groups()
        if result[0] is None:  # empty string
            return (input_str[1:], "")
        else:
            return (input_str[len(result[0]) + 1:], result[0])

    @staticmethod
    def blockette_constructor(input_str):
        """
        Extract the next blockette from a given string.

        input_str -- The string to parse

        return    -- A couple (<string>, <Blockette>) corresponding to
                     remaining string and parsed blockette.
        """
        blk_type = int(input_str[0:3])
        lenght = int(input_str[3:7])

        if blk_type == 10:
            return (input_str[lenght:].lstrip(),
                    Blockette_010(blk_type, input_str[7:lenght]))
        elif blk_type == 11:
            return (input_str[lenght:].lstrip(),
                    Blockette_011(blk_type, input_str[7:lenght]))
        elif blk_type == 12:
            return (input_str[lenght:].lstrip(),
                    Blockette_012(blk_type, input_str[7:lenght]))
        elif blk_type == 30:
            return (input_str[lenght:].lstrip(),
                    Blockette_030(blk_type, input_str[7:lenght]))
        elif blk_type == 31:
            return (input_str[lenght:].lstrip(),
                    Blockette_031(blk_type, input_str[7:lenght]))
        elif blk_type == 33:
            return (input_str[lenght:].lstrip(),
                    Blockette_033(blk_type, input_str[7:lenght].decode('utf8')))
        elif blk_type == 34:
            return (input_str[lenght:].lstrip(),
                    Blockette_034(blk_type, input_str[7:lenght].decode('utf8')))
        elif blk_type == 50:
            return (input_str[lenght:].lstrip(),
                    Blockette_050(blk_type, input_str[7:lenght].decode('utf8')))
        elif blk_type == 51:
            return (input_str[lenght:].lstrip(),
                    Blockette_051(blk_type, input_str[7:lenght].decode('utf8')))
        elif blk_type == 52:
            return (input_str[lenght:].lstrip(),
                    Blockette_052(blk_type, input_str[7:lenght]))
        elif blk_type == 53:
            return (input_str[lenght:].lstrip(),
                    Blockette_053(blk_type, input_str[7:lenght]))
        elif blk_type == 54:
            return (input_str[lenght:].lstrip(),
                    Blockette_054(blk_type, input_str[7:lenght]))
        elif blk_type == 57:
            return (input_str[lenght:].lstrip(),
                    Blockette_057(blk_type, input_str[7:lenght]))
        elif blk_type == 58:
            return (input_str[lenght:].lstrip(),
                    Blockette_058(blk_type, input_str[7:lenght]))
        elif blk_type == 59:
            return (input_str[lenght:].lstrip(),
                    Blockette_059(blk_type, input_str[7:lenght]))
        elif blk_type == 61:
            return (input_str[lenght:].lstrip(),
                    Blockette_061(blk_type, input_str[7:lenght]))
        elif blk_type == 62:
            return (input_str[lenght:].lstrip(),
                    Blockette_062(blk_type, input_str[7:lenght]))
        else:
            raise ValueError("Unknown blockette type (%d)" % blk_type)


class Blockette_010(Blockette):

    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        self.__version_of_format = content[0:4]
        (remaining_content, self.__logical_record_lenght) = Blockette._get_integer(content[4:], 2)
        (remaining_content, self.__beginning_time) = Blockette._get_time(remaining_content)
        (remaining_content, self.__end_time) = Blockette._get_time(remaining_content)
        (remaining_content, self.__volume_time) = Blockette._get_time(remaining_content)
#        (remaining_content, self.__originating_organisation) = Blockette._get_string(remaining_content, 1, 80)
#        (remaining_content, self.__label) = Blockette._get_string(remaining_content, 1, 80)
        (remaining_content, self.__originating_organisation) = Blockette._get_string(remaining_content, 0, 80)
        (remaining_content, self.__label) = Blockette._get_string(remaining_content, 0, 80)
        if len(remaining_content) != 0:
            raise ValueError("Content of blk 010 is not valid find remaining string <%s>" % remaining_content)

    def get_volumes_size(self):
        return 2 ** self.__logical_record_lenght

    def __str__(self, *args, **kwargs):
        return "Blk 010 : %d %s %s %s %s" % (self.type(), self.__beginning_time, self.__end_time, self.__originating_organisation, self.__label)


class Blockette_011(Blockette):

    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content,
         self.__number_of_station) = Blockette._get_integer(content, 3)
        self.__stations_index = []
        for current_index in range(self.__number_of_station):  # @UnusedVariable
            if len(remaining_content) < 11:
                raise ValueError("Blk_011 : %s is not a valid station header index" % remaining_content)
            (remaining_content,
             station_id) = Blockette._get_string(remaining_content,
                                                 5, 5, "UN")
            (remaining_content,
             sequence_number) = Blockette._get_integer(remaining_content, 6)
            self.__stations_index.append((station_id, sequence_number))

        if len(remaining_content) != 0:
            raise ValueError("Content of blk 011 is not valid find remaining string <%s>" %
                             remaining_content)

    def __str__(self, *args, **kwargs):
        result = "Blk 011 : %d" % self.__number_of_station
        for (station, index) in self.__stations_index:  # @UnusedVariable
            result = "%s %s" % (result, station)
        return result

    def get_nb_stations(self):
        """
        Return the number of station defined in this seed file

        return  -- The number of stations definitions
        """
        return self.__number_of_station

    def get_station_by_index(self, station_index):
        """
        Return the informations of the station with a given index.

        station_index -- The index of the station

        return        -- (station_id, sequence_number)
                         as tuple (string, integer)
        """
        return self.__stations_index[station_index]

    def get_station_by_name(self, station_name):
        """
        Give the list of station with a given name.

        station_name -- The name of the station

        return       -- The list of stations index with the given name
        """
        result = []
        for current in self.__stations_index:
            if current[0] == station_name:
                result.append(current[1])
        return result


class Blockette_012(Blockette):

    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        self.__spans = []
        (remaining_content,
         self.__number_of_spans) = Blockette._get_integer(content, 4)
        for span in range(self.__number_of_spans):  # @UnusedVariable
            (remaining_content,
             span_begin) = Blockette._get_time(remaining_content)
            (remaining_content,
             span_end) = Blockette._get_time(remaining_content)
            (remaining_content,
             span_sequence_number) = Blockette._get_integer(remaining_content,
                                                            6)
            self.__spans.append((span_begin, span_end, span_sequence_number))
        if len(remaining_content) != 0:
            raise ValueError("Content of blk 012 is not valid find remaining string <%s>" %
                             remaining_content)

    def __str__(self, *args, **kwargs):
        return "Blk 012 : %d" % self.__number_of_spans


class Blockette_030(Blockette):

    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content,
         self.__descriptive_name) = Blockette._get_string(content,
                                                          1, 50, "UNLPS")
        (remaining_content,
         self.__lookup_code) = Blockette._get_integer(remaining_content, 4)
        (remaining_content,
         self.__family_type) = Blockette._get_integer(remaining_content, 3)
        (remaining_content,
         self.__nb_decoder_keys) = Blockette._get_integer(remaining_content, 2)
        self.__decoder_keys = []
        for key in range(self.__nb_decoder_keys):  # @UnusedVariable
            (remaining_content,
             current_key) = Blockette._get_string(remaining_content,
                                                  min_size=0,
                                                  valid_char_mask="UNLPS")
            self.__decoder_keys.append(current_key)
        if len(remaining_content) != 0:
            raise ValueError("Content of blk 030 is not valid find remaining string <%s>" %
                             remaining_content)

    def lookup_code(self):
        return self.__lookup_code

    def __str__(self, *args, **kwargs):
        return "Blk 030 : %s (%d, %d)" % (self.__descriptive_name,
                                          self.__lookup_code,
                                          self.__nb_decoder_keys)


class Blockette_031(Blockette):
    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content,
         self.__lookup_code) = Blockette._get_integer(content, 4)
        (remaining_content,
         self.__class_code) = Blockette._get_string(remaining_content, 1, 1,
                                                    valid_char_mask="U")
        (remaining_content,
         self.__description) = Blockette._get_string(remaining_content, 1, 70,
                                                     valid_char_mask="UNLPS")
        (remaining_content,
         self.__units) = Blockette._get_integer(remaining_content, 3)

        if len(remaining_content) != 0:
            raise ValueError("Content of blk 031 is not valid find remaining string <%s>" %
                             remaining_content)

    def lookup_code(self):
        return self.__lookup_code

    def __str__(self, *args, **kwargs):
        return ("Blk 031 : %s %s: %s" % (self.__lookup_code,
                                         self.__class_code,
                                         self.__description)).encode('utf8')


class Blockette_033(Blockette):

    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content,
         self.__lookup_code) = Blockette._get_integer(content, 3)
        (remaining_content,
         self.__abbreviation_description) = Blockette._get_string(remaining_content,
                                                                  1, 50,
                                                                  valid_char_mask="UNLPS")
        if len(remaining_content) != 0:
            raise ValueError("Content of blk 033 is not valid find remaining string <%s>" %
                             remaining_content)

    def lookup_code(self):
        return self.__lookup_code

    def __str__(self, *args, **kwargs):
        return ("Abbreviation (033) : %3d %50s" %
                (self.__lookup_code,
                 self.__abbreviation_description)).encode('utf8')


class Blockette_034(Blockette):

    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content,
         self.__lookup_code) = Blockette._get_integer(content, 3)
        (remaining_content,
         self.__unit_name) = Blockette._get_string(remaining_content, 1, 20,
                                                   "UNP")
        (remaining_content,
         self.__unit_description) = Blockette._get_string(remaining_content,
                                                          0, 50, "UNLPS")
        if len(remaining_content) != 0:
            raise ValueError("Content of blk 034 is not valid find remaining string <%s>" %
                             remaining_content)

    def lookup_code(self):
        return self.__lookup_code

    def __str__(self, *args, **kwargs):
        return ("UNIT Abbreviation (034): %3d %20s %50s" %
                (self.__lookup_code, self.__unit_name,
                 self.__unit_description)).encode('utf8')


class Blockette_050(Blockette):

    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content,
         self.__station_code) = Blockette._get_string(content, 5, 5, "UN")
        (remaining_content,
         self.__latitude) = Blockette._get_decimal(remaining_content,
                                                   "-##.######")
        (remaining_content,
         self.__longitude) = Blockette._get_decimal(remaining_content,
                                                    "-###.######")
        (remaining_content,
         self.__elevation) = Blockette._get_decimal(remaining_content,
                                                    "-####.#")
        (remaining_content,
         self.__nb_channels) = Blockette._get_decimal(remaining_content,
                                                      "####")
        (remaining_content,
         self.__nb_comments) = Blockette._get_decimal(remaining_content, "###")
        (remaining_content,
         self.__site_name) = Blockette._get_string(remaining_content, 1, 60,
                                                   "UNLPS")
        (remaining_content,
         self.__owner_id) = Blockette._get_decimal(remaining_content, "###")

        (remaining_content,
         self.__word_order_32) = Blockette._get_decimal(remaining_content,
                                                        "####")
        (remaining_content,
         self.__word_order_16) = Blockette._get_decimal(remaining_content,
                                                        "##")
        (remaining_content,
         self.__effective_start) = Blockette._get_time(remaining_content)
        (remaining_content,
         self.__effective_end) = Blockette._get_time(remaining_content)
        (remaining_content,
         self.__udate_flag) = Blockette._get_string(remaining_content, 1, 1,
                                                    "UN")
        (remaining_content,
         self.__network_code) = Blockette._get_string(remaining_content, 2, 2,
                                                      "ULN")
        if len(remaining_content) != 0:
            raise ValueError("Content of blk 050 is not valid find remaining string <%s>" %
                             remaining_content)

    def get_station_code(self):
        """
        Give the station code of the station.
        """
        return self.__station_code.strip()

    def get_position(self):
        """
        Give the tuple (latitude, longitude, elevation) of the station defined
        in this blockette.
        """
        return (self.__latitude, self.__longitude, self.__elevation)

    def get_nb_channels(self):
        """
        Give the number of channels of the station defined
        in this blockette.
        """
        return self.__nb_channels

    def get_owner(self):
        """
        Give the owner lookup code of this station (field 10)
        """
        return self.__owner_id

    def get_dates(self):
        """
        Give the validity period of informations present in this blockette.
        """
        return (self.__effective_start, self.__effective_end)

    def get_network(self):
        """
        Give the network code of this station.
        """
        return self.__network_code.strip()

    def __str__(self, *args, **kwargs):
        return ("%s %10f %11f %12f %12d %s %22s %s" %
                (self.__station_code, self.__latitude, self.__longitude,
                 self.__elevation, self.__nb_channels, self.__effective_start,
                 self.__effective_end, self.__network_code)).encode('utf8')


class Blockette_051(Blockette):
    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content,
         self.__effective_start) = Blockette._get_time(content)
        (remaining_content,
         self.__effective_end) = Blockette._get_time(remaining_content)
        (remaining_content,
         self.__code_key) = Blockette._get_integer(remaining_content, 4)
        (remaining_content,
         self.__level) = Blockette._get_integer(remaining_content, 6)
        if len(remaining_content) != 0:
            raise ValueError("Content of blk 051 is not valid find remaining string <%s>" % remaining_content)

    def __str__(self, *args, **kwargs):
        return ("Blk 051 Station comment (%s-%s): %s" %
                (self.__effective_start, self.__effective_end,
                 "comment")).encode('utf8')

    def lookup_code(self):
        """
        Give the lookup key of comment
        """
        return self.__code_key


class Blockette_052(Blockette):

    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content, self.__location_id) = Blockette._get_string(content, 2, 2, "UN")
        (remaining_content, self.__channel_id) = Blockette._get_string(remaining_content, 3, 3, "UN")
        (remaining_content, self.__subchannel_id) = Blockette._get_decimal(remaining_content, "####")
        (remaining_content, self.__intrument_id) = Blockette._get_decimal(remaining_content, "###")
        (remaining_content, self.__comment) = Blockette._get_string(remaining_content, 0, 30, "UNLPS")
        (remaining_content, self.__signal_resp_unit) = Blockette._get_decimal(remaining_content, "###")
        (remaining_content, self.__calibration_input_unit) = Blockette._get_decimal(remaining_content, "###")
        (remaining_content, self.__latitude) = Blockette._get_decimal(remaining_content, "-##.######")
        (remaining_content, self.__longitude) = Blockette._get_decimal(remaining_content, "-###.######")
        (remaining_content, self.__elevation) = Blockette._get_decimal(remaining_content, "-####.#")
        (remaining_content, self.__local_depth) = Blockette._get_decimal(remaining_content, "###.#")
        (remaining_content, self.__azimuth) = Blockette._get_decimal(remaining_content, "###.#")
        (remaining_content, self.__dip) = Blockette._get_decimal(remaining_content, "-##.#")
        (remaining_content, self.__data_format_id) = Blockette._get_decimal(remaining_content, "####")
        (remaining_content, self.__data_record_length) = Blockette._get_decimal(remaining_content, "##")
        (remaining_content, self.__sample_rate) = Blockette._get_float(remaining_content, "#.####E-##")
        (remaining_content, self.__max_clock_drift) = Blockette._get_float(remaining_content, "#.####E-##")
        (remaining_content, self.__nb_comments) = Blockette._get_decimal(remaining_content, "####")
        (remaining_content, self.__channel_flag) = Blockette._get_string(remaining_content, 0, 26, "U")
        (remaining_content, self.__start_date) = Blockette._get_time(remaining_content)
        (remaining_content, self.__end_date) = Blockette._get_time(remaining_content)
        (remaining_content, self.__update_flag) = Blockette._get_string(remaining_content, 1, 1, "UN")
        if len(remaining_content) != 0:
            raise ValueError("Content of blk 052 is not valid find remaining string <%s>" % remaining_content)

    def instrument_id(self):
        """
        Give the instrument description lookup key.
        """
        return self.__intrument_id

    def signal_response_unit(self):
        """
        Give the unit lookup key of signal.
        """
        return self.__signal_resp_unit

    def calibration_input_unit(self):
        """
        Give the calibration unit lookup key.
        """
        return self.__calibration_input_unit

    def data_format(self):
        """
        Give the data format lookup code key
        """
        return self.__data_format_id

    def __str__(self, *args, **kwargs):
        return "%2s %3s %11f %12f %12f %10f %22s %22s" % (self.__location_id, self.__channel_id, self.__latitude, self.__longitude,
                                                          self.__elevation, self.__sample_rate, self.__start_date, self.__end_date)


class Blockette_053(Blockette):

    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content, self.__transfer_function_type) = Blockette._get_string(content, 1, 1, "U")
        (remaining_content, self.__stage_sequence) = Blockette._get_decimal(remaining_content, "##")
        (remaining_content, self.__stage_input_unit) = Blockette._get_decimal(remaining_content, "###")
        (remaining_content, self.__stage_output_unit) = Blockette._get_decimal(remaining_content, "###")
        (remaining_content, self.__normalisation_factor) = Blockette._get_float(remaining_content, "-#.#####E-##")
        (remaining_content, self.__normalisation_freq) = Blockette._get_float(remaining_content, "-#.#####E-##")

        (remaining_content, self.__nb_zeros) = Blockette._get_decimal(remaining_content, "###")
        self.__zeros = []
        for current_zero in range(self.__nb_zeros):  # @UnusedVariable
            (remaining_content, real_zero) = Blockette._get_float(remaining_content, "-#.#####E-##")
            (remaining_content, imaginary_zero) = Blockette._get_float(remaining_content, "-#.#####E-##")
            (remaining_content, real_error) = Blockette._get_float(remaining_content, "-#.#####E-##")
            (remaining_content, imaginary_error) = Blockette._get_float(remaining_content, "-#.#####E-##")
            self.__zeros.append((real_zero, imaginary_zero, real_error, imaginary_error))

        (remaining_content, self.__nb_poles) = Blockette._get_decimal(remaining_content, "###")
        self.__poles = []
        for current_pole in range(self.__nb_poles):  # @UnusedVariable
            (remaining_content, real_pole) = Blockette._get_float(remaining_content, "-#.#####E-##")
            (remaining_content, imaginary_pole) = Blockette._get_float(remaining_content, "-#.#####E-##")
            (remaining_content, real_error) = Blockette._get_float(remaining_content, "-#.#####E-##")
            (remaining_content, imaginary_error) = Blockette._get_float(remaining_content, "-#.#####E-##")
            self.__poles.append((real_pole, imaginary_pole, real_error, imaginary_error))
        if len(remaining_content) != 0:
            raise ValueError("Content of blk 053 is not valid find remaining string <%s>" % remaining_content)

    def __str__(self, *args, **kwargs):
        transfer_function_type = "None"
        if self.__transfer_function_type == 'A':
            transfer_function_type = 'LAPLACE TRANSFORM (rad/s)'
        elif self.__transfer_function_type == 'B':
            transfer_function_type = 'ANALOG RESPONSE (Hz)'
        elif self.__transfer_function_type == 'C':
            transfer_function_type = 'Composite (currently undefined)'
        elif self.__transfer_function_type == 'D':
            transfer_function_type = 'Digital (Z - transform)'
        return "Responses Poles and zeros (053) : %2d %32s %12f %12f" % (self.__stage_sequence, transfer_function_type, self.__normalisation_factor, self.__normalisation_freq)

    def input_unit(self):
        """
        Give stage input unit lookup code
        """
        return self.__stage_input_unit

    def output_unit(self):
        """
        Give stage output unit lookup code
        """
        return self.__stage_output_unit


class Blockette_054(Blockette):

    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content, self.__response_type) = Blockette._get_string(content, 1, 1, "U")
        (remaining_content, self.__stage_sequence) = Blockette._get_decimal(remaining_content, "##")
        (remaining_content, self.__stage_input_unit) = Blockette._get_decimal(remaining_content, "###")
        (remaining_content, self.__stage_output_unit) = Blockette._get_decimal(remaining_content, "###")

        (remaining_content, self.__nb_numerators) = Blockette._get_decimal(remaining_content, "####")
        self.__numerators = []
        for current_numerator in range(self.__nb_numerators):  # @UnusedVariable
            (remaining_content, numerator_coef) = Blockette._get_float(remaining_content, "-#.#####E-##")
            (remaining_content, numerator_error) = Blockette._get_float(remaining_content, "-#.#####E-##")
            self.__numerators.append((numerator_coef, numerator_error))

        (remaining_content, self.__nb_denominators) = Blockette._get_decimal(remaining_content, "####")
        self.__denominators = []
        for current_denominator in range(self.__nb_denominators):  # @UnusedVariable
            (remaining_content, denominator_coef) = Blockette._get_float(remaining_content, "-#.#####E-##")
            (remaining_content, denominator_error) = Blockette._get_float(remaining_content, "-#.#####E-##")
            self.__denominators.append((denominator_coef, denominator_error))

        if len(remaining_content) != 0:
            raise ValueError("Content of blk 054 is not valid find remaining string <%s>" % remaining_content)

    def __str__(self, *args, **kwargs):
        return "Responses Coefficients (054) : %2d %4d %4d" % (self.__stage_sequence, self.__nb_numerators, self.__nb_denominators)

    def input_unit(self):
        """
        Give stage input unit lookup code
        """
        return self.__stage_input_unit

    def output_unit(self):
        """
        Give stage output unit lookup code
        """
        return self.__stage_output_unit


class Blockette_057(Blockette):

    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content, self.__stage_sequence) = Blockette._get_decimal(content, "##")
        (remaining_content, self.__input_rate) = Blockette._get_float(remaining_content, "#.####E-##")
        (remaining_content, self.__decimation_factor) = Blockette._get_decimal(remaining_content, "#####")
        (remaining_content, self.__decimation_offset) = Blockette._get_decimal(remaining_content, "#####")
        (remaining_content, self.__estimation_delay) = Blockette._get_float(remaining_content, "-#.####E-##")
        (remaining_content, self.__correction_applied) = Blockette._get_float(remaining_content, "-#.####E-##")

        if len(remaining_content) != 0:
            raise ValueError("Content of blk 057 is not valid find remaining string <%s>" % remaining_content)

    def __str__(self, *args, **kwargs):
        return "Decimation (057) : %2d %10f %5d %5d %10f %10f" % (self.__stage_sequence, self.__input_rate, self.__decimation_offset, self.__decimation_offset,
                                                                  self.__estimation_delay, self.__correction_applied)


class Blockette_058(Blockette):

    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content, self.__stage_sequence) = Blockette._get_decimal(content, "##")
        (remaining_content, self.__Sd) = Blockette._get_float(remaining_content, "-#.#####E-##")
        (remaining_content, self.__frequency) = Blockette._get_float(remaining_content, "-#.#####E-##")
        (remaining_content, self.__nb_history_values) = Blockette._get_decimal(remaining_content, "##")

        self.__history_values = []
        for current_value in range(self.__nb_history_values):  # @UnusedVariable
            (remaining_content, sensitivity_calibration) = Blockette._get_float(remaining_content, "-#.#####E-##")
            (remaining_content, frequency_calibration) = Blockette._get_float(remaining_content, "-#.#####E-##")
            (remaining_content, time_above_calibration) = Blockette._get_time(remaining_content)
            self.__history_values.append((sensitivity_calibration, frequency_calibration, time_above_calibration))

        if len(remaining_content) != 0:
            raise ValueError("Content of blk 058 is not valid find remaining string <%s>" % remaining_content)

    def __str__(self, *args, **kwargs):
        return "Sensitivity/Gain (058): %2d Sd=%12f f=%12f %2d" % (self.__stage_sequence, self.__Sd,
                                                                   self.__frequency, self.__nb_history_values)


class Blockette_059(Blockette):
    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content,
         self.__effective_start) = Blockette._get_time(content)
        (remaining_content,
         self.__effective_end) = Blockette._get_time(remaining_content)
        (remaining_content,
         self.__code_key) = Blockette._get_integer(remaining_content, 4)
        (remaining_content,
         self.__level) = Blockette._get_integer(remaining_content, 6)
        if len(remaining_content) != 0:
            raise ValueError("Content of blk 051 is not valid find remaining string <%s>" % remaining_content)

    def __str__(self, *args, **kwargs):
        return ("Blk 059 Channel comment (%s-%s): %s" %
                (self.__effective_start, self.__effective_end,
                 "comment")).encode('utf8')

    def lookup_code(self):
        """
        Give the lookup key of comment
        """
        return self.__code_key


class Blockette_061(Blockette):

    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content, self.__stage_sequence) = Blockette._get_decimal(content, "##")
        (remaining_content, self.__response_name) = Blockette._get_string(remaining_content, 1, 25, "UN_S")
        (remaining_content, self.__symmetry_code) = Blockette._get_string(remaining_content, 1, 1, "U")
        (remaining_content, self.__input_signal_unit) = Blockette._get_decimal(remaining_content, "###")
        (remaining_content, self.__output_signal_unit) = Blockette._get_decimal(remaining_content, "###")
        (remaining_content, self.__nb_coef) = Blockette._get_decimal(remaining_content, "####")

        self.__coef = []
        for current_coef in range(self.__nb_coef):  # @UnusedVariable
            (remaining_content, coef) = Blockette._get_float(remaining_content, "-#.#######E-##")
            self.__coef.append(coef)

        if len(remaining_content) != 0:
            raise ValueError("Content of blk 061 is not valid find remaining string <%s>" % remaining_content)

    def __str__(self, *args, **kwargs):
        return ("FIR Response %s (061) : %2d %4d" %
                (self.__response_name, self.__stage_sequence,
                 self.__nb_coef))

    def input_unit(self):
        """
        Give stage input unit lookup code
        """
        return self.__input_signal_unit

    def output_unit(self):
        """
        Give stage output unit lookup code
        """
        return self.__output_signal_unit


class Blockette_062(Blockette):
    def __init__(self, blk_type, content):
        Blockette.__init__(self, blk_type, content)
        (remaining_content, self.__transfer_function_type) = Blockette._get_string(content, 1, 1, "U")
        (remaining_content, self.__stage_sequence) = Blockette._get_decimal(remaining_content, "##")
        (remaining_content, self.__input_signal_unit) = Blockette._get_decimal(remaining_content, "###")
        (remaining_content, self.__output_signal_unit) = Blockette._get_decimal(remaining_content, "###")
        (remaining_content, self.__approximation_type) = Blockette._get_string(remaining_content, 1, 1, "U")
        (remaining_content, self.__valid_frequency_unit) = Blockette._get_string(remaining_content, 1, 1, "U")
        (remaining_content, self.__lower_valid_frequency_bound) = Blockette._get_float(remaining_content, "-#.#####E-##")
        (remaining_content, self.__upper_valid_frequency_bound) = Blockette._get_float(remaining_content, "-#.#####E-##")
        (remaining_content, self.__lower_bound_approximation) = Blockette._get_float(remaining_content, "-#.#####E-##")
        (remaining_content, self.__upper_bound_approximation) = Blockette._get_float(remaining_content, "-#.#####E-##")
        (remaining_content, self.__maximum_absolute_error) = Blockette._get_float(remaining_content, "-#.#####E-##")
        (remaining_content, self.__number_coefficients) = Blockette._get_decimal(remaining_content, "###")

        self.__coefficients = []
        for current_coef_index in range(self.__number_coefficients):  # @UnusedVariable
            (remaining_content, coef) = Blockette._get_float(remaining_content, "-#.#####E-##")
            (remaining_content, error) = Blockette._get_float(remaining_content, "-#.#####E-##")
            self.__coefficients.append((coef, error))

        if len(remaining_content) != 0:
            raise ValueError("Content of blk 062 is not valid find remaining string <%s>" %
                             remaining_content)

    def __str__(self, *args, **kwargs):
        return ("Polynomial Response %s (062) : %2d %4d" %
                (self.__transfer_function_type,
                 self.__stage_sequence,
                 self.__number_coefficients))

    def input_unit(self):
        """
        Give stage input unit lookup code
        """
        return self.__input_signal_unit

    def output_unit(self):
        """
        Give stage output unit lookup code
        """
        return self.__output_signal_unit
