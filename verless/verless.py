#!/usr/bin/python
# -*- coding : utf-8 -*-
from blockette import Blockette
from getopt import GetoptError
from getopt import getopt
import traceback
import sys
import re


####
# Usage function
####
def usage():
    """
    Simple usage method.
    """
    print(sys.argv[0], " verify dataless syntax")
    print("-i, --input    Set the input file")
    print("-v, --verbose  Enable verbose mode")


class DatalessReaderException(Exception):

    def __init__(self, message):
        self.__message = message

    def __str__(self, *args, **kwargs):
        return self.__message


class DatalessReader(object):

    def __init__(self, filename, verbose=False):
        """
        Initialize this dataless reader

        filename -- The dataless filename
        verbose  -- The verbose flag
        """
        self.__filename = filename
        self.__verbose = verbose
        self.__file_read = False
        self.__blockettes = list()
        self.__data_format = dict()
        self.__comment = dict()
        self.__generic_abbreviation = dict()
        self.__unit_abbreviation = dict()

    def read(self):
        """
        Open, read and parse the dataless file.
        """
        dataless_file = open(self.__filename)

        # First we try to parse first blockette (blk010) for getting logical volume size
        line = dataless_file.readline()
        (not_used, blk010) = Blockette.blockette_constructor(line[8:])  # @UnusedVariable
        if blk010.type() != 10:
            raise DatalessReaderException("Expecting blockette 010 and get blockette %s" %
                                          blk010.type())
        logical_volume_size = blk010.get_volumes_size()
        if self.__verbose:
            print("Logical volume size: %s" % logical_volume_size)

        # Reaopen dataless
        dataless_file = open(self.__filename)
        if self.__verbose:
            print("Logical volume size = %d" % logical_volume_size)
        # First we read seed file
        for line in dataless_file.readlines():

            while(len(line) != 0 and len(line.strip()) != 0):
                if len(line) < logical_volume_size:
                    raise DatalessReaderException("Size of logical is not correct, must be %d and is %d" %
                                                  (logical_volume_size, len(line)))

                if not re.match("^[0-9]{6}[VAS][ *]$", line[0:8]):
                    raise DatalessReaderException("Bad header of logical (%s)\n%s" %
                                                  (line[0:7], line))

                # logical_volume_index = int(line[0:6])
                logical_volume = line[8:logical_volume_size]
                line = line[logical_volume_size:]
                if self.__verbose:
                    print("remaining line <%s...>" % line[:60])
                # Merge logical volume that is continuation
                while(len(line) != 0 and line[7] == '*'):
                    if self.__verbose:
                        print("merging %s\n" % line[0:8])
                    logical_volume = logical_volume + line[8:logical_volume_size]
                    line = line[logical_volume_size:]
                    if self.__verbose:
                        print("remaining line <%s...>" % line[:60])
                # Now parse this logical volume
                while(len(logical_volume) != 0 and
                      len(logical_volume.lstrip()) != 0):
                    if self.__verbose:
                        print("remaining logical volume <%s...>" %
                              logical_volume[:60])
                    (logical_volume, new_blockette) = Blockette.blockette_constructor(logical_volume)
                    if self.__verbose:
                        print(new_blockette)
                    self.__blockettes.append(new_blockette)
                    if new_blockette.type() == 30:
                        self.__data_format[new_blockette.lookup_code()] = new_blockette
                    elif new_blockette.type() == 31:
                        self.__comment[new_blockette.lookup_code()] = new_blockette
                    elif new_blockette.type() == 33:
                        self.__generic_abbreviation[new_blockette.lookup_code()] = new_blockette
                    elif new_blockette.type() == 34:
                        self.__unit_abbreviation[new_blockette.lookup_code()] = new_blockette
                    elif new_blockette.type() == 50:
                        # Verify that owner lookup code exists
                        if new_blockette.get_owner() not in self.__generic_abbreviation:
                            raise DatalessReaderException("Can't find owner with lookup code %s" %
                                                          new_blockette.get_owner())

        
    def verify(self):
        """
        Make dataless verification
        """
        if not self.__file_read:
            self.read()
        for current_blk in self.__blockettes:
            if current_blk.type() == 50:
                if current_blk.get_owner() not in self.__generic_abbreviation:
                    raise DatalessReaderException("Can't find abbreviation %d (Owner B050F10)" %
                                                  current_blk.get_owner())
            elif current_blk.type() == 52:
                if current_blk.signal_response_unit() not in self.__unit_abbreviation:
                    raise DatalessReaderException("Can't find abbreviation %d (Signal unit B052F08)" %
                                                  current_blk.signal_response_unit())
                if current_blk.calibration_input_unit() not in self.__unit_abbreviation:
                    raise DatalessReaderException("Can't find abbreviation %d (Calibration unit B052F09)" %
                                                  current_blk.calibration_input_unit())
                if current_blk.instrument_id() not in self.__generic_abbreviation:
                    raise DatalessReaderException("Can't find abbreviation %d (Intrument B052F06)" %
                                                  current_blk.instrument_id())
                if current_blk.data_format() not in self.__data_format:
                    raise DatalessReaderException("Can't find abbreviation %d (Data format B052F16)" %
                                                  current_blk.data_format())
            elif current_blk.type() in (51, 59):
                   # Verify that comment lookup code exist
                   if current_blk.lookup_code() not in self.__comment:
                       raise DatalessReaderException("Can't find comment with lookup code %d" %
                                                     current_blk.lookup_code())

            elif current_blk.type() in (53, 54, 61, 62):
                        if current_blk.input_unit() not in self.__unit_abbreviation:
                            raise DatalessReaderException("Can't find stage input unit with lookup code %d" %
                                                          current_key.input_unit())
                        if current_blk.output_unit() not in self.__unit_abbreviation:
                            raise DatalessReaderException("Can't find stage output unit with lookup code %d" %
                                                          current_blk.output_unit())

    def __str__(self, *args, **kwargs):
        result = ""
        for blk in self.__blockettes:
            result = "%s%s\n" % (result, blk)
        return result


def main():
    input_file = None
    verbose = False
    try:
        opts, args = getopt(sys.argv[1:], "hi:v", ["help", "input:",  # @UnusedVariable
                                                   "verbose"])
    except GetoptError as err:
        print(str(err))
        sys.exit()

    for current_option, current_argument in opts:
        if current_option in ("-h", "--help"):
            usage()
            sys.exit()
        elif current_option in ("-i", "--input"):
            input_file = current_argument
        elif current_option in ("-v", "--verbose"):
            verbose = True

    if input_file is None:
        print("You must specify input file with -i")
        sys.exit(1)

    # Enable strict mode
    Blockette.set_strict_mode(True)
    reader = DatalessReader(input_file, verbose)
    try:
        reader.read()
        reader.verify()
    except Exception as exception:
        traceback.print_exc(file=sys.stderr)
        mes = "\n\nError: %s\n" % unicode(exception)
        sys.stderr.write(mes.encode('UTF8'))
        sys.exit(1)


if __name__ == "__main__":
    main()
