#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
from jinja2 import Environment, PackageLoader
from gendbirdv2.jsondbird import generateJSONDbird
from oca.jinjautils.filters import dbirddate, empty, optional_value, prefix_list_elt


DBIRD_FORMAT = "dbird"
JSON_FORMAT = "json"
YAML_FORMAT = "yaml"

VALID_FORMAT = [DBIRD_FORMAT, JSON_FORMAT, YAML_FORMAT]


class DbirdGenerator(object):

    def __init__(self, generation_format):
        if generation_format not in VALID_FORMAT:
            raise ValueError("%s is an unknown generation format")
        self.__generation_format = generation_format
        if generation_format != JSON_FORMAT:
            self.__environement = Environment(loader=PackageLoader('gendbirdv2',
                                                                   'templates'),
                                              line_statement_prefix="%")
            self.__environement.filters['dbirddate'] = dbirddate
            self.__environement.filters['optional_value'] = optional_value
            self.__environement.filters['prefix_list_elt'] = prefix_list_elt
            self.__environement.tests['empty'] = empty

            if generation_format == DBIRD_FORMAT:
                self.__template = self.__environement.get_template('dbird.tmpl')
            elif generation_format == YAML_FORMAT:
                self.__template = self.__environement.get_template('ydbird.tmpl')

    def generate(self, template_infos):
        if self.__generation_format == JSON_FORMAT:
            return json.dumps(generateJSONDbird(template_infos), indent=2, ensure_ascii=False)
        return self.__template.render(template_infos)
