#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys


def string_date(date):
    if date is None:
        return None
    return date.strftime("%Y-%m-%dT%H:%M:%SZ")


def fill_authors(result, dbird_definition):
    for author in dbird_definition["authors"]:
        new_author = {
            "label": author.dbird_code(),
            "names": [" ".join([author.author().firstname(), author.author().lastname()])],
            "agencies": [],
            "emails": map(lambda x: x.email(), author.author().contact().contact_email()),
            "phones": map(lambda x: x.phone(), author.author().contact().contact_phone()),
        }
        result["authors"].append(new_author)


def dbird_comment(comment, comment_type):
    return {
        "label": comment.dbird_code(),
        "description": comment.comment().value(),
        "starttime": string_date(comment.comment().begin_time()),
        "endtime": string_date(comment.comment().end_time()),
        "authors": comment.author_list(),
        "type": comment_type
    }


def fill_networks(result, dbird_definition):
    for network in dbird_definition["networks"]:
        for comment in network.comment():
            new_comment = dbird_comment(comment, "N")
            result["networks"]["network_comments"].append(new_comment)

    for network in dbird_definition["networks"]:
        new_network = {
            "label": network.dbird_code(),
            "fdsn_code": network.fdsn_code(),
            "description": network.description(),
            "contact_mail": network.contact(),
            "starttime": string_date(network.open_date()),
            "endtime": string_date(network.close_date()),
            "alternate_code": network.code(),
            "comments": network.comments_dbird_code()
        }
        result["networks"]["networks"].append(new_network)


def init_station(station):
    return {
        "iris_code": station.iris_code(),
        "starttime": string_date(station.first_install()),
        "endtime": string_date(station.close_date()),
        "latitude": {
            "value": station.latitude(),
            "minus_error": station.latitude_min_err(),
            "plus_error": station.latitude_plus_err(),
        },
        "longitude": {
            "value": station.longitude(),
            "minus_error": station.longitude_min_err(),
            "plus_error": station.longitude_plus_err()
        },
        "elevation": {
            "value": station.altitude() * station.altitude_unit().mksa(),
            "minus_error": station.altitude_min_err(),
            "plus_error": station.altitude_plus_err()
        },
        "site_name": station.toponyme(),
        "contact": station.observatory().network_contact(),
        "owner": station.observatory().description(),
        "owner_website": station.observatory().url(),
        "comments": [],
        "locations": [],
        "sensors": [],
        "analog_filters": [],
        "digitizers": [],
        "digital_filters": [],
        "channels": []
    }


def dbird_location(location):
    return {
        "label": location.dbird_code(),
        "code": location.code(),
        "latitude": {
            "value": location.latitude(),
            "minus_error": location.latitude_min_err(),
            "plus_error": location.latitude_plus_err()
        },
        "longitude": {
            "value": location.longitude(),
            "minus_error": location.longitude_min_err(),
            "plus_error": location.longitude_plus_err()
        },
        "elevation": {
            "value": location.altitude(),
            "minus_error": location.altitude_min_err(),
            "plus_error": location.altitude_plus_err()
        },
        "depth": location.depth(),
        "vault": location.vault(),
        "geology": location.geology()
    }


def dbird_sensor(sensor):
    return {
        "label": sensor.dbird_code(),
        "type": sensor.model(),
        "serial_number": sensor.serial_number(),
        "pz_file": sensor.pz_file(),
        "azimuth": sensor.azimuth(),
        "dip": sensor.dip()
    }


def dbird_device(device):
    return {
        "label": device.dbird_code(),
        "type": device.model(),
        "serial_number": device.serial_number(),
        "pz_file": device.pz_file()
    }


def dbird_channel(channel):
    return {
        "location": channel.location().dbird_code(),
        "iris_code": channel.iris_code(),
        "sample_rate": channel.sample_rate(),
        "sensor": channel.sensor_comp().dbird_code(),
        "analog_filter": channel.analog_filter(),
        "digitizer": channel.das_comp().dbird_code(),
        "digital_filter": channel.digital_filter(),
        "flags": channel.channel_flags(),
        "encoding": channel.data_format().code(),
        "starttime": string_date(channel.open_date()),
        "endtime": string_date(channel.close_date()),
        "network": channel.dbird_network().dbird_code(),
        "comments": channel.comments_dbird_code()
    }


def fill_station(result, dbird_definition):
    for station in dbird_definition["stations"]:
        new_station = init_station(station)
        result["stations"].append(new_station)

        for comment in station.comment():
            new_comment = dbird_comment(comment, "S")
            new_station["comments"].append(new_comment)
        for comment in station.selected_channel_comment():
            new_comment = dbird_comment(comment, "C")
            new_station["comments"].append(new_comment)
        for location in station.locations():
            new_location = dbird_location(location)
            new_station["locations"].append(new_location)
        for sensor in station.sensors():
            new_sensor = dbird_sensor(sensor)
            new_station["sensors"].append(new_sensor)
        for current_filter in station.analog_filters():
            new_filter = dbird_device(current_filter)
            new_station["analog_filters"].append(new_filter)
        for digitizer in station.digitizers():
            new_digitizer = dbird_device(digitizer)
            new_station["digitizers"].append(new_digitizer)
        for digital_filter in station.digital_filters():
            new_filter = dbird_device(digital_filter)
            new_station["digital_filters"].append(new_filter)
        for current in station.selected_channel():
            if current.sample_rate() is None:
                sys.stderr.write("Skip %s_%s can't determine sample rate for channel\n" %
                                 (station.iris_code(), current.iris_code()))
                continue
            new_channel = dbird_channel(current)
            new_station["channels"].append(new_channel)


def generateJSONDbird(dbird_definition):
    """
    Generate JSON dbird ditionnary according to information
    present in given dbird definition.

    dbird_definition -- The definition of dbird

    return           -- The JSON dbird as dictionnary
    """
    result = {
        "version": 2.0,
        "originating_organization": dbird_definition["seed_generator"] if "seed_generator" in dbird_definition else "",
        "datacenter_contact": dbird_definition["seed_generator_contact"] if "seed_generator_contact" in dbird_definition else "",
        "web_site": dbird_definition["seed_generator_url"] if "seed_generator_url" in dbird_definition else "",
        "contact_phone": dbird_definition["seed_generator_phone"] if "seed_generator_phone" in dbird_definition else "",
        "authors": [],
        "networks": {
            "network_comments": [],
            "networks": []
        },
        "stations": []
    }
    fill_authors(result, dbird_definition)
    fill_networks(result, dbird_definition)
    fill_station(result, dbird_definition)
    return result
